/*
 *	Author: Christopher Acornley
 *	Use:	Holds the objects, enumerations and functions used by the AI 
 *	Notes:	Contains:	Set of moves for maze generation
 *						2D Vector object
 *						2D Vector object for pathfinding (contains additional types)
 *						Data Holder for valid moves
 *						Object with overwritten bool operator (used for pathfinding)
 *						Pathfinding function
 */
#ifndef _VECTOR_2_HPP_
#define _VECTOR_2_HPP_

//////////////
// INCLUDES //
//////////////
#include <stack>
#include <queue>
#include <list>

// Namespace to contain the possible valid moves during maze generation (used only by Maze Generator class)
namespace MAZEMOVES
{
	enum MazeMoves { MOVE_UP = 0, MOVE_DOWN = 1, MOVE_LEFT = 2, MOVE_RIGHT = 3, MOVE_INVALID = 4 };
}

// Namespace for Library
namespace olib
{
	// Struct - Vector2
	struct Vector2
	{
		// X and Y data types
		int x, y;

		/*
		 * Name:		Vector2
		 ======================================================================
		 * Arguments:	None

		 * Returns:		None

		 * Notes:		Constructor. Sets X and Y values to 0.
		 */
		Vector2() {
			x = y = 0;
		}

		/*
		 * Name:		Vector2
		 ======================================================================
		 * Arguments:	p_X - Value to set x
						p_Y	- Value to set y

		 * Returns:		None

		 * Notes:		Constructor. Sets X and Y values to argument values.
		 */
		Vector2(int p_X, int p_Y) {
			x = p_X;
			y = p_Y;
		}

	}; //== End of Vector2 ==//

	// Struct - MoveFindData
	struct MoveFindData
	{
		// Counter for the number of valid moves
		int m_NumberOfMoves;

		// Queue container for valid moves
		std::queue<int> m_Moves;

		/*
		 * Name:		MoveFindData
		 ======================================================================
		 * Arguments:	None

		 * Returns:		None

		 * Notes:		Constructor. Sets the number counter to 0. Queue is 
						empty by default.
		 */
		MoveFindData() 
		{
			m_NumberOfMoves = 0;
		}

	}; //== End of MoveFindData ==//

	// Struct - PathfindVector2
	struct PathfindVector2
	{
		// X and Y data types, as well as G and H
		// G - Number of moves to get to this point
		// H - Number of tiles to exit
		int x, y, g, h;

		/*
		 * Name:		PathfindVector2
		 ======================================================================
		 * Arguments:	None

		 * Returns:		None

		 * Notes:		Constructor. Sets all values to 0.
		 */
		PathfindVector2() {
			x = y = g = h = 0;
		}

		/*
		 * Name:		PathfindVector2
		 ======================================================================
		 * Arguments:	p_X	- Value to set x
						p_Y	- Value to set y

		 * Returns:		None

		 * Notes:		Constructor. Sets X and Y to argument values. Sets G
						and H to 0.
		 */
		PathfindVector2(int p_X, int p_Y) {
			x = p_X;
			y = p_Y;
			g = 0;
			h = 0;
		}

		/*
		 * Name:		PathfindVector2
		 ======================================================================
		 * Arguments:	p_X	- Value to set x
						p_Y	- Value to set y
						p_G - Value to set g
						p_H - Value to set h

		 * Returns:		None

		 * Notes:		Constructor. Sets X, Y, G and H to argument values.
		 */ 
		PathfindVector2(int p_X, int p_Y, int p_G, int p_H) {
			x = p_X;
			y = p_Y;
			g = p_G;
			h = p_H;
		}

		/*
		 * Name:		PathfindVector2
		 ======================================================================
		 * Arguments:	p_Vector2 - Normal vector data to start from

		 * Returns:		None

		 * Notes:		Constructor. Sets X and Y values to those from the 
						argument Vector2. Sets H to 999 (this indicates start
						point) and G to 0.
		 */ 
		PathfindVector2(const Vector2& p_Vector2) {
			x = p_Vector2.x;
			y = p_Vector2.y;
			h = 999;
			g = 0;
		}

	}; //== End of PathfindVector2 ==//

	// Struct - VectorPathCheck
	struct VectorPathCheck
	{
		/*
		 *	Overwriting Boolean Operator
		 *	
		 *	Reason : This operator is used in the priority queue for
		 *			 pathfinding. It checks if a node in the queue should be
		 *			 placed closer to the front depending on the sum of the 
		 *			 PathfindVector2's g and h values. Higher values (those
		 *			 more moves or further from the target) have lower
		 *			 priority in the queue.
		 */
		bool operator() (PathfindVector2& p_PathVector1, PathfindVector2& p_PathVector2)
		{
			if((p_PathVector1.g + p_PathVector1.h) > (p_PathVector2.g + p_PathVector2.h)) return true;
			return false;
		}

	}; //== End of VectorPathCheck ==//

	// Pathfinding function. Uses A* algorithm to find a path from the start position to the exit position.
	// It returns true if a path is found and false if otherwise
	bool Pathfind(const Vector2 p_MapSize, const Vector2 p_StartPosition, const Vector2 p_ExitPosition, int** p_Map, std::stack<Vector2>& p_PathStack);

} //== End of  olib ==//

#endif