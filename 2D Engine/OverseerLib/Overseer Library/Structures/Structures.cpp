//////////////
// INCLUDES //
//////////////
#include "Structures.hpp"

// Library namespace
namespace olib
{
	/*
	 * Name:		Pathfind
	 ==========================================================================
	 * Arguments:	p_MapSize		- Size of the maze
	 *				p_StartPosition	- Starting position for the path
	 *				p_ExitPosition	- Ending position for the path
	 *				p_Map			- Maze array
	 *				p_PathStack		- Reference to a stack which holds the path
	 *
	 * Returns:		Boolean			- True means a valid path was found between
	 *								  the start position and the end position.
	 *								- False means no path was found.
	 *
	 * Notes:		Use the A* algorithm to find a path between the start and 
	 *				end points specified. Makes use of a priority queue to 
	 *				sort out any nodes and chose the quickest path quickly. 
	 *				Once a path is found, it is backtracked and the Vector data
	 *				is pushed into a stack. A stack is used as it has a 'first
	 *				in last out' policy, so the first node will be the starting
	 *				point, and the ending node will be the exit.
	 */
	bool Pathfind(const Vector2 p_MapSize, const Vector2 p_StartPosition, const Vector2 p_ExitPosition, int** p_Map, std::stack<Vector2>& p_PathStack)
	{
		// Create a local priority queue
		std::priority_queue<PathfindVector2, std::vector<PathfindVector2>, VectorPathCheck> l_PathfindingQueue;

		// Create array for keeping tabs on which tiles have been visited
		bool l_VisitedArray[900] = { false };

		// Create array for certain tiles (exit and entrance) as well as the direction move needed to create the node
		int l_HashArray[900] = { 0 };

		// Flag to check if a valid path has been found - Set to false
		bool l_PathFound = false;
	
		// Push the starting position into the queue
		l_PathfindingQueue.push(PathfindVector2(p_StartPosition));

		// Set the value of that in the hash array to be 5 - This represents the starting point 
		l_HashArray[(p_StartPosition.y*p_MapSize.x) + p_StartPosition.x] = 5;

		// While loop - continue whilst there are nodes in the queue
		while(!l_PathfindingQueue.empty()) {

			// Get the top node
			PathfindVector2 l_CurrentVector = l_PathfindingQueue.top();

			// State this node has been visited
			l_VisitedArray[(l_CurrentVector.y*p_MapSize.x) + l_CurrentVector.x] = true;
			
			// Check if the distance between this node and the exit position is 0 (therefore the exit)
			// If so set the path found flag to true and break from the while loop
			if(l_CurrentVector.h == 0) {
				l_PathFound = true;
				break;
			}

			// Pop the node from the queue (We've recorded we've visited it and have it stored locally)
			l_PathfindingQueue.pop();

			// Check node is not at the edge of the map
			// Check the block is not a wall or one all ready visited
			// If all checks pass, calculate the H value for the target node
			// Find horizontal displacement from current position to target position (if negative turn positive)
			// Find vertical displaceent from current position to target position (if negative turn positive)
			// Add together to make H value
			// Push the target node into the queue, settings the new X, Y and H values
			// Set G value to be the current nodes G value + 1

			// Left
			if(l_CurrentVector.x != 0) {
				if((p_Map[l_CurrentVector.y][l_CurrentVector.x - 1] != 1) && (!l_VisitedArray[(l_CurrentVector.y*p_MapSize.x) + (l_CurrentVector.x - 1)])) {
					
					// X-axis displacement
					int l_XHValue = p_ExitPosition.x - (l_CurrentVector.x - 1);
					if(l_XHValue < 0) {
						l_XHValue = -l_XHValue;
					}
					
					// Y-axis displacement
					int YHValue = p_ExitPosition.y - l_CurrentVector.y;
					if(YHValue < 0) {
						YHValue = -YHValue;
					}

					int l_HValue = YHValue+l_XHValue;
					l_PathfindingQueue.push(PathfindVector2((l_CurrentVector.x - 1), l_CurrentVector.y, l_CurrentVector.g + 1, l_HValue));
					l_HashArray[(l_CurrentVector.y*p_MapSize.x) + (l_CurrentVector.x - 1)] = 1;
				}
			}

			// Down
			if(l_CurrentVector.y != (p_MapSize.y - 1)) {
				if((p_Map[l_CurrentVector.y + 1][l_CurrentVector.x] != 1) && (!l_VisitedArray[((l_CurrentVector.y + 1)*p_MapSize.x) + l_CurrentVector.x])) {
					
					// X-axis displacement
					int l_XHValue = p_ExitPosition.x - l_CurrentVector.x;
					if(l_XHValue < 0) {
						l_XHValue = -l_XHValue;
					}

					// Y-axis displacement
					int YHValue = p_ExitPosition.y - (l_CurrentVector.y + 1);
					if(YHValue < 0) {
						YHValue = -YHValue;
					}

					int l_HValue = YHValue+l_XHValue;
					l_PathfindingQueue.push(PathfindVector2(l_CurrentVector.x, (l_CurrentVector.y + 1), l_CurrentVector.g + 1, l_HValue));
					l_HashArray[((l_CurrentVector.y + 1)*p_MapSize.x) + l_CurrentVector.x] = 2;
				}
			}

			// Right
			if(l_CurrentVector.x != (p_MapSize.x - 1)) {
				if((p_Map[l_CurrentVector.y][l_CurrentVector.x + 1] != 1) && (!l_VisitedArray[(l_CurrentVector.y*p_MapSize.x) + (l_CurrentVector.x + 1)])) {
					
					// X-axis displacement
					int l_XHValue = p_ExitPosition.x - (l_CurrentVector.x + 1);
					if(l_XHValue < 0) {
						l_XHValue = -l_XHValue;
					}

					// X-axis displacement
					int YHValue = p_ExitPosition.y - l_CurrentVector.y;
					if(YHValue < 0) {
						YHValue = -YHValue;
					}

					int l_HValue = YHValue+l_XHValue;
					l_PathfindingQueue.push(PathfindVector2((l_CurrentVector.x + 1), l_CurrentVector.y, l_CurrentVector.g + 1, l_HValue));
					l_HashArray[(l_CurrentVector.y*p_MapSize.x) + (l_CurrentVector.x + 1)] = 3;
				}
			}

			// Up
			if(l_CurrentVector.y != 0) {
				if((p_Map[l_CurrentVector.y - 1][l_CurrentVector.x] != 1) && (!l_VisitedArray[((l_CurrentVector.y - 1)*p_MapSize.x) + l_CurrentVector.x])) {
					
					// X-axis displacement
					int l_XHValue = p_ExitPosition.x - l_CurrentVector.x;
					if(l_XHValue < 0) {
						l_XHValue = -l_XHValue;
					}

					// Y-axis displacement
					int YHValue = p_ExitPosition.y - (l_CurrentVector.y - 1);
					if(YHValue < 0) {
						YHValue = -YHValue;
					}

					int l_HValue = YHValue+l_XHValue;
					l_PathfindingQueue.push(PathfindVector2(l_CurrentVector.x, (l_CurrentVector.y - 1), l_CurrentVector.g + 1, l_HValue));
					l_HashArray[((l_CurrentVector.y - 1)*p_MapSize.x) + l_CurrentVector.x] = 4;
				}
			}
		}

		// If a valid path was found we now need to work backwards to store the path
		if(l_PathFound) {

			// Get the top value of the queue (this is why the node is popped after the check)
			PathfindVector2 l_CurrentVector = l_PathfindingQueue.top();

			// Place top point into the stack
			p_PathStack.push(Vector2(l_CurrentVector.x, l_CurrentVector.y));

			// Get the hash value to know where the previous tile to the current one was
			int l_CurrentHashValue = l_HashArray[(l_CurrentVector.y * p_MapSize.x) + l_CurrentVector.x];

			// While the current hash value is not 5 (represents start point)
			while(l_CurrentHashValue != 5) {
				
				// Check which direction move was used to create the current node
				switch(l_CurrentHashValue) {

					// RIGHT
					case 1:
					{
						l_CurrentVector.x ++;
						break;
					}

					// DOWN
					case 2:
					{
						l_CurrentVector.y --;
						break;
					}

					// LEFT
					case 3:
					{
						l_CurrentVector.x --;
						break;
					}

					// UP
					case 4:
					{
						l_CurrentVector.y ++;
						break;
					}

					// Dafault
					default:
					{
						break;
					}
				}

				// Push the new vector data into the stack
				p_PathStack.push(Vector2(l_CurrentVector.x, l_CurrentVector.y));

				// Get the next hash value from the current data
				l_CurrentHashValue = l_HashArray[(l_CurrentVector.y * p_MapSize.x) + l_CurrentVector.x];
			}

			// State the pathfinding was succesful
			return true;
		}

		return false;
	}
} //== End of olib ==//