#include "OverseerLibrary.hpp"

namespace olib
{
	OverseerLibrary::OverseerLibrary(void)
	{
		// Nullify the pointers
		m_CurrentMap = 0;
		m_MinorChangeHandler = 0;
		m_FuzzyInterface = 0;

		// Set any other variables to start values
		m_ChangeTest = false;

		// Set the elapsed time for the whole AI to zero
		m_OverseerElapsedTime = 0.0f;

		// Set the counter for the number of changes made by the AI to 0
		m_NumberOfChanges = 0;

		// Ensure the queue is empty by poping elements until it is empty
		while(!m_PathStack.empty()) {
			m_PathStack.pop();
		}

		// Set the starting length to zero
		m_PreviousPathLength = 0;

		for(int i = 0; i < 18; i++) {
			m_ResponsesArray[i] = "";
		}

		m_Response = "";
	}

	OverseerLibrary::~OverseerLibrary(void)
	{
		// Nothing to clean up
	}

	bool OverseerLibrary::Initialise(int** p_Map, Vector2 p_MapSize)
	{
		// Set the pointer to the map object
		m_CurrentMap = p_Map;
		// Check the pointer is valid
		if(!m_CurrentMap) {
			return false;
		}

		// Set map size vector
		m_MapSize = p_MapSize;

		// Find the exit block
		for(int i = 0; i < m_MapSize.y; i++) {
			for(int j = 0; j < m_MapSize.x; j++) {
				if(m_CurrentMap[j][i] == 2) {
					m_ExitPosition.x = i;
					m_ExitPosition.y = j;
				}
			}
		}

		m_FuzzyInterface = new FuzzyInferenceClass();
		if(!m_FuzzyInterface) {
			return false;
		}

		if(!m_FuzzyInterface->Initialise()) {
			return false;
		}

		// Seed the random number generator
		std::srand((int)time(NULL));

		// Set the first time elapse
		m_TimeElapseToChange = 15000.0f + (float)(rand() % 20000);

		// Set the time since the last change to zero
		m_TimeSinceLastChange = 0.0f;

		// Load in strings
		m_ResponsesArray[0] = "Just a change here and... whalah!";
		m_ResponsesArray[1] = "I don't like you going THAT way...";
		m_ResponsesArray[2] = "Hehe, take that.";
		m_ResponsesArray[3] = "Let's swap things up shall we?";
		m_ResponsesArray[4] = "Small changes here, here aaaand.... here.";
		m_ResponsesArray[5] = "Nooo! I wanted to change more. Not enough time.";
		m_ResponsesArray[6] = "Take that!";
		m_ResponsesArray[7] = "I've had enough of your meddling!";
		m_ResponsesArray[8] = "Escape this.";
		m_ResponsesArray[9] = "Try to find your way now.";
		m_ResponsesArray[10] = "I will trap you here forever!";
		m_ResponsesArray[11] = "Take that! And This!";
		m_ResponsesArray[12] = "Enough of your meddling. You'll be lost forever!";
		m_ResponsesArray[13] = "I will not let you escape MY maze!";
		m_ResponsesArray[14] = "There is no escape. Hehehehe.";
		m_ResponsesArray[15] = "<INVALID INPUT> What?!? No it isn't!";
		m_ResponsesArray[16] = "Hmm no that won't work...";
		m_ResponsesArray[17] = "Let me think...";

		// State the function was successful
		return true;
	}

	bool OverseerLibrary::Run(Vector2 p_PlayerMapPosition, float p_FrameTime)
	{
		// Set the players current map position
		m_PlayerMapPosition = p_PlayerMapPosition;

		// Update time since AI started
		m_OverseerElapsedTime += p_FrameTime;

		// Check if the AI is going to make a change
		if(CheckIfToMakeChange(p_FrameTime)) {
			// Generate a new maze for the game depending on the changes decided
			GenerateNewMaze(DecideChange());
		}

		// State that a change has been made and a new map created
		return true;
	}

	void OverseerLibrary::Shutdown(void)
	{
		// Check if the map structure pointer is valid
		if(m_CurrentMap) {
			// Nullify the pointer
			m_CurrentMap = 0;
		}

		if(m_MinorChangeHandler) {
			m_MinorChangeHandler->Shutdown();
			delete m_MinorChangeHandler;
			m_MinorChangeHandler = 0;
		}

		if(m_FuzzyInterface) {
			m_FuzzyInterface->Shutdown();
			delete m_FuzzyInterface;
			m_FuzzyInterface = 0;
		}
	}

	bool OverseerLibrary::CheckIfToMakeChange(float p_FrameTime)
	{
		// Set the flag to false
		m_ChangeTest = false;
		// Update the elapsed time since last chnge
		m_TimeSinceLastChange += p_FrameTime;
		// Check if elapsed time greater than the needed time
		if(m_TimeSinceLastChange >= m_TimeElapseToChange) {
			// Recalculate a new time to elapse for another change
			// Seed the random number generator
			std::srand((int)time(NULL));
			// Set the first time elapse
			m_TimeElapseToChange = 15000.0f + (float)(rand() % 20000);
			// Set the time since the last change to zero
			m_TimeSinceLastChange = 0.0f;
			// State a change has occured
			m_ChangeTest = true;
			// Return true
			return true;
		}
		
		// State no change to be made
		return false;
	}

	OverseerDecision::OVSDECISION OverseerLibrary::DecideChange(void)
	{
		std::stack<Vector2> l_DecisionStack;
		int l_BlocksToExit = 100;

		if(Pathfind(m_MapSize, m_PlayerMapPosition, m_ExitPosition, m_CurrentMap, l_DecisionStack)) {
			l_BlocksToExit = l_DecisionStack.size();
		}

		OverseerDecision::OVSDECISION l_TempHolder = m_FuzzyInterface->Run(m_OverseerElapsedTime/1000.0f, l_BlocksToExit);

		switch(l_TempHolder) {
			case OverseerDecision::OVS_MINOR:
			{
				std::srand((int)time(NULL));
				int l_Rand = std::rand() % 3;
				m_Response = m_ResponsesArray[l_Rand];
				break;
			}
			case OverseerDecision::OVS_SMALL:
			{
				std::srand((int)time(NULL));
				int l_Rand = (std::rand() % 3) + 3;
				m_Response = m_ResponsesArray[l_Rand];
				break;
			}
			case OverseerDecision::OVS_MEDIUM:
			{
				std::srand((int)time(NULL));
				int l_Rand = (std::rand() % 3) + 6;
				m_Response = m_ResponsesArray[l_Rand];
				break;
			}
			case OverseerDecision::OVS_LARGE:
			{
				std::srand((int)time(NULL));
				int l_Rand = (std::rand() % 3) + 9;
				m_Response = m_ResponsesArray[l_Rand];
				break;
			}
			case OverseerDecision::OVS_EXTREME:
			{
				std::srand((int)time(NULL));
				int l_Rand = (std::rand() % 3) + 12;
				m_Response = m_ResponsesArray[l_Rand];
				break;
			}
			case OverseerDecision::OVS_NONE:
			{
				std::srand((int)time(NULL));
				int l_Rand = (std::rand() % 3) + 15;
				m_Response = m_ResponsesArray[l_Rand];
				break;
			}
			default:
			{
				m_Response = "";
				break;
			}
		}
		// Return the output of the fuzzy inference system
		return l_TempHolder;
	}

	void OverseerLibrary::GenerateNewMaze(OverseerDecision::OVSDECISION p_Decision)
	{
		// Depending how the AI decided to make change
		// Minor, block current exit and open up new route somewhere else
		// Extreme, re-arrange entire map
		// Small - Large, re-generate a set portion of the map, connecting up old paths
		switch(p_Decision)
		{
			case OverseerDecision::OVS_MINOR:
			{
				// Check if the handler is created
				if(!m_MinorChangeHandler) {
					// If not, create the object
					m_MinorChangeHandler = new MinorChangeHandler();
					// Check if the object created properly and break from the loop if it does not
					if(!m_MinorChangeHandler) {
						break;
					}

					// Try to call the Initialise function
					if(!m_MinorChangeHandler->Initialise(m_MapSize, m_PlayerMapPosition, m_ExitPosition, m_CurrentMap)) {
						// If this fails, safely close the object and break from the loop
						m_MinorChangeHandler->Shutdown();
						delete m_MinorChangeHandler;
						m_MinorChangeHandler = 0;
						break;
					}
				}

				// Try and run the object
				if(m_MinorChangeHandler->Run(m_PathStack, m_MinorBlockChange)) {
					// If the object runs successfully, close the object safely
					m_MinorChangeHandler->Shutdown();
					delete m_MinorChangeHandler;
					m_MinorChangeHandler = 0;
				}

				// Break from the switch statement
				break;
			}
			case OverseerDecision::OVS_SMALL:
			{
				// Create a local map generator
				MazeGenerator l_SmallMazeGenerator;

				// Create a local temporary store for the generated maze
				int** l_TempMaze = l_SmallMazeGenerator.GenerateMaze(m_CurrentMap, Vector2(10,10), m_MapSize, m_PlayerMapPosition, m_ExitPosition);

				// If the new maze pointer is valid
				if(l_TempMaze) {
					// Remove the current map data
					for(int i = 0; i < m_MapSize.y; i++) {
						for(int j = 0; j < m_MapSize.x; j++) {
							m_CurrentMap[j][i] = l_TempMaze[j][i];
						}
					}
				}

				// Break from the switch statement
				break;
			}
			case OverseerDecision::OVS_MEDIUM:
			{
				// Create a local map generator
				MazeGenerator l_MediumMazeGenerator;

				// Create a local temporary store for the generated maze
				int** l_TempMaze = l_MediumMazeGenerator.GenerateMaze(m_CurrentMap, Vector2(15,15), m_MapSize, m_PlayerMapPosition, m_ExitPosition);

				// If the new maze pointer is valid
				if(l_TempMaze) {
					// Remove the current map data
					for(int i = 0; i < m_MapSize.y; i++) {
						for(int j = 0; j < m_MapSize.x; j++) {
							m_CurrentMap[j][i] = l_TempMaze[j][i];
						}
					}
				}

				// Break from the switch statement
				break;
			}
			case OverseerDecision::OVS_LARGE:
			{
				// Create a local map generator
				MazeGenerator l_LargeMazeGenerator;

				// Create a local temporary store for the generated maze
				int** l_TempMaze = l_LargeMazeGenerator.GenerateMaze(m_CurrentMap, Vector2(20,20), m_MapSize, m_PlayerMapPosition, m_ExitPosition);

				// If the new maze pointer is valid
				if(l_TempMaze) {
					// Remove the current map data
					for(int i = 0; i < m_MapSize.y; i++) {
						for(int j = 0; j < m_MapSize.x; j++) {
							m_CurrentMap[j][i] = l_TempMaze[j][i];
						}
					}
				}

				// Break from the switch statement
				break;
			}
			case OverseerDecision::OVS_EXTREME:
			{
				// Create a local map generator
				MazeGenerator l_ExtremeMazeGenerator;

				// Create a local temporary store for the generated maze
				int** l_TempMaze = l_ExtremeMazeGenerator.GenerateMaze(m_CurrentMap, m_MapSize, m_MapSize, m_PlayerMapPosition, m_ExitPosition);

				// If the new maze pointer is valid
				if(l_TempMaze) {
					// Remove the current map data
					for(int i = 0; i < m_MapSize.y; i++) {
						for(int j = 0; j < m_MapSize.x; j++) {
							m_CurrentMap[j][i] = l_TempMaze[j][i];
						}
					}
				}

				// Break from the switch statement
				break;
			}
			case OverseerDecision::OVS_NONE:
			{
				// Break out of the switch statement
				break;
			}
			case OverseerDecision::OVS_INVALID:
			{
				// Break out of the switch statement
				break;
			}
			default:
			{
				// Break out of the switch statement
				break;
			}
		}

		// State the end of the function
		return;
	}
}