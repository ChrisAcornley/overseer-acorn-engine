#ifndef _MEDIUM_CHANGE_HANDLER_HPP_
#define _MEDIUM_CHANGE_HANDLER_HPP_

namespace olib
{
	class MediumChangeHandler
	{
	public:
		MediumChangeHandler(void);
		~MediumChangeHandler(void);
		bool Initialise(void);
		bool Run(void);
		void Shutdown(void);
	};
}

#endif