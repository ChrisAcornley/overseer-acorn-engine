#ifndef _SMALL_CHANGE_HANDLER_HPP_
#define _SMALL_CHANGE_HANDLER_HPP_

namespace olib
{
	class SmallChangeHandler
	{
	public:
		SmallChangeHandler(void);
		~SmallChangeHandler(void);
		bool Initialise(void);
		bool Run(void);
		void Shutdown(void);
	};
}

#endif