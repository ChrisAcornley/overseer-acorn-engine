#ifndef _LARGE_CHANGE_HANDLER_HPP_
#define _LARGE_CHANGE_HANDLER_HPP_

namespace olib
{
	class LargeChangeHandler
	{
	public:
		LargeChangeHandler(void);
		~LargeChangeHandler(void);
		bool Initialise(void);
		bool Run(void);
		void Shutdown(void);
	};
}

#endif