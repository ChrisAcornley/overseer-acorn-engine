#ifndef _MINOR_CHANGE_HANDLER_HPP_
#define _MINOR_CHANGE_HANDLER_HPP_

#include "../Structures/Structures.hpp"

namespace olib
{
	class MinorChangeHandler
	{
	public:
		MinorChangeHandler(void);
		~MinorChangeHandler(void);
		bool Initialise(const Vector2 p_MapSize, const Vector2 p_PlayerMapPosition, const Vector2 p_ExitPosition, int** p_Map);
		bool Run(std::stack<Vector2>& p_PathStack, Vector2& p_BlockChangeLocation);
		void Shutdown(void);

	private:
		Vector2 m_MapSize;
		Vector2 m_PlayerMapPosition;
		Vector2 m_ExitPosition;
		int** m_MapPointer;
	};
}

#endif