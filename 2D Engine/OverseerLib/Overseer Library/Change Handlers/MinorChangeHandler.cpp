#include "MinorChangeHandler.hpp"

namespace olib
{
	MinorChangeHandler::MinorChangeHandler(void)
	{
		// Initialise Pointers
		m_MapPointer = NULL;
	}

	MinorChangeHandler::~MinorChangeHandler(void)
	{
		// Nothing to clean up
	}

	bool MinorChangeHandler::Initialise(const Vector2 p_MapSize, const Vector2 p_PlayerMapPosition, const Vector2 p_ExitPosition, int** p_Map)
	{
		m_MapSize = p_MapSize;
		m_PlayerMapPosition = p_PlayerMapPosition;
		m_ExitPosition = p_ExitPosition;
		m_MapPointer = p_Map;

		if(!m_MapPointer) {
			return false;
		}

		return true;
	}
	bool MinorChangeHandler::Run(std::stack<Vector2>& p_PathStack, Vector2& p_BlockChangeLocation)
	{
		// Pathfind route
		// Pick one point at random, between 3 and 7 from the player and change to wall
		// Pick a point between 4 points away check to see if changing it to a floor opens a path to the exit
		// If no, leave alone and pick another point
		if(!Pathfind(m_MapSize, m_PlayerMapPosition, m_ExitPosition, m_MapPointer, p_PathStack)) {
			return false;
		}

		// Get length of stack
		int l_NumberOfPathSquares = p_PathStack.size();
		int l_Rand = 0;
		std::stack<Vector2> l_TempStack;

		// Check the stack has at least 2 entries
		if(l_NumberOfPathSquares >= 2) {
			// Choose a random number between 2 and the l_NumberOfPathSquares or 7, whichever is smaller
			if(l_NumberOfPathSquares > 7) {
				l_Rand = std::rand() % 4 + 2;
			}
			else {
				int l_Range = l_NumberOfPathSquares - 2;
				l_Rand = std::rand() % l_Range + 2;
			}

			// Get to that number in the stack and extract it
			for(int i = 0; i < l_Rand; i++) {
				l_TempStack.push(p_PathStack.top());
				p_PathStack.pop();
			}

			p_BlockChangeLocation = l_TempStack.top();

			while(!l_TempStack.empty()) {
				p_PathStack.push(l_TempStack.top());
				l_TempStack.pop();
			}

			m_MapPointer[p_BlockChangeLocation.y][p_BlockChangeLocation.x] = 1;
		}

		// Enter loop to keep looking for exit until makes one
		// If it tries 50 attempts (5 frames), edit origional change 
		bool l_HashLocate[900] = { false };
		bool l_NoPath = true;
		int l_NumberOfGoes = 0;

		if(!Pathfind(m_MapSize, m_PlayerMapPosition, m_ExitPosition, m_MapPointer, p_PathStack)) {

			// set the change block as visited
			l_HashLocate[(p_BlockChangeLocation.y * m_MapSize.x) + p_BlockChangeLocation.x] = true;

			while(l_NoPath){
				// Go to random square
				int l_RandX = std::rand() % 15 - 7;
				int l_RandY = std::rand() % 15 - 7;

				// Keep rand numbers within map
				if((m_PlayerMapPosition.x + l_RandX) < 0) {
					l_RandX = -m_PlayerMapPosition.x;
				}

				if((m_PlayerMapPosition.x + l_RandX) > (m_MapSize.x - 1)) {
				l_RandX = (m_MapSize.x - 1);
				}

				if((m_PlayerMapPosition.y + l_RandY) < 0) {
					l_RandY = -m_PlayerMapPosition.y;
				}

				if((m_PlayerMapPosition.y + l_RandY) > (m_MapSize.y - 1)) {
				l_RandY = (m_MapSize.y - 1);
				}
				Vector2 currentBlock = Vector2(m_PlayerMapPosition.x + l_RandX, m_PlayerMapPosition.y + l_RandY);

				// Check if all ready visited
				if(!l_HashLocate[(currentBlock.y*m_MapSize.x) + currentBlock.x]) {
					// Check if wall block
					if(m_MapPointer[currentBlock.y][currentBlock.x] == 1) {
						// State block is visited
						l_HashLocate[(currentBlock.y*m_MapSize.x) + currentBlock.x] = true;
						// Turn to floor
						m_MapPointer[currentBlock.y][currentBlock.x] = 0;
						// Perform pathfind
						if(Pathfind(m_MapSize, m_PlayerMapPosition, m_ExitPosition, m_MapPointer, p_PathStack)) {
							// if true, end loop
							l_NoPath = false;
						}
						else {
							// If false, return block to wall and start again
							m_MapPointer[currentBlock.y][currentBlock.x] = 1;
						}
					}

					// Add one to the number of goes the AI has tried to make a path
					l_NumberOfGoes++;
				}

				// If the AI has tried 50 or more times, break the loop
				if(l_NumberOfGoes >= 50) {
					break;
				}
			}
		}

		// If out of the loop and there is still no path
		if(l_NoPath) {
			// Reset the change
			m_MapPointer[p_BlockChangeLocation.y][p_BlockChangeLocation.x] = 0;
		}

		// If out of the loop and a change has been made
		else {
			l_NoPath = true;
		}

		return true;
	}
	void MinorChangeHandler::Shutdown(void)
	{
		if(m_MapPointer) {
			m_MapPointer = NULL;

		}

		return;
	}
}