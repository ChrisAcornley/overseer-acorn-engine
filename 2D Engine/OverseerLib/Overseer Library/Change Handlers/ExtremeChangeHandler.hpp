#ifndef _EXTREME_CHANGE_HANDLER_HPP_
#define _EXTREME_CHANGE_HANDLER_HPP_

namespace olib
{
	class ExtremeChangeHandler
	{
	public:
		ExtremeChangeHandler(void);
		~ExtremeChangeHandler(void);
		bool Initialise(void);
		bool Run(void);
		void Shutdown(void);
	};
}

#endif