#include "ExtremeChangeHandler.hpp"

namespace olib
{
	ExtremeChangeHandler::ExtremeChangeHandler(void)
	{
	}

	ExtremeChangeHandler::~ExtremeChangeHandler(void)
	{
		// Nothing to clean up
	}

	bool ExtremeChangeHandler::Initialise(void)
	{
		return true;
	}

	bool ExtremeChangeHandler::Run(void)
	{
		return true;
	}

	void ExtremeChangeHandler::Shutdown(void)
	{
		return;
	}
}