#include "MediumChangeHandler.hpp"

namespace olib
{
	MediumChangeHandler::MediumChangeHandler(void)
	{
	}

	MediumChangeHandler::~MediumChangeHandler(void)
	{
		// Nothing to clean up
	}

	bool MediumChangeHandler::Initialise(void)
	{
		return true;
	}

	bool MediumChangeHandler::Run(void)
	{
		return true;
	}

	void MediumChangeHandler::Shutdown(void)
	{
		return;
	}
}