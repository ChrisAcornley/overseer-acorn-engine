#include "LargeChangeHandler.hpp"

namespace olib
{
	LargeChangeHandler::LargeChangeHandler(void)
	{
	}

	LargeChangeHandler::~LargeChangeHandler(void)
	{
		// Nothing to clean up
	}

	bool LargeChangeHandler::Initialise(void)
	{
		return true;
	}

	bool LargeChangeHandler::Run(void)
	{
		return true;
	}

	void LargeChangeHandler::Shutdown(void)
	{
		return;
	}
}