#include "SmallChangeHandler.hpp"

namespace olib
{
	SmallChangeHandler::SmallChangeHandler(void)
	{
	}

	SmallChangeHandler::~SmallChangeHandler(void)
	{
		// Nothing to clean up
	}

	bool SmallChangeHandler::Initialise(void)
	{
		return true;
	}

	bool SmallChangeHandler::Run(void)
	{
		return true;
	}

	void SmallChangeHandler::Shutdown(void)
	{
		return;
	}
}