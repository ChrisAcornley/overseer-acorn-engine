#ifndef _OVERSEER_LIBRARY_HPP_
#define _OVERSEER_LIBRARY_HPP_

//////////////
// INCLUDES //
//////////////
#include "Fuzzy Classes\FuzzyHandlerClass.hpp"
#include "Fuzzy Classes\FuzzyInferenceClass.hpp"
#include "Structures\Structures.hpp"
#include "Change Handlers\MinorChangeHandler.hpp"
#include "Maze Generator\MazeGenerator.hpp"
#include <stdlib.h>
#include <time.h>

namespace olib
{
	class OverseerLibrary {
		//////////////////////
		// PUBLIC FUNCTIONS //
		//////////////////////
		public:
			// Constructor
			OverseerLibrary(void);
			// Deconstructor
			~OverseerLibrary(void);
			bool Initialise(int** p_Map, Vector2 p_MapSize);
			bool Run(Vector2 p_PlayerMapPosition, float p_FrameTime);
			void Shutdown(void);
			int** GetMapData(void) { return m_CurrentMap; }
			bool IsTestChange(void) { return m_ChangeTest; }
			std::string GetResponce(void) { return m_Response; }

		///////////////////////
		// PRIVATE FUNCTIONS //
		///////////////////////
		private:
			bool CheckIfToMakeChange(float p_FrameTime);
			OverseerDecision::OVSDECISION DecideChange(void);
			void GenerateNewMaze(OverseerDecision::OVSDECISION p_Decision);

		///////////////////
		// PRIVATE TYPES //
		///////////////////
		private:
			int** m_CurrentMap;
			Vector2 m_PlayerMapPosition;
			bool m_ChangeTest;
			Vector2 m_ExitPosition;
			Vector2 m_MapSize;
			float m_OverseerElapsedTime;
			float m_TimeElapseToChange;
			float m_TimeSinceLastChange;
			int m_NumberOfChanges;
			std::stack<Vector2> m_PathStack;
			Vector2 m_MinorBlockChange;
			MinorChangeHandler *m_MinorChangeHandler;
			int m_PreviousPathLength;
			FuzzyInferenceClass *m_FuzzyInterface;
			std::string m_ResponsesArray[18];
			std::string m_Response;
	};
}

#endif