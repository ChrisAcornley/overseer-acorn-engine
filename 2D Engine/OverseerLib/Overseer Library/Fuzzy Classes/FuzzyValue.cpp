#include "FuzzyValue.hpp"

namespace olib
{
	FuzzyValue::FuzzyValue(void)
	{
		m_StartPoint = 0.0f;
		m_EndPoint = 0.0f;
		m_StartFull = 0.0f;
		m_EndFull = 0.0f;
	}

	FuzzyValue::~FuzzyValue(void)
	{
		// Nothing to clean up
	}

	bool FuzzyValue::Initialise(const float p_StartPoint, const float p_EndPoint, const float p_StartFull, const float p_EndFull)
	{
		m_StartPoint = p_StartPoint;
		m_EndPoint = p_EndPoint;
		m_StartFull = p_StartFull;
		m_EndFull = p_EndFull;

		if(!IsValueValid()) {
			return false;
		}

		return true;
	}

	bool FuzzyValue::IsValueValid(void)
	{
		if((m_StartPoint >= 0.0f) && (m_StartPoint <= m_EndPoint) && (m_EndPoint <= 1.0f)) {
			if((m_StartFull >= m_StartPoint) && (m_StartFull <= m_EndFull) && (m_EndFull <= m_EndPoint)) {
				// State fuzzy value is valid
				return true;
			}
		}
		
		// State current fuzzy value is invalid (will not work with inference system)
		return false;
	}
}
