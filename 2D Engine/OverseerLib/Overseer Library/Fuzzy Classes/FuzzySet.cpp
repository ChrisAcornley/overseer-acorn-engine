#include "FuzzySet.hpp"

namespace olib
{
	FuzzySet::FuzzySet(void)
	{
		m_NumberOfFuzzyValues = 0;
		m_SetRangeBegin = 0.0f;
		m_SetRangeEnd = 0.0f;
		m_FuzzyValueContainer = 0;
	}

	FuzzySet::~FuzzySet(void)
	{
	}

	bool FuzzySet::Initialise(const int p_NumberOfFuzzyValues, const float p_StartRange, const float p_EndRange)
	{
		m_NumberOfFuzzyValues = p_NumberOfFuzzyValues;
		m_SetRangeBegin = p_StartRange;
		m_SetRangeEnd = p_EndRange;

		if(p_NumberOfFuzzyValues <= 0) {
			return false;
		}

		m_FuzzyValueContainer = new FuzzyValue[m_NumberOfFuzzyValues];
		if(!m_FuzzyValueContainer) {
			return false;
		}

		return true;
	}

	void FuzzySet::AddInFuzzyValue(FuzzyValue p_FuzzyValue, const int p_Index)
	{
		m_FuzzyValueContainer[p_Index].Initialise(p_FuzzyValue.GetStartPoint(), p_FuzzyValue.GetEndPoint(), p_FuzzyValue.GetStartFull(), p_FuzzyValue.GetEndFull());
	}

	DataOutput FuzzySet::Output(const float p_CrispValue)
	{
		DataOutput l_Data(p_CrispValue, m_NumberOfFuzzyValues);

		// Get value for crisp value for each fuzzy value in the set
		// Apply to the dataoutput class
		for(int i = 0; i < m_NumberOfFuzzyValues; i++) {
			float l_Value = GetMembershipValue(p_CrispValue, i);
			l_Data.m_MembershipValues[i] = l_Value;
		}

		// return the DataOutput
		return l_Data;
	}

	float FuzzySet::GetMembershipValue(const float p_CrispValue, const int p_Index)
	{
		float l_RealStartPoint = GetRealFuzzyValue(m_FuzzyValueContainer[p_Index].GetStartPoint());
		float l_RealEndPoint = GetRealFuzzyValue(m_FuzzyValueContainer[p_Index].GetEndPoint());
		float l_RealStartFull = GetRealFuzzyValue(m_FuzzyValueContainer[p_Index].GetStartFull());
		float l_RealEndFull = GetRealFuzzyValue(m_FuzzyValueContainer[p_Index].GetEndFull());

		// Check crisp value is within range of fuzzy value
		// Check if within the "full marks"
		if((p_CrispValue >= l_RealStartFull) && (p_CrispValue <= l_RealEndFull)) {
			return 1.0f;
		}

		// If between start and start full
		if((p_CrispValue > l_RealStartPoint) && (p_CrispValue < l_RealStartFull)) {
			// Calculate where it intersects with the line
			// Generate line equation y = mx + c
			float l_Gradiant = 1/(l_RealStartFull - l_RealStartPoint);
			float l_C = 1 - (l_Gradiant*l_RealStartFull);

			// find the membership value
			float l_Result = (l_Gradiant*p_CrispValue) + l_C;
			
			// return
			return l_Result;
		}

		// If between end full and end
		if((p_CrispValue > l_RealEndFull) && (p_CrispValue < l_RealEndPoint)) {
			// Calculate where it intersects with the line
			// Generate line equation y = mx + c
			float l_Gradiant = 1/(l_RealEndFull - l_RealEndPoint);
			float l_C = 1 - (l_Gradiant*l_RealEndFull);

			// find the membership value
			float l_Result = (l_Gradiant*p_CrispValue) + l_C;
			
			// return
			return l_Result;
		}

		// Return no membership if all checks above fail - Therefore outside the ranges
		return 0.0f;
	}

	float FuzzySet::GetRealFuzzyValue(const float p_Value)
	{
		// Convert Fuzzy Value into a value representing the range
		float l_Result = m_SetRangeEnd - m_SetRangeBegin;

		l_Result = p_Value*l_Result;

		l_Result = l_Result + m_SetRangeBegin;

		return l_Result;
	}
}