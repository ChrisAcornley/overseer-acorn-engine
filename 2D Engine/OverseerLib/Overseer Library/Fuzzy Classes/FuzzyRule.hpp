#ifndef _FUZZY_RULE_HPP_
#define _FUZZY_RULE_HPP_

namespace olib
{
	class  ANDFuzzyRule
	{
	public:
		ANDFuzzyRule(void) { /* Do Nothing */ }
		~ANDFuzzyRule(void) { /* Do Nothing */ }
		float RunRule(const float p_InputOne, const  float p_InputTwo);
	};

	class  ORFuzzyRule
	{
	public:
		ORFuzzyRule(void) { /* Do Nothing */ }
		~ORFuzzyRule(void) { /* Do Nothing */ }
		float RunRule(const float p_InputOne, const  float p_InputTwo);
	};

	class  NOTFuzzyRule
	{
	public:
		NOTFuzzyRule(void) { /* Do Nothing */ }
		~NOTFuzzyRule(void) { /* Do Nothing */ }
		float RunRule(const float p_Input);
	};
}

#endif