#include "FuzzyInferenceClass.hpp"

namespace olib
{
	FuzzyInferenceClass::FuzzyInferenceClass(void)
	{
		// Set pointers to Null
		m_ElapsedTimeSet = 0;
		m_DistanceToExitSet = 0;
		m_DefuzzificationSet = 0;
		m_FuzzyHandler = 0;
		m_ANDRule = 0;
		m_ORRule = 0;
		m_NOTRule = 0;
	}

	FuzzyInferenceClass::~FuzzyInferenceClass(void)
	{
		// Nothing to clean up - Handled in shutdown
	}

	bool FuzzyInferenceClass::Initialise(void)
	{
		m_FuzzyHandler = new FuzzyHandlerClass();
		if(!m_FuzzyHandler) {
			return false;
		}

		// Create time fuzzy set - Between 0 and 300 seconds (game run time)
		m_ElapsedTimeSet = m_FuzzyHandler->CreateFuzzySet(5, 0.0f, 300.0f);
		// Create Fuzzy Values in set
		m_ElapsedTimeSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.0f, 0.2f, 0.0f, 0.17f), 0);
		m_ElapsedTimeSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.15f, 0.35f, 0.23f, 0.27f), 1);
		m_ElapsedTimeSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.34f, 0.66f, 0.48f, 0.52f), 2);
		m_ElapsedTimeSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.65f, 0.85f, 0.73f, 0.77f), 3);
		m_ElapsedTimeSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.8f, 1.0f, 0.83f, 1.0f), 4);

		// Create distance fuzzy set - Between 0 and 100 moves from the exit
		m_DistanceToExitSet = m_FuzzyHandler->CreateFuzzySet(7, 0.0f, 100.0f);
		m_DistanceToExitSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.0f, 0.18f, 0.09f, 0.11f), 0);
		m_DistanceToExitSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.15f, 0.33f, 0.23f, 0.25f), 1);
		m_DistanceToExitSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.3f, 0.48f, 0.38f, 0.4f), 2);
		m_DistanceToExitSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.45f, 0.55f, 0.5f, 0.5f), 3);
		m_DistanceToExitSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.52f, 0.7f, 0.6f, 0.62f), 4);
		m_DistanceToExitSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.67f, 0.85f, 0.75f, 0.77f), 5);
		m_DistanceToExitSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.82f, 1.0f, 0.9f, 0.92f), 6);

		// Create defuzzification set - Between 0 and 100 percent
		m_DefuzzificationSet = m_FuzzyHandler->CreateFuzzySet(6, 0.0f, 100.0f);
		m_DefuzzificationSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.0f, 0.40f, 0.0f, 0.2f), 0);
		m_DefuzzificationSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.3f, 0.55f, 0.4f, 0.45f), 1);
		m_DefuzzificationSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.5f, 0.7f, 0.59f, 0.61f), 2);
		m_DefuzzificationSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.65f, 0.8f, 0.62f, 0.64f), 3);
		m_DefuzzificationSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.78f, 0.9f, 0.735f, 0.745f), 4);
		m_DefuzzificationSet->AddInFuzzyValue(m_FuzzyHandler->CreateFuzzyValue(0.9f, 1.0f, 0.99f, 1.0f), 5);

		m_ANDRule = m_FuzzyHandler->CreateFuzzyANDRule();
		m_ORRule = m_FuzzyHandler->CreateFuzzyORRule();
		m_NOTRule = m_FuzzyHandler->CreateFuzzyNOTRule();

		return true;
	}

	OverseerDecision::OVSDECISION FuzzyInferenceClass::Run(const float p_ElapsedGameTime, int p_BlocksToExit)
	{
		// Elapsed Time Set Output struct
		DataOutput l_ETSOutput = m_ElapsedTimeSet->Output(p_ElapsedGameTime);
		
		// Distance To Exit Set Output struct
		DataOutput l_DTESOutput = m_DistanceToExitSet->Output((float)p_BlocksToExit);

		// Create input data array
		float *l_InputData = new float[m_DefuzzificationSet->GetNumberOfValuesInSet()];
		if(!l_InputData) {
			l_InputData = 0;
			return OverseerDecision::OVS_NONE;
		}

		for(int i = 0; i < m_DefuzzificationSet->GetNumberOfValuesInSet(); i++) {
			l_InputData[i] = 0.0f;
		}

		// Perform rules
		// MINOR
		// if very far and very short
		l_InputData[0] += m_ORRule->RunRule(l_ETSOutput.m_MembershipValues[0], l_DTESOutput.m_MembershipValues[6]);
		// if very far and short
		l_InputData[0] += m_ORRule->RunRule(l_ETSOutput.m_MembershipValues[1], l_DTESOutput.m_MembershipValues[6]);
		// if far and very short
		l_InputData[0] += m_ORRule->RunRule(l_ETSOutput.m_MembershipValues[0], l_DTESOutput.m_MembershipValues[5]);
		// if far and short
		l_InputData[0] += m_ORRule->RunRule(l_ETSOutput.m_MembershipValues[1], l_DTESOutput.m_MembershipValues[5]);

		// SMALL
		// if (far or medium) && short or medium
		l_InputData[1] += m_ORRule->RunRule(m_ORRule->RunRule(	l_DTESOutput.m_MembershipValues[5],
																l_DTESOutput.m_MembershipValues[4]),
											 m_ORRule->RunRule(	l_ETSOutput.m_MembershipValues[1],
																l_ETSOutput.m_MembershipValues[2]));

		// MEDUM
		l_InputData[2] += m_ORRule->RunRule(l_DTESOutput.m_MembershipValues[2],m_ANDRule->RunRule(l_ETSOutput.m_MembershipValues[3], l_ETSOutput.m_MembershipValues[1]));
		l_InputData[2] += m_ORRule->RunRule(l_DTESOutput.m_MembershipValues[4],m_ANDRule->RunRule(l_ETSOutput.m_MembershipValues[3], l_ETSOutput.m_MembershipValues[1]));

		// LARGE
		l_InputData[3] += m_ORRule->RunRule(m_ORRule->RunRule(l_ETSOutput.m_MembershipValues[1], l_ETSOutput.m_MembershipValues[3]), l_DTESOutput.m_MembershipValues[3]);

		// EXTREME 
		l_InputData[4] += m_ORRule->RunRule(l_ETSOutput.m_MembershipValues[3], l_DTESOutput.m_MembershipValues[2]);

		// Add 0.1 to NONE just incase no other values work out
		l_InputData[5] += m_NOTRule->RunRule(l_ETSOutput.m_MembershipValues[4]);
		
		// Get defuzzification results
		float l_Result = 0.0f;
		l_Result = m_FuzzyHandler->Defuzzification(m_DefuzzificationSet, l_InputData);
		// See what result this is
		// 90 - 
		if(l_Result < 30.0f) {
			return OverseerDecision::OVS_MINOR;
		}
		else if ((l_Result >= 30.0f) && (l_Result < 50.0f)) {
			return OverseerDecision::OVS_SMALL;
		}
		else if ((l_Result >= 50.0f) && (l_Result < 65.0f)) {
			return OverseerDecision::OVS_MEDIUM;
		}
		else if ((l_Result >= 65.0f) && (l_Result < 78.0f)) {
			return OverseerDecision::OVS_LARGE;
		}
		else if ((l_Result >= 78.0f) && (l_Result < 90.0f)) {
			return OverseerDecision::OVS_EXTREME;
		}
		else if ((l_Result >= 90.0f)) {
			return OverseerDecision::OVS_NONE;
		}

		// Return none if not any of the above
		return OverseerDecision::OVS_NONE;
	}

	void FuzzyInferenceClass::Shutdown(void)
	{
		if(m_ElapsedTimeSet) {
			delete m_ElapsedTimeSet;
			m_ElapsedTimeSet = 0;
		}

		if(m_DistanceToExitSet) {
			delete m_DistanceToExitSet;
			m_DistanceToExitSet = 0;
		}

		if(m_DefuzzificationSet) {
			delete m_DefuzzificationSet;
			m_DefuzzificationSet = 0;
		}

		if(m_FuzzyHandler) {
			delete m_FuzzyHandler;
			m_FuzzyHandler = 0;
		}

		if(m_ANDRule) {
			delete m_ANDRule;
			m_ANDRule = 0;
		}

		if(m_ORRule) {
			delete m_ORRule;
			m_ORRule = 0;
		}

		if(m_NOTRule) {
			delete m_NOTRule;
			m_NOTRule = 0;
		}

		return;
	}
}