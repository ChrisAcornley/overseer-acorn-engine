#ifndef _FUZZY_HANDLER_CLASS_HPP_
#define _FUZZY_HANDLER_CLASS_HPP_

#include "FuzzyValue.hpp"
#include "FuzzySet.hpp"
#include "FuzzyRule.hpp"

namespace olib
{
	class FuzzyHandlerClass
	{
		public:
			FuzzyHandlerClass(void);
			~FuzzyHandlerClass(void);
			FuzzyValue CreateFuzzyValue(const float p_StartPoint, const float p_EndPoint, const float p_StartFull, const float p_EndFull);
			FuzzySet* CreateFuzzySet(const int p_NumberOfFuzzyValues, const float p_StartRange, const float p_EndRange);
			ANDFuzzyRule* CreateFuzzyANDRule(void);
			ORFuzzyRule* CreateFuzzyORRule(void);
			NOTFuzzyRule* CreateFuzzyNOTRule(void);
			float Defuzzification(FuzzySet* p_DefuzzificationSet, float* p_SetInputs);
	};
}

#endif