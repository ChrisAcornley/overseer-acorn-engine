#ifndef _FUZZY_INFERENCE_CLASS_HPP_
#define _FUZZY_INFERENCE_CLASS_HPP_

#include "FuzzyHandlerClass.hpp"

namespace OverseerDecision
{
	enum OVSDECISION { OVS_NONE = 0, OVS_MINOR = 1, OVS_SMALL = 2, OVS_MEDIUM = 3, OVS_LARGE = 4, OVS_EXTREME = 5, OVS_INVALID = 6 };
}

namespace olib 
{
	class FuzzyInferenceClass
	{
	public:
		FuzzyInferenceClass(void);
		~FuzzyInferenceClass(void);
		bool Initialise(void);
		OverseerDecision::OVSDECISION Run(const float p_ElapsedGameTime, int p_BlocksToExit);
		void Shutdown(void);

	private:
		FuzzySet *m_ElapsedTimeSet;
		FuzzySet *m_DistanceToExitSet;
		FuzzySet *m_DefuzzificationSet;
		FuzzyHandlerClass *m_FuzzyHandler;
		ANDFuzzyRule *m_ANDRule;
		ORFuzzyRule *m_ORRule;
		NOTFuzzyRule *m_NOTRule;
	};
}

#endif