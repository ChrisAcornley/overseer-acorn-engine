#include "FuzzyHandlerClass.hpp"

namespace olib
{
	FuzzyHandlerClass::FuzzyHandlerClass(void)
	{
	}

	FuzzyHandlerClass::~FuzzyHandlerClass(void)
	{
	}

	FuzzyValue FuzzyHandlerClass::CreateFuzzyValue(const float p_StartPoint, const float p_EndPoint, const float p_StartFull, const float p_EndFull)
	{
		FuzzyValue l_LocalFuzzyValue;

		l_LocalFuzzyValue.Initialise(p_StartPoint, p_EndPoint, p_StartFull, p_EndFull);

		return l_LocalFuzzyValue;
	}

	FuzzySet* FuzzyHandlerClass::CreateFuzzySet(const int p_NumberOfFuzzyValues, const float p_StartRange, const float p_EndRange)
	{
		FuzzySet *l_LocalFuzzySet = new FuzzySet();
		if(!l_LocalFuzzySet) {
			return 0;
		}

		if(!l_LocalFuzzySet->Initialise(p_NumberOfFuzzyValues, p_StartRange, p_EndRange)) {
			delete l_LocalFuzzySet;
			l_LocalFuzzySet = 0;
			return 0;
		}

		return l_LocalFuzzySet;
	}

	ANDFuzzyRule* FuzzyHandlerClass::CreateFuzzyANDRule(void)
	{
		ANDFuzzyRule *l_LocalRule = new ANDFuzzyRule();
		if(!l_LocalRule) {
			return 0;
		}

		return l_LocalRule;
	}

	ORFuzzyRule* FuzzyHandlerClass::CreateFuzzyORRule(void)
	{
		ORFuzzyRule *l_LocalRule = new ORFuzzyRule();
		if(!l_LocalRule) {
			return 0;
		}

		return l_LocalRule;
	}

	NOTFuzzyRule* FuzzyHandlerClass::CreateFuzzyNOTRule(void)
	{
		NOTFuzzyRule *l_LocalRule = new NOTFuzzyRule();
		if(!l_LocalRule) {
			return 0;
		}

		return l_LocalRule;
	}

	float FuzzyHandlerClass::Defuzzification(FuzzySet* p_DefuzzificationSet, float* p_SetInputs)
	{
		float l_TotalArea = 0.0f;
		FuzzyValue l_CurrentValue;

		float *l_XValues = new float[p_DefuzzificationSet->GetNumberOfValuesInSet()];
		float *l_UnitAreas = new float[p_DefuzzificationSet->GetNumberOfValuesInSet()];

		for(int i = 0; i < p_DefuzzificationSet->GetNumberOfValuesInSet(); i++) {
			l_XValues[i] = 0.0f;
			l_UnitAreas[i] = 0.0f;
		}

		if(p_DefuzzificationSet && p_SetInputs) {
			// Area under graph
			for(int i = 0 ; i < p_DefuzzificationSet->GetNumberOfValuesInSet(); i++) {
				// Go through each value in the set and find the area under the curve (ignore possible overlap)
				if(p_SetInputs[i] > 0) {
					FuzzyValue l_CurrentValue = p_DefuzzificationSet->GetFuzzyValue(i);
					// Figure points membership intersects with function
					// construct line equations
					float t1 = (l_CurrentValue.GetStartFull() - l_CurrentValue.GetStartPoint());
					float g1 = 0.0f;
					if(t1 > 0) {
						g1 = 1/t1;
					}
					else {
						g1 = 0.0f;
					}
					float c1 = 0 - g1*l_CurrentValue.GetStartPoint();

					float t2 = (l_CurrentValue.GetEndPoint() - l_CurrentValue.GetEndFull());
					float g2 = 0.0f;
					if(t2 < 0) {
						g2 = -(1/t2);
					}
					else {
						g2 = 0.0f;
					}
					float c2 = 0 - g2*l_CurrentValue.GetEndPoint();

					float intersectX1 = 0.0f;
					if(g1 > 0) {
						intersectX1 = ((p_SetInputs[i] - c1)/g1);
					}
					else {
						intersectX1 = l_CurrentValue.GetStartFull();
					}
					float intersectX2 = 0.0f;
					if(g2 < 0) {
						intersectX2 = ((p_SetInputs[i] - c2)/g2);
					}
					else {
						intersectX2 = l_CurrentValue.GetEndFull();
					}
					// Use those points to map out area under graph
					// Determine area and centroids for each of the three shapes
					float area1 = ((intersectX1 - l_CurrentValue.GetStartPoint())*p_SetInputs[i])/2;
					float x1 = l_CurrentValue.GetStartPoint() + ((2*(intersectX1 - l_CurrentValue.GetStartPoint()))/3);

					float area2 = ((intersectX2 - intersectX1)*p_SetInputs[i]);
					float x2 = intersectX1 + (0.5f*(intersectX2 - intersectX1));

					float area3 = ((l_CurrentValue.GetEndPoint() - intersectX2)*p_SetInputs[i])/2;
					float x3 = intersectX2 + ((2.0f*(l_CurrentValue.GetEndPoint() - intersectX2))/3);


					// X*Area / Area
					
					float finalX = (x1*area1 + x2*area2 + x3*area3)/(area1+area2+area3);
					// Add data to arrays
					l_XValues[i] = finalX;
					l_UnitAreas[i] = area1+area2+area3;

					l_TotalArea += l_UnitAreas[i];
				}
				else {
					l_XValues[i] = 0.0f;
					l_UnitAreas[i] = 0.0f;
				}
			}
			// Find true centroid
			float topX = 0.0f;
			float topY = 0.0f;
			for(int i = 0; i < p_DefuzzificationSet->GetNumberOfValuesInSet(); i++) {
				topX += l_XValues[i]*l_UnitAreas[i];
				topY += p_SetInputs[i]*l_UnitAreas[i];
			}

			float finalcentroidX = topX/l_TotalArea;
			float finalcentroidY = topY/l_TotalArea;
		
			if(l_TotalArea == 0.0f) {
				return p_DefuzzificationSet->GetRangeBeginning();
			}

			return p_DefuzzificationSet->GetRangeBeginning() + (finalcentroidX*(p_DefuzzificationSet->GetRangeEnding() - p_DefuzzificationSet->GetRangeBeginning()));
		}

		return p_DefuzzificationSet->GetRangeBeginning();
	}
}