#ifndef _FUZZY_SET_HPP_
#define _FUZZY_SET_HPP_

#include "FuzzyValue.hpp"
#include <string>

namespace olib
{
	struct DataOutput
	{
		float m_CrispValue;
		float *m_MembershipValues;
		int m_NumberOfValues;

		DataOutput() : m_CrispValue(0),
					   m_MembershipValues(0),
					   m_NumberOfValues(0)
		{
		}

		DataOutput(const float p_CrispValue, const int p_NumberOfValues) {
			m_NumberOfValues = p_NumberOfValues;
			m_CrispValue = p_CrispValue;

			m_MembershipValues = new float[m_NumberOfValues];
			
			for(int i = 0; i < m_NumberOfValues; i++) {
				m_MembershipValues[i] = 0;
			}
		}

		void Close() {
			if(m_MembershipValues) {
				delete [] m_MembershipValues;
				m_MembershipValues = 0;
			}
		}
	};

	class FuzzySet
	{
	public:
		FuzzySet(void);
		~FuzzySet(void);
		bool Initialise(const int p_NumberOfFuzzyValues, const float p_StartRange, const float p_EndRange);
		void AddInFuzzyValue(const FuzzyValue p_FuzzyValue, const int p_Index);
		float GetRangeBeginning(void) { return m_SetRangeBegin; }
		float GetRangeEnding(void) { return m_SetRangeEnd; }
		int GetNumberOfValuesInSet(void) { return m_NumberOfFuzzyValues; }
		DataOutput Output(const float p_CrispValue);
		FuzzyValue GetFuzzyValue(int p_Index) { return m_FuzzyValueContainer[p_Index]; }

	private:
		inline float GetMembershipValue(const float p_CrispValue, const int p_Index);
		inline float GetRealFuzzyValue(const float p_Value);

	private:
		FuzzyValue* m_FuzzyValueContainer;
		int m_NumberOfFuzzyValues;
		float m_SetRangeBegin;
		float m_SetRangeEnd;
	};
}

#endif