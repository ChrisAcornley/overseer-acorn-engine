#ifndef _FUZZY_VALUE_HPP_
#define _FUZZY_VALUE_HPP_

namespace olib
{
	class FuzzyValue
	{
	public:
		FuzzyValue(void);
		~FuzzyValue(void);
		bool Initialise(const float p_StartPoint, const float p_EndPoint, const float p_StartFull, const float p_EndFull);

		// Manual set functions (this is usually handled by the Initialise function)
		void SetStartPoint(const float p_StartPoint) { m_StartPoint = p_StartPoint; }
		void SetEndPoint(const float p_EndPoint) { m_EndPoint = p_EndPoint; }
		void SetStartFull(const float p_StartFull) { m_StartFull = p_StartFull; }
		void SetEndFull(const float p_EndFull) { m_EndFull = p_EndFull; }

		// Get functions
		float GetStartPoint(void) { return m_StartPoint; }
		float GetEndPoint(void) { return m_EndPoint; }
		float GetStartFull(void) { return m_StartFull; }
		float GetEndFull(void) { return m_EndFull; }

	private:
		bool IsValueValid(void);

	private:
		float m_StartPoint;
		float m_EndPoint;
		float m_StartFull;
		float m_EndFull;
	};
}

#endif