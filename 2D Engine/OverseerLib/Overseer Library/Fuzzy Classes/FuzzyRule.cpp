#include "FuzzyRule.hpp"

// https://www.calvin.edu/~pribeiro/othrlnks/Fuzzy/fuzzyops.htm - 24-04-2014 - 10:02

namespace olib
{
	float ANDFuzzyRule::RunRule(const float p_InputOne,const  float p_InputTwo)
	{
		// Get minimum
		if(p_InputOne > p_InputTwo) {
			return p_InputTwo;
		}
		else {
			return p_InputOne;
		}
	}

	float ORFuzzyRule::RunRule(const float p_InputOne,const  float p_InputTwo)
	{
		// Get maximum
		if(p_InputOne < p_InputTwo) {
			return p_InputTwo;
		}
		else {
			return p_InputOne;
		}
	}

	float NOTFuzzyRule::RunRule(const float p_Input)
	{
		// Get
		return (1.0f - p_Input);
	}
}