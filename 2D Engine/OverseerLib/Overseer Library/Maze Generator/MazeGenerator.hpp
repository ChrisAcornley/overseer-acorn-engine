/*
 *	Author: Christopher Acornley
 *	Use:	Generates a 2D Maze of designated size an writes it into the
 *			current maze.
 *	Notes:	Use a randomized Prim's algorithm to generate the new maze.
 *			Maze data is kept in the form of a 2D array and passed between
 *			functions and classes through a pointer to an array, each 
 *			containing a pointer to another array.
 */
#ifndef _MAZE_GENERATOR_HPP_
#define _MAZE_GENERATOR_HPP_

//////////////
// INCLUDES //
//////////////
#include "../Structures/Structures.hpp"
#include <stack>

// Namespace to contain the number of attemps the generator should make before quitting
// This is kept in a seperate namespace as this data is only needed by this class so
// doesn't need to be in 'olib' namespace and keeps it out of global namespace
namespace Attempts
{
	const static int MaximumNumberOfAttempts = 3;
}

// Library Namespace
namespace olib
{
	// Class - Maze Generator
	class MazeGenerator
	{
		//////////////////////
		// PUBLIC FUNCTIONS //
		//////////////////////
		public:
			// Constructor
			MazeGenerator(void);
			// Deconstructor
			~MazeGenerator(void);
			// Generate and returns the new maze
			int** GenerateMaze(int** p_CurrentMaze, Vector2 p_TargetSize, Vector2 p_MapSize, Vector2 p_PlayerMapPosition, Vector2 p_ExitPosition);
		
		private:
			// Handles the positioning, creation and validating of new mazes
			bool CreateNewMap(void);
			// Handles the actual maze generation
			bool GenMaze(void);
			// Positions the generated maze within the current one 
			void PositionMap(void);
			// Checks if the maze is a valid (there is an path from the users
			// current position to the exit block).
			bool CheckIfValid(void);
			// Finds all valid moves for a node during maze generation
			MoveFindData FindValidMoves(Vector2 p_CurrentPosition);
			// Adds selected node into the map and list for further searches
			void AddToListAndMap(Vector2 p_TargetVector, std::list<Vector2>& p_TargetList);
			// Writes the generated maze into the current one
			void WriteInGenMap(void);
			// Connects paths outside the generted maze with ones inside it
			void ConnectUpPaths(void);

		private:
			int** m_CurrentMap;				// Pointer to a pointer array of the current maze
			int** m_GenMap;					// Pointer to pointer array for the genereted maze
			std::stack<Vector2> m_TestPath;	// Container of tile moves from the players position to the exit block
			Vector2 m_GenMapSize;			// Size of the generated maze
			Vector2 m_GenMapStartPoint;		// Position of starting node inside the generated maze
			Vector2 m_GenMapEndPoint;		// Position of ending node inside the generated maze - Only needed if exit block is within generated maze
			Vector2 m_RealMapSize;			// Size of the current maze
			Vector2 m_InitialDrawPosition;	// The draw position to start writing the generated maze into the current one
			Vector2 m_RealExitPosition;		// The position of the exit block inside the current maze		
			Vector2 m_RealPlayerPosition;	// The position of the player in the current map
			bool m_FullSize;				// Flag to check if the generated maze is the same size as the current maze - Therefore replacing curret maze
			bool m_ExitPointInMap;			// Flag to check if the exit block is in the generated maz
			

	}; //== End of MazeGenerator ==//
} //== End of olib ==//

#endif