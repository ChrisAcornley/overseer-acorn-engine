//////////////
// INCLUDES //
//////////////
#include "MazeGenerator.hpp"
#include <time.h>
#include <list>

// Library namespace
namespace olib
{
	/*
	 * Name:		MazeGenerator
	 ==========================================================================
	 * Arguments:	None
	 * Returns:		None
	 * Notes:		None
	 */
	MazeGenerator::MazeGenerator(void)
	{
		// Set the pointer to be 0
		m_GenMap = 0;
		m_CurrentMap = 0;

		// Set the full size map flag to false
		m_FullSize = false;
		
		// State the exit is not within the map
		m_ExitPointInMap = false;

	}

	/*
	 * Name:		~MazeGenerator
	 ==========================================================================
	 * Arguments:	None
	 * Returns:		None
	 * Notes:		None
	 */
	MazeGenerator::~MazeGenerator(void)
	{
		// Nothing to clean up
	}

	/*
	 * Name:		GenerateMaze
	 ==========================================================================
	 * Arguments:	p_CurrentMaze		- Array containing current maze data
	 *				p_TargetSize		- Desired size of generated maze
	 *				p_MapSize			- Size of current maze
	 *				p_PlayerMapPosition	- Players position in the current maze
	 *				p_ExitPosition		- Position of the exit block in the
	 *									  current maze
	 *
	 * Returns:		Pointer to an array containing pointers to arrays (all int)
	 *				which holds the maze data.
	 *
	 * Notes:		Doesn't actually generate the maze itself, but sorts out 
	 *				argument data and checks everything is valid first before
	 *				starting the actual maze generation.
	 */
	int** MazeGenerator::GenerateMaze(int** p_CurrentMaze, Vector2 p_TargetSize, Vector2 p_MapSize, Vector2 p_PlayerMapPosition, Vector2 p_ExitPosition)
	{
		// Save data into the class
		m_RealMapSize = p_MapSize;
		m_GenMapSize = p_TargetSize;
		m_GenMapStartPoint = p_PlayerMapPosition;
		m_RealExitPosition = p_ExitPosition;
		m_CurrentMap = p_CurrentMaze;
		m_RealPlayerPosition = p_PlayerMapPosition;

		// Create a new map array that is the size desired
		m_GenMap = new int *[m_GenMapSize.y];
		for(int i = 0; i < m_GenMapSize.y; i++) {
			m_GenMap[i] = new int[m_GenMapSize.x];
		}

		// Check the maze pointer is valid and return 0 if invalid
		if(!m_GenMap) {
			return 0;
		}

		// Fill the array with '1' - represents wall block
		for(int i = 0; i < m_GenMapSize.y; i++) {
			for(int j = 0; j < m_GenMapSize.x; j++) {
				m_GenMap[j][i] = 1;
			}
		}

		// Try and generate a new map and return 0 if an error occurs
		if(!CreateNewMap()) {
			return 0;
		}

		// Return the new maze
		return m_CurrentMap;

	} //== End of GenerateMaze ==//

	/*
	 * Name:		CreateNewMap
	 ==========================================================================
	 * Arguments:	None
	 *
	 * Returns:		Boolean flag	- True means the function completed
	 *								  succesfully
	 *								- False means an error occured
	 *
	 * Notes:		Again doesn't actual create the maze, but calls the
	 *				positioning, creation and validating functions in order
	 *				and attemps to do so three times unless a valid maze is
	 *				found. Very unlikely but there as precaution.
	 */
	bool MazeGenerator::CreateNewMap(void)
	{
		// Create a counter for the number of attempts
		int l_MapGenCounter = 0;

		// Create a flag for if a valid new map was found
		bool l_ValidFind = false;

		// While there is no valid map and there has been less than three attempts, keep trying to generate a new map
		while((!l_ValidFind) && (l_MapGenCounter < Attempts::MaximumNumberOfAttempts)) {

			// Determine the position to insert the new maze
			PositionMap();

			// Generate the new maze
			if(GenMaze()) {

				// Check the maze new maze when inserted into the old one is valid
				if(CheckIfValid()) {

					// State that the AI has found a valid maze
					l_ValidFind = true;
				}
			}

			// Add one to the number of attempts
			l_MapGenCounter++;

		}

		// Return the state of the generation
		return l_ValidFind;

	} //== End of CreateNewMap ==//

	/*
	 * Name:		GenMaze
	 ==========================================================================
	 * Arguments:	None
	 *
	 * Returns:		Boolean flag	- True means a new maze was succesfully 
	 *								  generated
	 *								- False means an error occured
	 *
	 * Notes:		Uses a randomized Prim's algorithm to generate the maze.
	 *				From the start node, the algorithm looks for any valid
	 *				moves around it, and presents these moves back to the
	 *				program. A move is selected at random and executed, with
	 *				the change being made in the map and the move being pushed
	 *				into the list of 'front line' nodes (nodes that have valid
	 *				moves left). Once all the front nodes are exhausted, then a
	 *				new maze should have been generated.
	 *
	 *				W - Wall Block
	 *				C - Current Node
	 *				V - Valid Move
	 *				M - Chosen Move
	 *
	 *				W -	W - W	Look for		W -	V - W	Choose a	W -	M - W
	 *				|	|	|	valid moves		|	|	|	move (Up	|	|	|		Apply change to
	 *				W - C - W    --------->		V - C - V   ------->	V - C - V  	--> maze and add the			
	 *				|	|	|					|	|	|	in this		|	|	|		position to list
	 *				W - W - W					W - V - W	case		W - V - W
	 */
	bool MazeGenerator::GenMaze(void)
	{
		// Create a local list to store all nodes
		std::list<Vector2> l_FrontLineList;

		// Create a local store for the current node being looked at
		Vector2 l_CurrentNode;

		// Add the player position into the list - Start point
		l_FrontLineList.push_front(m_GenMapStartPoint);

		// Set the start point in the maze to be 0 - represents a floor block
		m_GenMap[m_GenMapStartPoint.y][m_GenMapStartPoint.x] = 0;

		// If the exit point of the maze is in the map set that position to 2 - Represents the exit block - This stops it being overwritten during maze generation
		if(m_ExitPointInMap) {
			m_GenMap[m_GenMapEndPoint.y][m_GenMapEndPoint.x] = 2;
		}

		// Keep doing the following whilst there is data in the list
		while(!l_FrontLineList.empty()) {

			// Extract the top node from the list
			l_CurrentNode = l_FrontLineList.front();

			// Check the current node for all valid moves around it and save it to a local store
			MoveFindData l_Data = FindValidMoves(l_CurrentNode);

			// Check there is at least one valid move - if not then do nothing
			if(l_Data.m_NumberOfMoves > 0) {

				// Store for which move to apply
				int l_Move = 0;

				// If only one move, do that move
				if(l_Data.m_NumberOfMoves == 1) {
					l_Move = l_Data.m_Moves.front();
				}

				// Else pick a move at random
				else {

					// Seed the random number generator with current time
					std::srand((int)time(NULL));

					// Create a random number between 0 and the number of moves minus 1 (eg with 4 moves, number is between 0 and 3
					int l_Rand = std::rand() % (l_Data.m_NumberOfMoves - 1);

					// Pop the random number worth of nodes from the queue to moves
					for(int i = 0; i < l_Rand; i++) {
						l_Data.m_Moves.pop();
					}

					// Get the chosen move
					l_Move = l_Data.m_Moves.front();
				}
					
				// Pass the chosen move into a switch statement
				// Set the node the move points to (relative to the current node) and add it to the map and to the front line list
				switch(l_Move) {

					// Move = UP
					case MAZEMOVES::MOVE_UP:
					{
						AddToListAndMap(Vector2(l_CurrentNode.x, l_CurrentNode.y - 1), l_FrontLineList);
						break;
					}

					// Move = DOWN
					case MAZEMOVES::MOVE_DOWN:
					{
						AddToListAndMap(Vector2(l_CurrentNode.x, l_CurrentNode.y + 1), l_FrontLineList);
						break;
					}

					// Move = LEFT
					case MAZEMOVES::MOVE_LEFT:
					{
						AddToListAndMap(Vector2(l_CurrentNode.x - 1, l_CurrentNode.y), l_FrontLineList);
						break;
					}

					// Move = RIGHT
					case MAZEMOVES::MOVE_RIGHT:
					{
						AddToListAndMap(Vector2(l_CurrentNode.x + 1, l_CurrentNode.y), l_FrontLineList);
						break;
					}

					// Move = INVALID - Debug purpose
					case MAZEMOVES::MOVE_INVALID:
					{
						// Break from the switch statement
						break;
					}

					// Default - Incase switch input is anything other than the above
					default:
					{
						// Break from the switch statement
						break;
					}
				}
			}

			// If there are no valid moves then remove the node from the list
			else {
				l_FrontLineList.pop_front();
			}

		}//== End while loop ==//
		
		// State the maze generation was successful
		return true;

	}

	/*
	 * Name:		PositionMap
	 ==========================================================================
	 * Arguments:	None
	 *
	 * Returns:		None
	 *
	 * Notes:		Positions the generated maze correctly within the current
	 *				maze. If the generated maze is outside the limits of the 
	 *				current maze, then when it comes to writing in the new
	 *				data, it will try to write outside the 2D array that
	 *				represents the maze. This tries to position the maze with
	 *				the player at the top left hand corner if it can, otherwise
	 *				it positions it far enough from the array limits to prevent
	 *				witting into unwanted memory.
	 */
	void MazeGenerator::PositionMap(void)
	{
		// Checks if the target size is equal to the current maze size
		if((m_RealMapSize.x == m_GenMapSize.x) && (m_RealMapSize.y == m_GenMapSize.y)) {

			// Sets flags and position so the new maze will overwrite the old one
			m_FullSize = true;
			m_InitialDrawPosition.x = 0;
			m_InitialDrawPosition.y = 0;
			m_GenMapEndPoint = m_RealExitPosition;
			m_ExitPointInMap = true;
		}

		// Try to position the maze within the old maze
		else {

			// Calculate distance from edge
			Vector2 l_DistanceFromEdge;
			l_DistanceFromEdge.x = m_RealMapSize.x - m_GenMapStartPoint.x;
			l_DistanceFromEdge.y = m_RealMapSize.y - m_GenMapStartPoint.y;

			// Check if one axis of the generated maze is bigger or equal to the distance to the edge
			// If so, calculate the amount it overlaps the edge of the maze and set a new draw position 
			// If not, set the draw position to be just behind the player on said axis

			// X-Axis
			if(m_GenMapSize.x >= l_DistanceFromEdge.x) {
				int l_XDisplacement = m_GenMapSize.x - l_DistanceFromEdge.x;
				m_InitialDrawPosition.x = m_GenMapStartPoint.x - l_XDisplacement - 1;
			}
			else {
				m_InitialDrawPosition.x = m_GenMapStartPoint.x - 1;
			}

			// Y-Axis
			if(m_GenMapSize.y >= l_DistanceFromEdge.y) {
				int l_YDisplacement = m_GenMapSize.y - l_DistanceFromEdge.y;
				m_InitialDrawPosition.y = m_GenMapStartPoint.y - l_YDisplacement - 1;
				
			}
			else {
				m_InitialDrawPosition.y = m_GenMapStartPoint.y - 1;
			}

			// Check if the exit block is within the new map
			if(((m_InitialDrawPosition.x + m_GenMapSize.x) >= m_RealExitPosition.x) && ((m_InitialDrawPosition.y + m_GenMapSize.y) >= m_RealExitPosition.y)) {
				
				m_ExitPointInMap = true;

				// Set Exit position in map
				m_GenMapEndPoint.x = m_RealExitPosition.x - m_InitialDrawPosition.x;
				m_GenMapEndPoint.y = m_RealExitPosition.y - m_InitialDrawPosition.y;
			}

			// Set the point in the generated map for the start node
			m_GenMapStartPoint.x = m_GenMapStartPoint.x - m_InitialDrawPosition.x;
			m_GenMapStartPoint.y = m_GenMapStartPoint.y - m_InitialDrawPosition.y;
		}
		
		// State the function was succesful
		return;
	}

	/*
	 * Name:		CheckIfValid
	 ==========================================================================
	 * Arguments:	None
	 *
	 * Returns:		Boolean flag	- True means the function completed
	 *								  succesfully
	 *								- False means an error occured
	 *
	 * Notes:		Positions the generated maze correctly within the current
	 *				maze. If the generated maze is outside the limits of the 
	 *				current maze, then when it comes to writing in the new
	 *				data, it will try to write outside the 2D array that
	 *				represents the maze. This tries to position the maze with
	 *				the player at the top left hand corner if it can, otherwise
	 *				it positions it far enough from the array limits to prevent
	 *				witting into unwanted memory.
	 */
	bool MazeGenerator::CheckIfValid(void)
	{
		// Checks if the exit block is within the generated map
		if(m_ExitPointInMap) {

			// Check if the end point is too close to an edge
			// Check if there is a floor block two tiles in said direction
			// If previous check passes, set the block between the end tile and previous tile to be a floor block

			// Up
			if(m_GenMapEndPoint.y > 1) {
				if(m_GenMap[m_GenMapEndPoint.y - 2][m_GenMapEndPoint.x] == 0) {
					m_GenMap[m_GenMapEndPoint.y - 1][m_GenMapEndPoint.x] = 0;
				}
			}

			// Left
			if(m_GenMapEndPoint.x > 1) {
				if(m_GenMap[m_GenMapEndPoint.y][m_GenMapEndPoint.x - 2] == 0) {
					m_GenMap[m_GenMapEndPoint.y][m_GenMapEndPoint.x - 1] = 0;
				}
			}

			// Down
			if(m_GenMapEndPoint.y < (m_GenMapSize.y - 2)) {
				if(m_GenMap[m_GenMapEndPoint.y + 2][m_GenMapEndPoint.x] == 0) {
					m_GenMap[m_GenMapEndPoint.y + 1][m_GenMapEndPoint.x] = 0;
				}
			}

			// Right
			if(m_GenMapEndPoint.x < (m_GenMapSize.x - 2)) {
				if(m_GenMap[m_GenMapEndPoint.y][m_GenMapEndPoint.x + 2] == 0) {
					m_GenMap[m_GenMapEndPoint.y][m_GenMapEndPoint.x + 1] = 0;
				}
			}
		}

		// Write the generated map into the curret one
		WriteInGenMap();

		// If the generated maze is not full size, connect paths outside the maze to ones inside it
		if(!m_FullSize) {
			ConnectUpPaths();
		}

		// Perform a pathfind test to ensure the whole maze has a path from the players position to the exit
		if(!Pathfind(m_RealMapSize, m_RealPlayerPosition, m_RealExitPosition, m_CurrentMap, m_TestPath)) {
			return false;
		}

		// State the function was succesful
		return true;
	}

	/*
	 * Name:		FindValidMoves
	 ==========================================================================
	 * Arguments:	p_CurrentPosition - Current node being reviewed
	 *
	 * Returns:		MoveFindData - Struct that holds the number of valid moves
	 *							   as well as a queue container for the valid
	 *							   moves.
	 *
	 * Notes:		As the walls and floor blocks use a whole tile each,
	 *				checking for valid moves requires slightly more
	 *				calculations that shown in most reviewed literature. The
	 *				tiles around the target tile have to be checked to ensure
	 *				that it doesn't create a criss-cross of paths.
	 */
	MoveFindData MazeGenerator::FindValidMoves(Vector2 p_CurrentPosition)
	{
		// Create a local store for the data
		MoveFindData l_Data;

		// Check the the current node is far enough from the corresponding edge.
		// Check if the target tile, and the tile beyond it are wall blocks
		// Check if the tiles on one side of the previous tiles are also wall blocks
		// Check if the tiles on the other side of the first tiles are wall blocks
		// If checks pass, add one to the number of valid moves
		// Pass the move into the queue

		// Left
		if(p_CurrentPosition.x > 1) {
			if((m_GenMap[p_CurrentPosition.y][p_CurrentPosition.x - 1] == 1) && (m_GenMap[p_CurrentPosition.y][p_CurrentPosition.x - 2] == 1)) {
				if((m_GenMap[p_CurrentPosition.y - 1][p_CurrentPosition.x - 1] == 1) && (m_GenMap[p_CurrentPosition.y - 1][p_CurrentPosition.x - 2] == 1)) {
					if((m_GenMap[p_CurrentPosition.y + 1][p_CurrentPosition.x - 1] == 1) && (m_GenMap[p_CurrentPosition.y + 1][p_CurrentPosition.x - 2] == 1)) {
						l_Data.m_NumberOfMoves ++;
						l_Data.m_Moves.push(MAZEMOVES::MOVE_LEFT);
					}
				}
			}
		}

		// Up
		if(p_CurrentPosition.y > 1) {
			if((m_GenMap[p_CurrentPosition.y - 1][p_CurrentPosition.x] == 1) && (m_GenMap[p_CurrentPosition.y - 2][p_CurrentPosition.x] == 1)) {
				if((m_GenMap[p_CurrentPosition.y - 1][p_CurrentPosition.x - 1] == 1) && (m_GenMap[p_CurrentPosition.y - 2][p_CurrentPosition.x - 1] == 1)) {
					if((m_GenMap[p_CurrentPosition.y - 1][p_CurrentPosition.x + 1] == 1) && (m_GenMap[p_CurrentPosition.y - 2][p_CurrentPosition.x + 1] == 1)) {
						l_Data.m_NumberOfMoves ++;
						l_Data.m_Moves.push(MAZEMOVES::MOVE_UP);
					}
				}
			}
		}

		// Right
		if(p_CurrentPosition.x < (m_GenMapSize.x - 2)) {
			if((m_GenMap[p_CurrentPosition.y][p_CurrentPosition.x + 1] == 1) && (m_GenMap[p_CurrentPosition.y][p_CurrentPosition.x + 2] == 1)) {
				if((m_GenMap[p_CurrentPosition.y - 1][p_CurrentPosition.x + 1] == 1) && (m_GenMap[p_CurrentPosition.y - 1][p_CurrentPosition.x + 2] == 1)) {
					if((m_GenMap[p_CurrentPosition.y + 1][p_CurrentPosition.x + 1] == 1) && (m_GenMap[p_CurrentPosition.y + 1][p_CurrentPosition.x + 2] == 1)) {
						l_Data.m_NumberOfMoves ++;
						l_Data.m_Moves.push(MAZEMOVES::MOVE_RIGHT);
					}
				}
			}
		}

		// Down
		if(p_CurrentPosition.y < (m_GenMapSize.y - 2)) {
			if((m_GenMap[p_CurrentPosition.y + 1][p_CurrentPosition.x] == 1) && (m_GenMap[p_CurrentPosition.y + 2][p_CurrentPosition.x] == 1)) {
				if((m_GenMap[p_CurrentPosition.y + 1][p_CurrentPosition.x - 1] == 1) && (m_GenMap[p_CurrentPosition.y + 2][p_CurrentPosition.x - 1] == 1)) {
					if((m_GenMap[p_CurrentPosition.y + 1][p_CurrentPosition.x + 1] == 1) && (m_GenMap[p_CurrentPosition.y + 2][p_CurrentPosition.x + 1] == 1)) {
						l_Data.m_NumberOfMoves ++;
						l_Data.m_Moves.push(MAZEMOVES::MOVE_DOWN);
					}
				}
			}
		}

		// Return the data
		return l_Data;
	}

	/*
	 * Name:		AddToListAndMap
	 ==========================================================================
	 * Arguments:	p_TargetVector	- Target node vector data
	 *				p_TargetList	- List to add data to
	 *
	 * Returns:		None
	 *
	 * Notes:		Inserts the node data into a random point within the list.
	 *				Then sets the target position to be a floor tile in the 
	 *				maze.
	 */
	void MazeGenerator::AddToListAndMap(Vector2 p_TargetVector, std::list<Vector2>& p_TargetList)
	{
		// Create a list iterator starting at the beginning of the target list
		std::list<Vector2>::iterator l_Interator = p_TargetList.begin();

		// Check if the size of the list is 0 (therefore empty) and insert the node into the front of the list
		if(p_TargetList.empty()) {
			p_TargetList.push_front(p_TargetVector);
		}

		// Add the node into a random point within the list
		else {

			// Seed the random number generator with the current time
			std::srand((int)time(NULL));

			// Generate a random number between 0 and the size of the list
			int l_Rand = std::rand() % p_TargetList.size();

			// Set the size of the interator to the size of the random number
			for(int i = 0; i < l_Rand; i++) {
				l_Interator++;
			}

			// Insert the node into the list at this point
			p_TargetList.insert(l_Interator, p_TargetVector);
		}

		// Set the node position in the maze to 0 (floor block)
		m_GenMap[p_TargetVector.y][p_TargetVector.x] = 0;

		// State end of function
		return;
	}

	/*
	 * Name:		WriteInGenMap
	 ==========================================================================
	 * Arguments:	None
	 *
	 * Returns:		None
	 *
	 * Notes:		Writes the generated maze into the current maze
	 */
	void MazeGenerator::WriteInGenMap(void)
	{
		// Check the generated map pointer is valid first
		if(m_GenMap) {
			// Go through each point in the generated maze array and apply it to the current maze
			// Start from the initial draw position calculted earlier
			for(int i = 0; i < m_GenMapSize.y; i++) {
				for(int j = 0; j < m_GenMapSize.x; j++) {
					m_CurrentMap[m_InitialDrawPosition.y + i][m_InitialDrawPosition.x + j] = m_GenMap[i][j];
				}
			}
		}

		// End of function
		return;
	}

	/*
	 * Name:		ConnectUpPaths
	 ==========================================================================
	 * Arguments:	None
	 *
	 * Returns:		None
	 *
	 * Notes:		Connects up paths outside the generated maze with the ones
	 *				inside it. This is done as the generated maze has a wall of
	 *				wall blocks surrounding the outside (caused by generation
	 *				process).
	 *
	 *				F - F - F - F - F - F			F - F - F - F - F - F
	 *				|   |   |   |   |   |			|   |   |   |   |   |
	 *				F - W - w - W - W - F			F - W - W - F - W - F
	 *				|   |   |   |   |   |			|   |   |   |   |   |
	 *				F - W - W - F - W - F			F - W - W - F - F - F
	 *				|   |   |   |   |   |  =====>	|   |   |   |   |   |
	 *				F - W - F - F - W - F			F - F - F - F - W - F
	 *				|   |   |   |   |   |			|   |   |   |   |   |
	 *				F - W - W - W - W - F			F - W - W - F - W - F
	 *				|   |   |   |   |   |			|   |   |   |   |   |
	 *				F - F - F - F - F - F			F - F - F - F - F - F
	 *
	 *				It goes along each wall and checks if there is a floor
	 *				block on both sides. If there is, and there isn't a floor
	 *				block in the previous or next tile, then it turns the wall
	 *				into a floor block, connecting the path.
	 */
	void MazeGenerator::ConnectUpPaths(void)
	{
		// Determines where on the X-Axis we are looking at
		int l_LayerX = m_InitialDrawPosition.x;

		// Determines where on the Y-Axis we are looking at
		int l_LayerY = m_InitialDrawPosition.y;

		// Y-Axis value of the layer above the wall
		int l_TopPosition = m_InitialDrawPosition.y;

		// Y-Axis value of the layer below the wall
		int l_BottomPosition = m_InitialDrawPosition.y + m_GenMapSize.y - 1;

		// X-Axis value of the layer to the left of the wall
		int l_LeftPosition = m_InitialDrawPosition.x;

		// X-Axis value of the layer to the right of the wall
		int l_RightPosition = m_InitialDrawPosition.x + m_GenMapSize.x - 1;
		
		// Going along the axis
		// Check they are far enough from the edge
		// Check the if the block on either side of the wall is a floor block and the one next and previously are not
		// If all checks pass, set the position to a wall block
		// Add one to the axis counter to move to the next position

		// Along the X-Axis (top and bottom walls)
		for(int i = 0; i < m_GenMapSize.x - 2; i++) {

			// Check above
			if(m_InitialDrawPosition.y >= 3){
				if((m_CurrentMap[l_TopPosition - 1][l_LayerX] == 0) 
					 && (m_CurrentMap[l_TopPosition + 1][l_LayerX] == 0) 
					 && (m_CurrentMap[l_TopPosition][l_LayerX + 1] != 0)
					 && (m_CurrentMap[l_TopPosition][l_LayerX - 1] != 0)) {
					m_CurrentMap[l_TopPosition][l_LayerX] = 0;
				}
			}

			// Check below
			if((l_BottomPosition) <= (m_RealMapSize.y - 3) ) {
				if((m_CurrentMap[l_BottomPosition - 1][l_LayerX] == 0)
					 && (m_CurrentMap[l_BottomPosition + 1][l_LayerX] == 0)
					 && (m_CurrentMap[l_BottomPosition][l_LayerX + 1] != 0)
					 && (m_CurrentMap[l_BottomPosition][l_LayerX - 1] != 0)) {
					m_CurrentMap[l_BottomPosition][l_LayerX] = 0;
				}
			}

			l_LayerX ++;
		}

		// Along the Y-Axis (left and right walls)
		for(int i = 0; i < m_GenMapSize.y - 2; i++) {
			
			// Check left
			if(l_LeftPosition >= 3){
				if((m_CurrentMap[l_LayerY][l_LeftPosition + 1] == 0) 
					&& (m_CurrentMap[l_LayerY][l_LeftPosition - 1] == 0)
					&& (m_CurrentMap[l_LayerY + 1][l_LeftPosition] != 0)
					&& (m_CurrentMap[l_LayerY - 1][l_LeftPosition] != 0)) {
					m_CurrentMap[l_LayerY][l_LeftPosition] = 0;
				}
			}
			
			// Check right
			if((l_RightPosition) <= (m_RealMapSize.x - 3) ) {
				if((m_CurrentMap[l_LayerY][l_RightPosition + 1] == 0) 
					&& (m_CurrentMap[l_LayerY][l_RightPosition - 1] == 0)
					&& (m_CurrentMap[l_LayerY + 1][l_RightPosition] != 0)
					&& (m_CurrentMap[l_LayerY - 1][l_RightPosition] != 0)) {
					m_CurrentMap[l_LayerY][l_RightPosition] = 0;
				}
			}

			l_LayerY ++;
		}

		// End of function
		return;
	}
}//== End of olib ==//