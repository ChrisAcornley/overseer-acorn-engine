========================================================================
    STATIC LIBRARY : OverseerLib Project Overview
========================================================================
	AUTHOR:			 Christopher Acornley
========================================================================

This library is for the Overseer AI part of the project. It handles the AI and updates it and runs it accordingly. It is in a seperate library to help keep the code seperate from the main game.

There are three classes in this library so far. They are in no particular order.

OverseerLibrary - This is the main run class for the AI. It is the interaction point for the rest of the program.

FuzzyHandlerClass - This is the handler class for any fuzzy system in the AI.

MazeGenerator - This class generates a new maze for the game.
