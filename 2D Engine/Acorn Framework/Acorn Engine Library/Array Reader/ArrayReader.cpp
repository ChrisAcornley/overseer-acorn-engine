#include "ArrayReader.hpp"

namespace acfw 
{
	ArrayReader::ArrayReader(void)
	{
		m_ArrayHeight = 0;
		m_ArrayWidth = 0;
		m_ArraySizeFound = false;
	}

	ArrayReader::~ArrayReader(void)
	{
	}

	bool ArrayReader::OpenAndReadArrayFile(int **p_PassedArray, std::string p_Filename)
	{
		std::string l_CurrentLine = "";
		std::string l_Delim = ",";
		int l_FindPos = 0;
		int l_CurrentHeight = 0;
		char* l_CurrentVariable;
		l_CurrentVariable = new char;
		// Open file
		m_DataStream.open(p_Filename);

		if(!m_DataStream.is_open()) {
			return false;
		}

		while(!m_DataStream.eof()) {
			if(!m_ArraySizeFound) {
				// Read in first line and try to find array size. Width then height
				std::getline(m_DataStream, l_CurrentLine);

				l_FindPos = l_CurrentLine.find(l_Delim);

				l_CurrentLine.copy(l_CurrentVariable, l_FindPos, 0);
				l_CurrentLine.erase(0, l_FindPos + l_Delim.length());
				m_ArrayWidth = atoi(l_CurrentVariable);

				l_CurrentLine.copy(l_CurrentVariable, l_CurrentLine.length(), 0);
				l_CurrentLine.erase(0, l_CurrentLine.length());
				m_ArrayHeight = atoi(l_CurrentVariable);

				m_ArraySizeFound = true;
			}
			else {
				// Read in line
				std::getline(m_DataStream, l_CurrentLine);

				for(int i = 0; i < (m_ArrayWidth); i++) {
					// Parse numbers from that line
					l_FindPos = l_CurrentLine.find(l_Delim);
					l_CurrentLine.copy(l_CurrentVariable, l_FindPos, 0);
					l_CurrentLine.erase(0, l_FindPos + l_Delim.length());
					// Add them to array pointer
					int a = atoi(l_CurrentVariable);
					p_PassedArray[l_CurrentHeight][i] = (atoi(l_CurrentVariable)/10);
				}

				l_CurrentHeight ++;
			}		
		}

		m_DataStream.close();
		
		return true;
	}
}
