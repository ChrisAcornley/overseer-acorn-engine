#ifndef _ARRAY_READER_HPP_
#define _ARRAY_READER_HPP_

#include <fstream>
#include <string>

namespace acfw
{
	class ArrayReader
	{
	public:

		ArrayReader(void);

		~ArrayReader(void);

		bool OpenAndReadArrayFile(int **p_PassedArray, std::string p_Filename);

	private:

		int m_ArrayHeight;
		int m_ArrayWidth;
		bool m_ArraySizeFound;
		std::ifstream m_DataStream;
	};
}

#endif