/*
 * Name:	TextFileLoader.hpp 
 * Author:	Christopher Acornley
 * Date:	05-Feb-2014
 */

#ifndef _TEXT_FILE_LOADER_HPP_
#define _TEXT_FILE_LOADER_HPP_

//////////////
// INCLUDES //
//////////////
#include <fstream>
#include <string>

/////////////////////
// USED NAMESPACES //
/////////////////////
using namespace std;

// Place the following code inside the AcornEngine Framework namespace
namespace acfw
{
	/////////////
	// DEFINES //
	/////////////
	#define MAXIMUM_TEXT_LINES 256

	// This struct contains the read text data from the file. Each line in the
	// file is stored inside an array of string objects.
	struct READTEXTDATA
	{
		string m_ReadText[MAXIMUM_TEXT_LINES];
		int m_NumberOfLines;
	};	//== End of READTEXTDATA ==//

	// This class can open and read text files with the extension .txt
	class TextFileLoader
	{
		//////////////////////
		// PUBLIC FUNCTIONS //
		//////////////////////
		public:
			// Constructor
			TextFileLoader(void);
			// Deconstructor
			~TextFileLoader(void);
			// Reads a specified file
			bool ReadFile(string, READTEXTDATA&);

		///////////////////
		// PRIVATE TYPES //
		///////////////////
		private:
			ifstream m_DataStream;	// The data stream for reading the text file
			int m_CurrentLine;		// Store for the current line being read
	}; //== End of TextFileLoader ==//
}//== End of acfw ==//

#endif
