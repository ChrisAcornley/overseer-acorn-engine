// 21-Nov-2013

#ifndef _ACORN_ENGINE_HPP_
#define _ACORN_ENGINE_HPP_

#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "Text File Reader/TextFileLoader.hpp"
#include "Input Handler/InputHandler.hpp"
#include "Graphics/Sprite.hpp"
#include "Graphics/Renderer.hpp"
#include "Graphics/FontRenderer.hpp"
#include "Array Reader/ArrayReader.hpp"
#include  "Game Timer/GameTimer.hpp"

static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

namespace acfw
{
	static const int SPRITE_FULLSIZE = 999999;

	struct Vector2 {
		int x;
		int y;

		Vector2() {
			x = y = 0;
		}

		Vector2(int p_X, int p_Y) {
			x = p_X;
			y = p_Y;
		}
	};

	class AcornEngine {

		public:
			AcornEngine();
			~AcornEngine();
			bool CreateAWindow(HINSTANCE, LPCWSTR);
			bool CreateAWindow(HINSTANCE, string);
			void Shutdown();
			HWND GetWindowHandle();
			HBITMAP LoadABitmap(LPCWSTR);
			InputHandler* GetInputHandler();
			float GetXScale() { return m_XScale; }
			float GetYScale() { return m_YScale; }
			Renderer* CreateRenderer();
			Renderer* GetRenderer() { return m_Renderer; }
			FontRenderer* CreateFontRenderer(LPCWSTR p_BitmapFilePath, string p_DataFilePath);
			int GetRezSizeX() { return m_ResolutionX; }
			int GetRezSizeY() { return m_ResolutionY; }
			Sprite* CreateSprite(int p_X, int p_Y, int p_Width, int p_Height, int p_SourceWidth, int p_SourceHeight, int p_OffsetX, int p_OffsetY);
			LRESULT CALLBACK AcornProc(HWND, UINT, WPARAM, LPARAM);
			int GetAppFPS(void) { return m_FPS; }

		private:
			HINSTANCE	m_WindowInstance;
			HBITMAP		m_FrontBitmap, m_BackBitmap;
			HWND        m_WindowHandle;
			RECT		m_ScreenRect;
			HDC			m_FrontBufferContextHandle, m_BackBufferContextHandle, m_BitmapContextHandle;
			float		m_XScale, m_YScale;
			int			m_WindowWidth, m_WindowHeight;
			int			m_ResolutionX, m_ResolutionY;
			int			m_FPS;

			InputHandler	*m_Input;
			Renderer		*m_Renderer;
	};


	static AcornEngine *g_PointerToEngine = NULL;
}

#endif