// 21-Nov-2013
#include "AcornEngine.hpp"

namespace acfw 
{
	// 21-Nov-2013
	AcornEngine::AcornEngine()
	{
		m_Input = NULL;
		m_Renderer = NULL;
		g_PointerToEngine = this;
		m_FPS = 0;
	}

	// 21-Nov-2013
	AcornEngine::~AcornEngine()
	{
	}

	// 21-Nov-2013
	bool AcornEngine::CreateAWindow(HINSTANCE p_WindowInstance,  LPCWSTR p_AppName)
	{
		m_WindowInstance = p_WindowInstance;

		m_WindowWidth = 800;
		m_WindowHeight = 600;

		m_ResolutionX = 1280;
		m_ResolutionY = 720;

		m_XScale = (float)m_WindowWidth/(float)m_ResolutionX;
		m_YScale = (float)m_WindowHeight/(float)m_ResolutionY;

		WNDCLASSEX wc;
		wc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		wc.lpfnWndProc   = WndProc;
		wc.cbClsExtra    = 0;
		wc.cbWndExtra    = 0;
		wc.hInstance     = p_WindowInstance;
		wc.hIcon		 = LoadIcon(NULL, IDI_WINLOGO);
		wc.hIconSm       = wc.hIcon;
		wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wc.lpszMenuName  = NULL;
		wc.lpszClassName = p_AppName;
		wc.cbSize        = sizeof(WNDCLASSEX);

		// Register the class
		RegisterClassEx(&wc);

		m_WindowHandle = CreateWindowEx(WS_EX_APPWINDOW, 
										p_AppName, 
										p_AppName, 
										WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_SYSMENU,
										0, 
										0, 
										m_WindowWidth, 
										m_WindowHeight, 
										NULL, 
										NULL, 
										m_WindowInstance, 
										NULL);
	
		// Bring the window up on the screen and set it as main focus.
		ShowWindow(m_WindowHandle, SW_SHOW);
		SetForegroundWindow(m_WindowHandle);
		SetFocus(m_WindowHandle);

		return true;
	}

	bool AcornEngine::CreateAWindow(HINSTANCE p_WindowInstance, string p_Filename)
	{
		string l_Delim = ": ";
		string l_CurrentString;
		string l_FindString;
		int l_CurrentLine = 0;
		int l_FindPos = 0;
		bool l_CheckingStrings = true;
		LPCWSTR l_AppName;
		wstring l_TempAppNameHolder;

		m_WindowInstance = p_WindowInstance;

		// Set the window size and resolution size just in case it is not read in
		m_WindowWidth = 800;
		m_WindowHeight = 600;

		m_ResolutionX = 800;
		m_ResolutionY = 600;

		// Set app name in case it is not read in
		l_AppName = L"No Name";

		TextFileLoader l_WindowDataLoader;
		READTEXTDATA l_WindowSettingsData;
		l_WindowDataLoader.ReadFile(p_Filename, l_WindowSettingsData);

		// Parse the data from the string
		while(l_CheckingStrings) {
			// Get the current string
			l_CurrentString = l_WindowSettingsData.m_ReadText[l_CurrentLine];

			// Set the find string to "WindowName"
			l_FindString = "WindowName";
			// Try and find the target string in the current string
			l_FindPos = l_CurrentString.find(l_FindString);
			// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
			// -1 is returned so this if statement won't find.
			if(l_FindPos == 0) {
				// Find the position of the proper delim ": "
				l_FindPos = l_CurrentString.find(l_Delim);
				// Delete everything up to and including the delimc and an extra space (to include the space in the text)
				l_CurrentString.erase(0, l_FindPos + l_Delim.length());
				// Covert the string into a wstring which can be converted to LCPWSTR (which is the type past to the
				// CreateWindow() function)
				l_TempAppNameHolder = wstring(l_CurrentString.begin(), l_CurrentString.end());
				// Convert the wstring to LCPWSTR
				l_AppName = l_TempAppNameHolder.c_str();
			}

			// Set the find string to "WindowWidth"
			l_FindString = "WindowWidth";
			// Try and find the target string in the current string
			l_FindPos = l_CurrentString.find(l_FindString);
			// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
			// -1 is returned so this if statement won't find.
			if(l_FindPos == 0) {
				// Find the position of the proper delim ": "
				l_FindPos = l_CurrentString.find(l_Delim);
				// Delete everything up to and including the delimc and an extra space (to include the space in the text)
				l_CurrentString.erase(0, l_FindPos + l_Delim.length());
				// Convert the string to an integer
				// the atoi() function converts a string into its integer number
				m_WindowWidth = atoi(l_CurrentString.c_str());
			}

			// Set the find string to "WindowHeight"
			l_FindString = "WindowHeight";
			// Try and find the target string in the current string
			l_FindPos = l_CurrentString.find(l_FindString);
			// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
			// -1 is returned so this if statement won't find.
			if(l_FindPos == 0) {
				// Find the position of the proper delim ": "
				l_FindPos = l_CurrentString.find(l_Delim);
				// Delete everything up to and including the delimc and an extra space (to include the space in the text)
				l_CurrentString.erase(0, l_FindPos + l_Delim.length());
				// Convert the string to an integer
				// the atoi() function converts a string into its integer number
				m_WindowHeight = atoi(l_CurrentString.c_str());
			}

			// Set the find string to "FPS"
			l_FindString = "FPS";
			// Try and find the target string in the current string
			l_FindPos = l_CurrentString.find(l_FindString);
			// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
			// -1 is returned so this if statement won't find.
			if(l_FindPos == 0) {
				// Find the position of the proper delim ": "
				l_FindPos = l_CurrentString.find(l_Delim);
				// Delete everything up to and including the delimc and an extra space (to include the space in the text)
				l_CurrentString.erase(0, l_FindPos + l_Delim.length());
				// Convert the string to an integer
				// the atoi() function converts a string into its integer number
				m_FPS = atoi(l_CurrentString.c_str());
			}

			// Set the find string to "ResolutionX"
			l_FindString = "ResolutionX";
			// Try and find the target string in the current string
			l_FindPos = l_CurrentString.find(l_FindString);
			// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
			// -1 is returned so this if statement won't find.
			if(l_FindPos == 0) {
				// Find the position of the proper delim ": "
				l_FindPos = l_CurrentString.find(l_Delim);
				// Delete everything up to and including the delimc and an extra space (to include the space in the text)
				l_CurrentString.erase(0, l_FindPos + l_Delim.length());
				// Convert the string to an integer
				// the atoi() function converts a string into its integer number
				m_ResolutionX = atoi(l_CurrentString.c_str());
			}

			// Set the find string to "ResolutionY"
			l_FindString = "ResolutionY";
			// Try and find the target string in the current string
			l_FindPos = l_CurrentString.find(l_FindString);
			// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
			// -1 is returned so this if statement won't find.
			if(l_FindPos == 0) {
				// Find the position of the proper delim ": "
				l_FindPos = l_CurrentString.find(l_Delim);
				// Delete everything up to and including the delimc and an extra space (to include the space in the text)
				l_CurrentString.erase(0, l_FindPos + l_Delim.length());
				// Convert the string to an integer
				// the atoi() function converts a string into its integer number
				m_ResolutionY = atoi(l_CurrentString.c_str());
			}

			// Move to the next string
			l_CurrentLine++; 

			// Check if the loop has exceeded the number of lines currently filled in the array
			if(l_CurrentLine >= l_WindowSettingsData.m_NumberOfLines) {
				// Set this flag to false so the while loop ends
				l_CheckingStrings = false;
			}
		}

		m_XScale = (float)m_WindowWidth/(float)m_ResolutionX;
		m_YScale = (float)m_WindowHeight/(float)m_ResolutionY;

		WNDCLASSEX wc;
		wc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		wc.lpfnWndProc   = WndProc;
		wc.cbClsExtra    = 0;
		wc.cbWndExtra    = 0;
		wc.hInstance     = p_WindowInstance;
		wc.hIcon		 = LoadIcon(NULL, IDI_WINLOGO);
		wc.hIconSm       = wc.hIcon;
		wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wc.lpszMenuName  = NULL;
		wc.lpszClassName = l_AppName;
		wc.cbSize        = sizeof(WNDCLASSEX);

		// Register the class
		RegisterClassEx(&wc);

		m_WindowHandle = CreateWindowEx(WS_EX_APPWINDOW, 
										l_AppName, 
										l_AppName, 
										WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_SYSMENU,
										0, 
										0, 
										m_WindowWidth, 
										m_WindowHeight, 
										NULL, 
										NULL, 
										m_WindowInstance, 
										NULL);
	
		// Bring the window up on the screen and set it as main focus.
		ShowWindow(m_WindowHandle, SW_SHOW);
		SetForegroundWindow(m_WindowHandle);
		SetFocus(m_WindowHandle);

		return true;
	}

	void AcornEngine::Shutdown() 
	{
		// Remove the window handle
		DestroyWindow(m_WindowHandle);
		m_WindowHandle = NULL;

		// Remove the application instance.
		UnregisterClass((LPCWSTR)"Name", m_WindowInstance);
		m_WindowInstance = NULL;

		return;
	}

	// 21-Nov-2013
	HWND AcornEngine::GetWindowHandle()
	{
		return m_WindowHandle;
	}

	// 21-Nov-2013
	HBITMAP AcornEngine::LoadABitmap(LPCWSTR p_Filename)
	{
		// Create a bitmap handle and load in an image from the passed filename
		/*
		Load stand alone resource
		Filename of the file to be loaded
		State the image to be loaded is a bitmap
		Default width of the image is loaded
		Default height of the image is loaded
		State the resource is to be loaded from a files
		*/
		HBITMAP imageData = (HBITMAP)LoadImage(	NULL, 
												p_Filename, 
												IMAGE_BITMAP, 
												0, 
												0, 
												LR_LOADFROMFILE);

		// Check if image data was correct
		if(imageData == NULL) {
			LPCWSTR l_ConstMessage = L" failed to load.";
			wstring l_Message = wstring(p_Filename) + l_ConstMessage;
			MessageBox(m_WindowHandle, LPCWSTR(l_Message.c_str()), L"Error - MainMenuClass::LoadResources()", MB_OK | MB_ICONERROR);
			return NULL;
		}

		// Return the image back to the program
		return imageData;
	}

	// 15-Jan-2014
	InputHandler* AcornEngine::GetInputHandler()
	{
		// If the input object does not exist (which is should at this point)
		if(!m_Input) {
			// Create a new inout handler object
			m_Input = new InputHandler(m_XScale, m_YScale);
			// Check if the input object created sucessfully
			if(!m_Input) {
				// Return NULL if it did not
				return NULL;
			}
		}

		// Return the pointer to the input handler object
		return m_Input;
	}

	Renderer* AcornEngine::CreateRenderer()
	{
		// Check if the renderer object has been created all ready - if it has then skip this if statement
		if(!m_Renderer) {
			// Create the new renderer object
			m_Renderer = new Renderer();
			// Check if the renderer object was created and if not, return NULL
			if(!m_Renderer) {
				return NULL;
			}
			// Try to initialise the renderer object and if not, return NULL
			if(!m_Renderer->Initialise(m_WindowHandle, m_ScreenRect, m_XScale, m_YScale)) {
				return NULL;
			}
		}

		// Return the pointer to the renderer object
		return m_Renderer;
	}

	FontRenderer* AcornEngine::CreateFontRenderer(LPCWSTR p_BitmapFilePath, string p_DataFilePath)
	{
		FontRenderer *l_FontRenderer = NULL;
		l_FontRenderer = new FontRenderer();
		if(!l_FontRenderer) {
			return NULL;
		}

		if(!l_FontRenderer->Initialise(LoadABitmap(p_BitmapFilePath), p_DataFilePath)) {
			return false;
		}

		return l_FontRenderer;
	}

	LRESULT CALLBACK AcornEngine::AcornProc(HWND p_WindowHandle, UINT p_WindowMessage, WPARAM p_WParam, LPARAM p_LParam)
	{
		switch(p_WindowMessage)
		{
			case WM_KEYDOWN:
			{
				m_Input->SetKeyDown(p_WParam);
				break;
			}

			case WM_KEYUP:
			{
				m_Input->SetKeyUp(p_WParam);
				break;
			}

			case WM_LBUTTONDOWN:
			{
				m_Input->SetLeftMouseDown();
				break;
			}

			case WM_MOUSEMOVE:
			{
				int X = LOWORD(p_LParam);
				int Y = HIWORD(p_LParam);
				m_Input->SetMousePosition(X, Y);
			}

			case WM_LBUTTONUP:
			{
				m_Input->SetLeftMouseUp();
				break;
			}

			case WM_RBUTTONDOWN:
			{
				m_Input->SetRightMouseDown();
				break;
			}

			case WM_RBUTTONUP:
			{
				m_Input->SetRightMouseUp();
				break;
			}

			default:
			{
				return DefWindowProc(p_WindowHandle, p_WindowMessage, p_WParam, p_LParam);
			}
		}

		// Catch case if for some mysterious reason the switch statement doesn't fire
		return DefWindowProc(p_WindowHandle, p_WindowMessage, p_WParam, p_LParam);
	}

	Sprite* AcornEngine::CreateSprite(int p_X, int p_Y, int p_Width, int p_Height, int p_SourceWidth, int p_SourceHeight, int p_OffsetX, int p_OffsetY)
	{
		// Check if the passed width is set as fullsized
		if(p_Width == SPRITE_FULLSIZE) {
			// Set the width of the sprite to the width of the screen resolution
			p_Width = m_ResolutionX;
		}

		// Check if the passed height is set as fullsized
		if(p_Height == SPRITE_FULLSIZE) {
			// Set the height of the sprite to the height of the screen resolution
			p_Height = m_ResolutionY;
		}

		// Create local pointer to a sprite and set to NULL
		Sprite* l_LocalSprite = NULL;
		// Create a new sprite object at the pointer address and pass in the scale variables
		l_LocalSprite = new Sprite();
		// Check if the sprite created correctly
		if(!l_LocalSprite) {
			// Return NULL if the sprite failed to create
			return NULL;
		}

		// Pass in the data for the sprite to object
		l_LocalSprite->Initialise(p_X, p_Y, p_Width, p_Height, p_SourceWidth, p_SourceHeight, p_OffsetX, p_OffsetY);

		// Return the newly created sprite object pointer
		return l_LocalSprite;
	}
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch(umessage)
	{
		// Check if the window is being destroyed.
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			break;
		}

		// Check if the window is being closed.
		case WM_CLOSE:
		{
			PostQuitMessage(0);		
			break;
		}

		// Send all other window messages to the Acorn Engine instance
		default:
		{
			return acfw::g_PointerToEngine->AcornProc(hwnd, umessage, wparam, lparam);
		}
	}

	// Catch case if for some mysterious reason the switch statement doesn't fire
	return DefWindowProc(hwnd, umessage, wparam, lparam);
}