//////////////
// INCLUDES //
//////////////
#include "InputHandler.hpp"

// The AcornEngine Framework namespace
namespace acfw
{
	InputHandler::InputHandler()
	{
		// Set the X and Y position of the mouse to 0
		m_MouseX = m_MouseY = 0;
		// Go through the arrays of key states and set them to false
		for(int i = 0; i < 256; i++) {
			m_Keys[i] = false;
			m_PreviousKeyState[i] = false;
		}
		// Set the current and previous states of the mouse buttons to false
		m_LeftMouseDown = m_RightMouseDown = false;
		m_PreviousLeftMouseState = m_PreviousRightMouseState = false;
	}

	InputHandler::InputHandler(double p_ScaleX, double p_ScaleY)
	{
		// Set the scale
		m_ScaleX = p_ScaleX;
		m_ScaleY = p_ScaleY;

		// Set the X and Y position of the mouse to 0
		m_MouseX = m_MouseY = 0;
		// Go through the arrays of key states and set them to false
		for(int i = 0; i < 256; i++) {
			m_Keys[i] = false;
			m_PreviousKeyState[i] = false;
		}
		// Set the current and previous states of the mouse buttons to false
		m_LeftMouseDown = m_RightMouseDown = false;
		m_PreviousLeftMouseState = m_PreviousRightMouseState = false;
	}

	bool InputHandler::WasKeyPressed(int p_KeyID)
	{
		// Check if the previous state of the key is false - and therefore not pressed
		if(m_PreviousKeyState[p_KeyID] == false) {
			// Check if the current state is not the same as the previous state
			if(m_Keys[p_KeyID] != m_PreviousKeyState[p_KeyID]) {
				// Return true if the states are different and therefore the key has just been pressed
				return true;
			}
		}

		// Return false if any of the above checks fail
		return false;
	}

	bool InputHandler::WasKeyReleased(int p_KeyID) 
	{
		// Check if the previous state of the key was true - and therefore pressed
		if(m_PreviousKeyState[p_KeyID] == true) {
			// Check that the current state is not the same as the previous state
			if(m_Keys[p_KeyID] != m_PreviousKeyState[p_KeyID]) {
				// Return true of the states are different and therefore the key has just been released
				return true;
			}
		}

		// Return false of any of the above checks fail
		return false;
	}

	void InputHandler::SetLeftMouseDown() 
	{ 
		// Set the flag to true - Left mouse button is down
		m_LeftMouseDown = true; 
		// Save the location the mouse was pressed down at
		m_LeftMousePressedX = m_MouseX;
		m_LeftMousePressedY = m_MouseY;
	}

	void InputHandler::SetRightMouseDown() 
	{ 
		// Set the flag to true - Right mouse button is down
		m_RightMouseDown = true; 
		// Save the location the mouse was pressed at
		m_RightMousePressedX = m_MouseX;
		m_RightMousePressedY = m_MouseY;
	}

	bool InputHandler::WasLeftMousePressed()
	{
		if(m_PreviousLeftMouseState == false) {
			if(m_LeftMouseDown != m_PreviousLeftMouseState) {
				return true;
			}
		}

		return false;
	}

	bool InputHandler::WasRightMousePressed()
	{
		if(m_PreviousRightMouseState == false) {
			if(m_RightMouseDown != m_PreviousRightMouseState) {
				return true;
			}
		}

		return false;
	}

	bool InputHandler::WasLeftMouseReleased()
	{
		if(m_PreviousLeftMouseState == true) {
			if(m_LeftMouseDown != m_PreviousLeftMouseState) {
				return true;
			}
		}

		return false;
	}

	bool InputHandler::WasRightMouseReleased()
	{
		if(m_PreviousRightMouseState == true) {
			if(m_RightMouseDown != m_PreviousRightMouseState) {
				return true;
			}
		}

		return false;
	}

	void InputHandler::UpdateStates()
	{
		// Set the previous state of the keys to be equal to the value of the keys
		for(int i = 0; i < 256; i++) {
			m_PreviousKeyState[i] = m_Keys[i];
		}
		// Set the previous state of the mouse buttons to be equal to the current state
		m_PreviousLeftMouseState = m_LeftMouseDown;
		m_PreviousRightMouseState = m_RightMouseDown;
	}

	bool InputHandler::SpriteLeftClicked(Sprite *p_Data)
	{
		if(m_LeftMouseDown) {
			if((m_LeftMousePressedX >= (int)(p_Data->GetXPosition()*m_ScaleX)) && (m_LeftMousePressedX <= ((int)(p_Data->GetXPosition()*m_ScaleX) + (int)(p_Data->GetSpriteWidth()*m_ScaleX)))) {
				if((m_LeftMousePressedY >= (int)(p_Data->GetYPosition()*m_ScaleY)) && (m_LeftMousePressedY <= ((int)(p_Data->GetYPosition()*m_ScaleY) + (int)(p_Data->GetSpriteHeight()*m_ScaleY)))) {
					return true;
				}
			}
		}
	
		return false;
	}

	bool InputHandler::SpriteRightClicked(Sprite *p_Data)
	{
		if(m_RightMouseDown) {
			if((m_RightMousePressedX >= (int)(p_Data->GetXPosition()*m_ScaleX)) && (m_RightMousePressedX <= ((int)(p_Data->GetXPosition()*m_ScaleX) + (int)(p_Data->GetSpriteWidth()*m_ScaleX)))) {
				if((m_RightMousePressedY >= (int)(p_Data->GetYPosition()*m_ScaleY)) && (m_RightMousePressedY <= ((int)(p_Data->GetYPosition()*m_ScaleY) + (int)(p_Data->GetSpriteHeight()*m_ScaleY)))) {
					return true;
				}
			}
		}
	
		return false;
	}

	bool InputHandler::MouseOverSprite(Sprite *p_Data)
	{
		if((m_MouseX >= (int)(p_Data->GetXPosition()*m_ScaleX)) && (m_MouseX <= ((int)(p_Data->GetXPosition()*m_ScaleX) + (int)(p_Data->GetSpriteWidth()*m_ScaleX)))) {
			if((m_MouseY >= (int)(p_Data->GetYPosition()*m_ScaleY)) && (m_MouseY <= ((int)(p_Data->GetYPosition()*m_ScaleY) + (int)(p_Data->GetSpriteHeight()*m_ScaleY)))) {
				return true;
			}
		}
	
		return false;
	}
}