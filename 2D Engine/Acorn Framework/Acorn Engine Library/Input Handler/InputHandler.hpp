/*
 * Name:	TextFileLoader.hpp 
 * Author:	Christopher Acornley
 * Date:	05-Feb-2014
 */

#ifndef _INPUT_HANDLER_HPP_
#define _INPUT_HANDLER_HPP_

//////////////
// INCLUDES //
//////////////
#include "../Graphics/Sprite.hpp"

// The AcornEngine Framework namespace
namespace acfw
{
	// This class holds all the data about keyboard and mouse input into the
	// application
	class InputHandler {
		//////////////////////
		// PUBLIC FUNCTIONS //
		//////////////////////
		public:
			// Constructor
			InputHandler();
			// Constructor with argument
			InputHandler(double, double);
			// Deconstructor
			~InputHandler();
			// Returns the status of the Left Mouse Button
			bool IsLeftMouseDown() {return m_LeftMouseDown; }
			// Returns the status of the Right Mouse Button
			bool IsRightMouseDown() { return m_RightMouseDown; }
			// Returns the status of the keyboard keys
			bool IsKeyDown(int p_KeyID) { return m_Keys[p_KeyID]; }
			// Returns the X position of the mouse
			int GetMouseXPosition() { return m_MouseX; }
			// Returns the Y position of the mouse
			int GetMouseYPosition() { return m_MouseY; }
			// Sets a specified key to be down
			void SetKeyDown(int p_KeyID) { m_Keys[p_KeyID] = true; }
			// Sets a specified key to be up
			void SetKeyUp(int p_KeyID) { m_Keys[p_KeyID] = false; } 
			// Sets the left mouse button to be down
			void SetLeftMouseDown();
			// Sets the left mouse button to be up
			void SetLeftMouseUp() { m_LeftMouseDown = false; }
			// Sets the right mouse button to the down
			void SetRightMouseDown();
			// Sets the right mouse button to be up
			void SetRightMouseUp() { m_RightMouseDown = false; }
			// Sets the mouse position
			void SetMousePosition(int p_PassX, int p_PassY) { m_MouseX = p_PassX; m_MouseY = p_PassY; }
			// Checks if the specified key was pressed
			bool WasKeyPressed(int p_KeyID);
			// Checks if the speficied key was released
			bool WasKeyReleased(int p_KeyID);
			// Checks if the left mouse button was pressed
			bool WasLeftMousePressed();
			// Checks if the right mouse button was pressed
			bool WasRightMousePressed();
			// Checks if the left mouse button was released
			bool WasLeftMouseReleased();
			// Checks if the right mouse button was released
			bool WasRightMouseReleased();
			// Checks if the mouse position intersects with a specified sprite when left clicked
			bool SpriteLeftClicked(Sprite *p_ClickedSprite);
			// Checks if the mouse position intersects with a specified sprite when right clicked
			bool SpriteRightClicked(Sprite *p_ClickedSprite);
			// Checks if the mouse intersects with a specified sprite
			bool MouseOverSprite(Sprite *p_TargetSprite);
			// Updates the states of the input handler
			void UpdateStates();

		private:
			int m_MouseX, m_MouseY;										// The mouse X and Y position
			bool m_Keys[256];											// Array to represent the states of all the keys
			bool m_PreviousKeyState[256];								// Array to represent the previous states of all the keys
			bool m_LeftMouseDown, m_RightMouseDown;						// Flags to check if the left or right mouse button is down
			bool m_PreviousLeftMouseState, m_PreviousRightMouseState;	// Flags to state the previous state of the left or right mouse button
			int m_LeftMousePressedX, m_LeftMousePressedY;				// X and Y position of the left mouse button when pressed down for the first time
			int m_RightMousePressedX, m_RightMousePressedY;				// X and Y position of the right mouse button when pressed down for the first time
			double m_ScaleX, m_ScaleY;									// X and Y scaling, used for intersection testing
	}; //== End of InputHandler ==//
}//== End of acfw ==//

#endif

