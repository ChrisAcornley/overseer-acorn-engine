#include "Sprite.hpp"

namespace acfw
{
	Sprite::Sprite(void)
	{
		m_X = 0;
		m_Y = 0;
		m_Width = 48;
		m_Height = 48;
		m_SourceWidth = 48;
		m_SourceHeight = 48;
		m_OffsetX = 0;
		m_OffsetY = 0;
		m_TransparentValue = 0xFF00FF;
	}

	Sprite::~Sprite(void)
	{
		// Nothing to clean up
	}

	void Sprite::Initialise(int p_X, int p_Y, int p_Width, int p_Height, int p_SourceWidth, int p_SourceHeight, int p_OffsetX, int p_OffsetY)
	{
		// Set the class variables to the argument ones (correct the ones affected by the scale)
		m_X = p_X;
		m_Y = p_Y;
		m_Width = p_Width;
		m_Height = p_Height;
		m_SourceWidth = p_SourceWidth;
		m_SourceHeight = p_SourceHeight;
		m_OffsetX = p_OffsetX;
		m_OffsetY = p_OffsetY;

		// State end of the function
		return;
	}
}
