#include "Renderer.hpp"

namespace acfw
{
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////

	Renderer::Renderer(void)
	{
		// Nothing to create here
	}

	Renderer::~Renderer(void)
	{
		// Nothing to clean up
	}

	bool Renderer::Initialise(HWND p_WindowHandle, RECT p_ScreenRect, float p_ScaleX, float p_ScaleY)
	{
		// Pass the values into the classes local variables
		m_WindowHandle = p_WindowHandle;
		m_ScreenRect = p_ScreenRect;

		// Check the Window Handle is valid
		if(!m_WindowHandle) {
			return false;
		}

		// Set up scale values
		m_ScaleX = p_ScaleX;
		m_ScaleY = p_ScaleY;

		// Set the buffers and check if they were set correctly
		if(!SetBuffers()) {
			return false;
		}

		// State that the function completed successfully
		return true;
	}

	void Renderer::Render(Sprite* p_Data)
	{
		// Create a handle to a bitmap object
		HBITMAP originalBitMap;

		// Selects the sprite bitmap data using the back bitmap context handle
		originalBitMap = (HBITMAP)SelectObject(m_BitmapContextHandle, p_Data->GetTexture());

		// Draw the bitmap into the back buffer from the context handle
		/*
		Source context handle
		X position in window of the sprite
		Y position in the window of the sprite
		Width of destination rectangle
		Height of destination rectangle
		Source context handle
		X position of source rectangle
		Y position of source rectangle
		Width of source rectangle
		Height of source rectangle
		RGB value in source bitmap to treat as the transparent colour - In this case a horrible magenta colour
		*/
		GdiTransparentBlt(	m_BackBufferContextHandle,
							(int)(p_Data->GetXPosition()*m_ScaleX),
							(int)(p_Data->GetYPosition()*m_ScaleY),
							(int)(p_Data->GetSpriteWidth()*m_ScaleX),
							(int)(p_Data->GetSpriteHeight()*m_ScaleY),
							m_BitmapContextHandle,
							p_Data->GetOffsetX(),
							p_Data->GetOffsetY(),
							p_Data->GetSourceWidth(),
							p_Data->GetSourceHeight(),
							p_Data->GetTransparentValue());

		// Gets the bitmap device context handle to select the local bitmap
		SelectObject(m_BitmapContextHandle,originalBitMap); 

		return;
	}

	void Renderer::Render(Sprite* p_Data, double p_Scale)
	{
		// Create a handle to a bitmap object
		HBITMAP originalBitMap;

		// Selects the sprite bitmap data using the back bitmap context handle
		originalBitMap = (HBITMAP)SelectObject(m_BitmapContextHandle, p_Data->GetTexture());

		// Draw the bitmap into the back buffer from the context handle
		/*
		Source context handle
		X position in window of the sprite
		Y position in the window of the sprite
		Width of destination rectangle
		Height of destination rectangle
		Source context handle
		X position of source rectangle
		Y position of source rectangle
		Width of source rectangle
		Height of source rectangle
		RGB value in source bitmap to treat as the transparent colour - In this case a horrible magenta colour
		*/
		GdiTransparentBlt(	m_BackBufferContextHandle,
							(int)(p_Data->GetXPosition()*m_ScaleX),
							(int)(p_Data->GetYPosition()*m_ScaleY),
							(int)(p_Data->GetSpriteWidth()*p_Scale),
							(int)(p_Data->GetSpriteHeight()*p_Scale),
							m_BitmapContextHandle,
							p_Data->GetOffsetX(),
							p_Data->GetOffsetY(),
							p_Data->GetSourceWidth(),
							p_Data->GetSourceHeight(),
							p_Data->GetTransparentValue());

		// Gets the bitmap device context handle to select the local bitmap
		SelectObject(m_BitmapContextHandle,originalBitMap); 

		return;
	}

	void Renderer::Render(int p_X, int p_Y, int p_Width, int p_Height, HBITMAP p_Bitmap, int p_OffsetX, int p_OffsetY, int p_SourceWidth, int p_SourceHeight, int p_TransparentValue)
	{
		// Create a handle to a bitmap object
		HBITMAP originalBitMap;

		// Passes the sprite bitmap data into the back bitmap context handle
		originalBitMap = (HBITMAP)SelectObject(m_BitmapContextHandle, p_Bitmap);

		// Draw the bitmap into the back buffer from the context handle
		/*
		Source context handle
		X position in window of the sprite
		Y position in the window of the sprite
		Width of destination rectangle
		Height of destination rectangle
		Source context handle
		X position of source rectangle
		Y position of source rectangle
		Width of source rectangle
		Height of source rectangle
		RGB value in source bitmap to treat as the transparent colour - In this case a horrible magenta colour
		*/
		GdiTransparentBlt(	m_BackBufferContextHandle,
							(int)(p_X*m_ScaleX),
							(int)(p_Y*m_ScaleY),
							(int)(p_Width*m_ScaleX),
							(int)(p_Height*m_ScaleY),
							m_BitmapContextHandle,
							p_OffsetX,
							p_OffsetY,
							p_SourceWidth,
							p_SourceHeight,
							p_TransparentValue);

		// Gets the bitmap device context handle to select the local bitmap
		SelectObject(m_BitmapContextHandle,originalBitMap); 

		return;
	}

	void Renderer::DisplayFrame()
	{
		// Transfers the data from the back buffer to the front buffer 
		/*
		Destination Handle
		Upper left corner of the window (x)
		Upper left corner of the window (y)
		Width of window
		Height of window
		Source Handle
		Upper left corner of the source buffer (x)
		Upper left corner of the source buffer (y)
		Copy source rectangle to destination rectangle
		*/
		BitBlt(	m_FrontBufferContextHandle, 
				m_ScreenRect.left,
				m_ScreenRect.top, 
				m_ScreenRect.right, 
				m_ScreenRect.bottom, 
				m_BackBufferContextHandle, 
				0, 
				0, 
				SRCCOPY);

		// Emties the back buffer so it can be filled with new data
		FillRect(	m_BackBufferContextHandle, 
					&m_ScreenRect, 
					(HBRUSH)GetStockObject(0));	

		return;
	}

	void Renderer::Shutdown()
	{
		// Release the device contexts
		ReleaseDC(NULL, m_BitmapContextHandle);
		ReleaseDC(NULL, m_BackBufferContextHandle);
		ReleaseDC(m_WindowHandle, m_FrontBufferContextHandle);

		// Delete the bitmap objects
		DeleteObject(m_FrontBitmap);
		DeleteObject(m_BackBitmap);

		// Ensure the objects are removed by nullifying the handlers themselves
		m_FrontBitmap = NULL;
		m_BackBitmap = NULL;

		// Nullify the window handle, don't delete it as it will be deleted elsewehere, not in the renderer
		m_WindowHandle = NULL;
	}

	///////////////////////
	// PRIVATE FUNCTIONS //
	///////////////////////

	bool Renderer::SetBuffers()
	{
		// Uses the handle to the current application window and make the rectangle
		// structure that represents the window
		GetClientRect(m_WindowHandle, &m_ScreenRect);	

		// Gets a device context for the application window
		m_FrontBufferContextHandle = GetDC(m_WindowHandle);	

		// Check the front buffer context handle is valid
		if(!m_FrontBufferContextHandle) {
			return false;
		}

		// Creates a device context that is compatible with the front buffer
		m_BackBufferContextHandle = CreateCompatibleDC(m_FrontBufferContextHandle);

		// Check the back buffer context handle is valid
		if(!m_BackBufferContextHandle) {
			return false;
		}

		// Creates a device context that is compatible with the back buffer
		m_BitmapContextHandle = CreateCompatibleDC(m_BackBufferContextHandle);

		// Check the normal bitmap context handle is valid
		if(!m_BitmapContextHandle) {
			return false;
		}

		// Creates a bitmap compatible with the front buffer and the size of the screen
		m_FrontBitmap = CreateCompatibleBitmap(m_FrontBufferContextHandle, m_ScreenRect.right, m_ScreenRect.bottom);	

		// Check the front bitmap was created
		if(!m_FrontBitmap) {
			return false;
		}

		// Does the same as above but does it for the back buffer
		m_BackBitmap = (HBITMAP)SelectObject(m_BackBufferContextHandle, m_FrontBitmap);

		// Check the back bitmap was created
		if(!m_BackBitmap) {
			return false;
		}
			
		// Empty the back buffer so new data can be added to it
		FillRect(m_BackBufferContextHandle, &m_ScreenRect, (HBRUSH)GetStockObject(0));	

		// State the buffers were set correctly
		return true;
	}
}
