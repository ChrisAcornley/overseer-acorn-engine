#ifndef _SPRITE_HPP_
#define _SPRITE_HPP_

#include <Windows.h>

namespace acfw
{
	#define SPRITE_MAGENTA 0xFF00FF

	class Sprite
	{
	public:
		Sprite(void);
		~Sprite(void);
		void Initialise(int p_X, int p_Y, int p_Width, int p_Height, int p_SourceWidth, int p_SourceHeight, int p_OffsetX, int p_OffsetY);
		void SetPosition(int p_X, int p_Y) { m_X = p_X; m_Y = p_Y; }
		void SetAbsolutePosition(int p_X, int p_Y) { m_X = p_X; m_Y = p_Y; }
		void SetSpriteSize(int p_Width, int p_Height) { m_Width = p_Width; m_Height = p_Height; }
		void SetSourceSize(int p_Width, int p_Height) { m_SourceHeight = p_Height; m_SourceWidth = p_Width; }
		void SetOffsetPosition(int p_OffsetX, int p_OffsetY) { m_OffsetX = p_OffsetX; m_OffsetY = p_OffsetY; }
		void SetTransparentColour(int p_Transparent);
		void SetBitmap(HBITMAP p_Texture) { m_SpriteTexture = p_Texture; }
		void SetXPosition(int p_X) { m_X = p_X; }
		void SetYPosition(int p_Y) { m_Y = p_Y; }
		void SetSpriteWidth(int p_Width) { m_Width = p_Width; }
		void SetSpriteHeight(int p_Height) { m_Height = p_Height; }
		void SetSourceWidth(int p_Width) { m_SourceWidth = p_Width; }
		void SetSourceHeight(int p_Height) { m_SourceHeight = p_Height; }
		void SetOffsetX(int p_OffsetX) { m_OffsetX = p_OffsetX; }
		void SetOffsetY(int p_OffsetY) { m_OffsetY = p_OffsetY; }
		int GetXPosition() { return m_X; }
		int GetYPosition() { return m_Y; }
		int GetSpriteWidth() { return m_Width; }
		int GetSpriteHeight() { return m_Height; }
		int GetOffsetX() { return m_OffsetX; }
		int GetOffsetY() { return m_OffsetY; }
		int GetSourceWidth() { return m_SourceWidth; }
		int GetSourceHeight() { return m_SourceHeight; }
		int GetTransparentValue() { return m_TransparentValue; }
		HBITMAP GetTexture() { return m_SpriteTexture; }

	private:
		int m_X, m_Y;
		int m_Width, m_Height;
		int m_OffsetX, m_OffsetY;
		int m_SourceWidth, m_SourceHeight;
		int m_TransparentValue;
		HBITMAP m_SpriteTexture;
	};
}

#endif

