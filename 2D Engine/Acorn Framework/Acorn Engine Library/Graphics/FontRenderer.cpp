#include "FontRenderer.hpp"

namespace acfw
{
	FontRenderer::FontRenderer(void)
	{
		m_SpriteArray = NULL;
	}

	FontRenderer::~FontRenderer(void)
	{
	}

	bool FontRenderer::Initialise(HBITMAP p_FontBitmap, string p_Filename)
	{
		m_FontBitmap = p_FontBitmap;

		READTEXTDATA l_ReadData;
		TextFileLoader l_FileLoader;

		if(!l_FileLoader.ReadFile(p_Filename, l_ReadData)) {
			return false;
		}
		// Go through each line and assign the letter to the array
		for(int i = 0; i < NUMBER_OF_KEYS; i++) {
			m_CharacterData[i] = GetLineData(l_ReadData.m_ReadText[i]);
		}

		return true;
	}

	bool FontRenderer::RenderText(string p_PassedString, int p_X, int p_Y, Renderer *p_2DRenderer, double p_FontSize)
	{
		p_FontSize = p_FontSize/10;

		int l_SizeOfString = p_PassedString.size();
		string l_CurrentCharacter;
		m_SpriteArray = new Sprite[l_SizeOfString];
		if(!m_SpriteArray) {
			return false;
		}

		for(int i = 0; i < l_SizeOfString; i++) {
			// Get character and create appropriate sprite
			l_CurrentCharacter = p_PassedString.front();
			p_PassedString.erase(0,1);

			for(int j = 0; j < NUMBER_OF_KEYS; j++) {
				// Check each character data struct until a match is found
				if(l_CurrentCharacter.compare(m_CharacterData[j].m_Charatcer) == 0) {
					m_SpriteArray[i].SetSourceSize(m_CharacterData[j].m_BottomRightY - m_CharacterData[j].m_TopLeftY, m_CharacterData[j].m_BottomRightX - m_CharacterData[j].m_TopLeftX);
					m_SpriteArray[i].SetSpriteSize(m_SpriteArray[i].GetSourceWidth(), m_SpriteArray[i].GetSourceHeight());
					m_SpriteArray[i].SetOffsetPosition(m_CharacterData[j].m_TopLeftY, m_CharacterData[j].m_TopLeftX);
					m_SpriteArray[i].SetPosition(p_X, p_Y);
					m_SpriteArray[i].SetBitmap(m_FontBitmap);

					j = NUMBER_OF_KEYS;
				}
			}
			p_X += m_SpriteArray[i].GetSpriteWidth()*p_FontSize;

			p_2DRenderer->Render(&m_SpriteArray[i], p_FontSize);
		}

		delete [] m_SpriteArray;
		m_SpriteArray = NULL;

		return true;
	}

	CharacterData FontRenderer::GetLineData(string p_String)
	{
		CharacterData l_TempStore;
		string l_CurrentCharacter;
		size_t findPos;
		string l_Delim = ",";
		// Get front character
		l_TempStore.m_Charatcer = p_String.front();
		// Delete it and the delim after it
		p_String.erase(0, 2);

		findPos = p_String.find(l_Delim);
		l_CurrentCharacter = p_String.substr(0, findPos);
		l_TempStore.m_TopLeftY = atoi(l_CurrentCharacter.c_str());
		p_String.erase(0, findPos+1);

		findPos = p_String.find(l_Delim);
		l_CurrentCharacter = p_String.substr(0, findPos);
		l_TempStore.m_TopLeftX = atoi(l_CurrentCharacter.c_str());
		p_String.erase(0, findPos+1);

		findPos = p_String.find(l_Delim);
		l_CurrentCharacter = p_String.substr(0, findPos);
		l_TempStore.m_BottomRightY = atoi(l_CurrentCharacter.c_str());
		p_String.erase(0, findPos+1);

		l_TempStore.m_BottomRightX = atoi(p_String.c_str());

		return l_TempStore;
	}

	void FontRenderer::Shutdown(void)
	{
		if(m_SpriteArray) {
			delete [] m_SpriteArray;
			m_SpriteArray = NULL;
		}

		return;
	}
}
