// 28-Feb-2014

#ifndef _FONT_RENDERER_HPP_
#define _FONT_RENDERER_HPP_

#include "Sprite.hpp"
#include "..\Text File Reader\TextFileLoader.hpp"
#include "Renderer.hpp"
#include <string>

using namespace std;

const static int NUMBER_OF_KEYS = 95;

namespace acfw 
{
	struct CharacterData {
			string m_Charatcer;
			int m_TopLeftX, m_TopLeftY;
			int m_BottomRightX, m_BottomRightY;
	};

	class FontRenderer
	{
	public:
		FontRenderer(void);
		~FontRenderer(void);
		bool Initialise(HBITMAP p_FontBitmap, string p_Filename);
		bool RenderText(string p_PassedString, int p_X, int p_Y, Renderer *p_2DRenderer, double p_FontSize);
		CharacterData GetLineData(string p_String);
		void Shutdown(void);

	private:
		HBITMAP m_FontBitmap;
		Sprite *m_SpriteArray;
		CharacterData m_CharacterData[NUMBER_OF_KEYS];
	};
}

#endif

