// 06/02/2014

#ifndef _RENDERER_HPP_
#define _RENDERER_HPP_

#include <Windows.h>
#include "Sprite.hpp"

namespace acfw
{
	class Renderer
	{
	public:
		Renderer(void);
		~Renderer(void);

		bool Initialise(HWND, RECT, float, float);
		void Render(Sprite*);
		void Render(int, int, int, int, HBITMAP, int, int, int, int, int);
		void Render(Sprite*, double);
		void DisplayFrame();
		void Shutdown();
		float GetScaleX() { return m_ScaleX; }
		float GetScaleY() { return m_ScaleY; }

	private:

		bool SetBuffers();

	private:

		HBITMAP		m_FrontBitmap, m_BackBitmap;
		HWND        m_WindowHandle;
		RECT		m_ScreenRect;
		HDC			m_FrontBufferContextHandle, m_BackBufferContextHandle, m_BitmapContextHandle;
		float		m_ScaleX, m_ScaleY;


	};
}
#endif

