========================================================================
    STATIC LIBRARY : Acorn Framework Project Overview
========================================================================
    AUTHOR : 	     Christopher Acornley (1000697)
========================================================================

This library is for the 2D game I will make for my honours project. The purpose of this library is to keep as much of the code that draws and renders out of the main application as possible. This allows easy reusing of the code and makes the main program very easy to create. For example, creating a window simply requires a valid AcornEngine object and then calling its CreateAWindow() function. This handles the window as well as the Windows message loop and can intercept and interpret messages as needs be. So far the only interaction needed with the message loop is the gathering of user input data.

So far there are seven classes that add functionality to the library. They are in no particular order;

AcornEngine - This class handles the creation of most of the other classes in the library such as the renderers, sprites and input handler.

InputHandler - This class contains the user input data and can pass it on to the application when needed. The pointer to this class is passed around a lot as a lot of classes need access to it.

TextFileLoader - This class allows the reading and saving of text files and their contents. The data is saved in an array of strings and passed back to the program. Parsing of the data has to be handled elsewhere as it can differ in each situation.

Sprite	- The sprite class contains all the data for a single sprite. This includes its position in the world, its size, the source position, source size, and bitmap handle.

Renderer - This class handles all graphical rendering and can render a sprite onto the screen.

FontRenderer - This class can draw text onto the screen like a sprite. It generates an array of sprites and passes these to the Renderer to draw. 

ArrayReader - This class reads a text file and reads the array back to the program. It takes a pointer to a pointer as argument and treats it as a double array. This allows loading of map data and 2D arrays.