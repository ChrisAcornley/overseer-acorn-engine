/*
 * Name:	GameClass.hpp
 * Author:	Christopher Acornley
 * Date:	26-Feb-2014
 */

#ifndef _GAME_CLASS_HPP_
#define _GAME_CLASS_HPP_

//////////////
// INCLUDES //
//////////////
#include <Windows.h>
#include "Acorn Engine Library\AcornEngine.hpp"
#include "Main Menu\MainMenuClass.hpp"
#include "Game\MainGameClass.hpp"

// Create a namespace for the game state enumeration. This prevents it from polluting global namespace
namespace GameStates 
{
	enum GameState { MAIN_MENU = 0, SETTINGS = 1, GAME_WITH_AI = 2, GAME_WITHOUT_AI = 3, INVALID = 4 };
}

// The game class handles the state the appliction is in and loads, unloads, 
// processes and draws the right resources at that point
class GameClass
{
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////
	public:
		// Constructor
		GameClass(void);
		// Constructor - Takes custom gamestate if desired
		GameClass(GameStates::GameState p_DesiredStartingState);
		// Deconstructor
		~GameClass(void);
		// Initiases the class - creates the pointers and sets member types
		bool Initialise(acfw::InputHandler *p_InputHandlerPointer, acfw::AcornEngine *p_EnginePointer);
		// Run function - runs through the game states to process the right resources
		bool Run(acfw::AcornEngine *p_EnginePointer, float p_DeltaTime);
		// Shutdown the pointers and types safely
		void Shutdown(void);
		// Sets the current game state - Allows classes with access to this one to alter the gamestate
		void SetCurrentGameState(GameStates::GameState p_NewGameState) { m_CurrentGameState = p_NewGameState; }
		// Returns the current game state
		GameStates::GameState GetCurrentGameState(void) { return m_CurrentGameState; }

	///////////////////////
	// PRIVATE FUNCTIONS //
	///////////////////////
	private:
		// Changes to a new gamestate depending on the menu options
		bool MenuMoveToNewGameState(MainMenuOptions::MainMenuOption p_Option);
		// Runs the game itself
		bool RunGame(acfw::AcornEngine* p_EnginePointer, float p_DeltaTime, bool p_RunOverseer);

	///////////////////
	// PRIVATE TYPES //
	///////////////////
	private:
		GameStates::GameState m_CurrentGameState;	// The current state of the game
		acfw::InputHandler *m_Input;				// Pointer to the input handler object - this is passed to subsequent classes
		acfw::Renderer *m_2DRenderer;				// Pointer to the renderer - this is passed to subsequent classes
		acfw::FontRenderer *m_FontRenderer;			// Pointer to the font renderer - this is passed to subsequent classes
		MainMenuClass *m_MainMenuHandler;			// Pointer to the main menu handler object
		MainGameClass *m_MainGameHandler;			// Pointer to the main game object
};

#endif
