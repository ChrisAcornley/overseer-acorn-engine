/*
 * Name:	GameBackground.hpp
 * Author:	Christopher Acornley
 * Date:	09-Mar-2014
 */

#ifndef _GAME_BACKGROUND_HPP_
#define _GAME_BACKGROUND_HPP_

//////////////
// INCLUDES //
//////////////
#include "Acorn Engine Library\AcornEngine.hpp"

/*
 * This class contains the data and functions nessesary to render and update
 * the background to the game.
 * The background moves around to keep in line with the players position. This
 * allows the background to be simular to the map object yet move around the
 * screen in the same way.
 */
class GameBackground
{
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////
	public:
		// Constructor
		GameBackground(void);
		// Deconstructor
		~GameBackground(void);
		// Initialises the class
		bool Initialise(acfw::AcornEngine* p_EnginePointer);
		// Updates background position
		bool Update(acfw::Vector2 p_OffsetVector);
		// Render the background
		bool Render(acfw::Renderer *p_Renderer, acfw::FontRenderer *p_FontRenderer);
		// Safely closes the class
		void Shutdown(void);

	///////////////////
	// PRIVATE TYPES //
	///////////////////
	private:
		acfw::Sprite* m_SpriteArray;	// Sprite Data
		acfw::Vector2 m_StartVector;	// Position vector of background
};

#endif