#include "MainGameClass.hpp"

MainGameClass::MainGameClass(void)
{
	m_Input = NULL;
	m_BackgroundObject = NULL;
	m_PlayerObject = NULL;
	m_MapDrawer = NULL;
	m_Overseer = NULL;
	m_UIClass = NULL;
	m_OverseerChange = false;
	m_GameOver = false;
	m_ElapsedGameTime = 0.0f;
}

MainGameClass::~MainGameClass(void)
{
}

bool MainGameClass::Initialise(acfw::InputHandler* p_InputHandler, acfw::AcornEngine* p_EnginePointer, const bool p_IsOverseerActive)
{
	m_OverseerActive = p_IsOverseerActive;

	m_Input = p_InputHandler;
	if(!m_Input) {
		return false;
	}

	m_BackgroundObject = new GameBackground();
	if(!m_BackgroundObject) {
		return false;
	}
	if(!m_BackgroundObject->Initialise(p_EnginePointer)) {
		return false;
	}

	m_PlayerObject = new PlayerObject();
	if(!m_PlayerObject) {
		return false;
	}
	if(!m_PlayerObject->Initialise(p_EnginePointer)) {
		return false;
	}

	m_MapDrawer = new MapDrawerObject();
	if(!m_MapDrawer) {
		return false;
	}

	if(!m_MapDrawer->Initialise(p_EnginePointer)) {
		return false;
	}

	m_UIClass = new UserInterfaceHandler();
	if(!m_UIClass) {
		return false;
	}

	if(!m_UIClass->Initialise(p_EnginePointer)) {
		return false;
	}

	if(m_OverseerActive) {
		m_Overseer = new olib::OverseerLibrary();
		if(!m_Overseer) {
			return false;
		}

		olib::Vector2 l_MapSize;
		acfw::Vector2 l_RealMapSize = m_MapDrawer->GetMapSize();

		l_MapSize.x = l_RealMapSize.x;
		l_MapSize.y = l_RealMapSize.y;

		if(!m_Overseer->Initialise(m_MapDrawer->GetMap(), l_MapSize)) {
			return false;
		}
	}

	m_CurrentGameState = MainGameStates::GAME_NORMAL;

	return true;
}

bool MainGameClass::Run(acfw::Renderer *p_Renderer, acfw::FontRenderer *p_FontRenderer, const float p_DeltaTime)
{
	if(!EndOfGame() && !m_GameOver) {
		m_ElapsedGameTime += p_DeltaTime;
	}

	if(!Update(p_DeltaTime)) {
		return false;
	}

	if(!Render(p_Renderer, p_FontRenderer)) {
		return false;
	}
	
	return true;
}

void MainGameClass::Shutdown(void)
{
	if(m_Input) {
		m_Input = NULL;
	}

	if(m_UIClass) {
		m_UIClass->Shutdown();
		delete m_UIClass;
		m_UIClass = NULL;
	}

	if(m_Overseer) {
		m_Overseer->Shutdown();
		delete m_Overseer;
		m_Overseer = NULL;
	}

	if(m_PlayerObject) {
		m_PlayerObject->Shutdown();
		delete m_PlayerObject;
		m_PlayerObject = NULL;
	}

	if(m_BackgroundObject) {
		m_BackgroundObject->Shutdown();
		delete m_BackgroundObject;
		m_BackgroundObject = NULL;
	}

	if(m_MapDrawer) {
		m_MapDrawer->Shutdown();
		delete m_MapDrawer;
		m_MapDrawer = NULL;
	}

	return;
}

bool MainGameClass::EndOfGame(void)
{
	return m_PlayerObject->IsPlayerAtExit();
}

bool MainGameClass::Update(float p_DeltaTime)
{
	if(m_OverseerActive) {
		if(m_Overseer->IsTestChange()) {
			//m_MapDrawer->ResetValidTiles();
			m_OverseerChange = true;
		}	
	}

	if((!m_GameOver) && (!EndOfGame())) {
		m_PlayerObject->Update(m_Input, m_MapDrawer->GetMap(), m_MapDrawer->GetMapSize());
	}

	m_BackgroundObject->Update(m_PlayerObject->GetPlayerPosition());

	m_MapDrawer->Update(m_PlayerObject->GetPlayerPosition(), m_PlayerObject->GetPlayerMapPosition());

	if(!m_OverseerActive) {
		m_UIClass->Update(EndOfGame(), m_GameOver, m_OverseerChange, "Let's make a change here, here and....here!", m_PlayerObject->GetPlayerMapPosition(), m_ElapsedGameTime);
	}
	else {
		m_UIClass->Update(EndOfGame(), m_GameOver, m_OverseerChange, m_Overseer->GetResponce(), m_PlayerObject->GetPlayerMapPosition(), m_ElapsedGameTime);
	}

	if(m_OverseerActive) {
		olib::Vector2 l_TempPlayerMapPos;
		acfw::Vector2 l_RealPlayerMapPos = m_PlayerObject->GetPlayerMapPosition();

		l_TempPlayerMapPos.x = l_RealPlayerMapPos.x;
		l_TempPlayerMapPos.y = l_RealPlayerMapPos.y;

		if(!m_Overseer->Run(l_TempPlayerMapPos, p_DeltaTime)) {
			return false;
		}
	}

	if(m_ElapsedGameTime > 300000.0f) {
		m_GameOver = true;
	}

	return true;
}

bool MainGameClass::Render(acfw::Renderer *p_Renderer, acfw::FontRenderer *p_FontRenderer)
{
	// Render the following objects - Check thay are successful in rendering
	// Return false if an error occurs
	// Background
	if(!m_BackgroundObject->Render(p_Renderer, p_FontRenderer)) {
		return false;
	}

	// Map and ghost map
	if(!m_MapDrawer->Render(p_Renderer, p_FontRenderer)) {
		return false;
	}

	// Player sprite
	if(!m_PlayerObject->Render(p_Renderer, p_FontRenderer)) {
		return false;
	}

	// User Interface
	if(!m_UIClass->Render(p_Renderer, p_FontRenderer)) {
		return false;
	}

	// State rendering operation was successful
	return true;
}