/*
 * Name:	UserInterfaceHandler.hpp
 * Author:	Christopher Acornley
 * Date:	31-Mar-2014
 */

#ifndef _USER_INTERFACE_HANDLER_HPP_
#define _USER_INTERFACE_HANDLER_HPP_

//////////////
// INCLUDES //
//////////////
#include "Acorn Engine Library\AcornEngine.hpp"
#include <string>
#include <sstream>

/*
 * Class contains the objects and data for the user interface
 */
class UserInterfaceHandler
{
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////
	public:
		// Constructor
		UserInterfaceHandler(void);
		// Deconstructor
		~UserInterfaceHandler(void);
		// Intialise
		bool Initialise(acfw::AcornEngine* p_EnginePointer);
		// Update
		bool Update(bool p_PlayerEndsGame, bool p_TimeExpired, bool p_OverseerSpeaks, std::string p_OverseerText, acfw::Vector2 p_PlayerMapPosition, float p_ElapsedTime);
		// Render
		bool Render(acfw::Renderer* p_Renderer, acfw::FontRenderer* p_FontRenderer);
		// Safely close the class
		void Shutdown(void);

	///////////////////
	// PRIVATE TYPES //
	///////////////////
	private:
		acfw::Sprite *m_OverseerTextBackground;	// Sprite for UI
		acfw::Sprite *m_EndGameBackground;		// Sprite for end game dialog
		bool m_RenderNormalEndGame;				// Flag to check if the normal ending should be rendered
		bool m_RenderFailedEndGame;				// Flag to check if the time out ending should be rendered
		bool m_RenderOverseerText;				// Flag to see if the Overseer text should be rendered
		std::string m_OverseerText;				// String containing the Overseer text
		acfw::Vector2 m_PlayerMapPosition;		// Position of the player in the map
		float m_ElapsedTime;					// Elapsed time of the game
};

#endif