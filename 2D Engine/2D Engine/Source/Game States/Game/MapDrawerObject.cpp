#include "MapDrawerObject.hpp"

MapDrawerObject::MapDrawerObject(void)
{
	m_MapSprite = NULL;
	m_ValidTiles = NULL;
	m_InSightTiles = NULL;
	m_MapSize = acfw::Vector2(0,0);
	m_Map = NULL;
}

MapDrawerObject::~MapDrawerObject(void)
{
}

bool MapDrawerObject::Initialise(acfw::AcornEngine* p_EnginePointer)
{
	m_MapSprite = p_EnginePointer->CreateSprite(0,0,64,64,64,64,0,0);
	if(!m_MapSprite) {
		return false;
	}

	m_MapSprite->SetBitmap((p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Game Sprites/MapSprites.bmp")));

	m_CurrentMapPosition = acfw::Vector2(0,0);
	m_MapSize = acfw::Vector2(30,30);

	m_Map = new int *[m_MapSize.y];
	for(int i = 0; i < m_MapSize.y; i++) {
		m_Map[i] = new int[m_MapSize.x];
	}

	acfw::ArrayReader l_ArrayReader;
	if(!l_ArrayReader.OpenAndReadArrayFile(m_Map, "Resources/Window Data/Map1.txt")) {
		return false;
	}

	m_ValidTiles = new bool[m_MapSize.y*m_MapSize.x];

	for(int i = 0; i < (m_MapSize.y*m_MapSize.x); i++) {
		m_ValidTiles[i] = false;
	}

	m_InSightTiles = new bool[m_MapSize.y*m_MapSize.x];

	for(int i = 0; i < (m_MapSize.y*m_MapSize.x); i++) {
		m_InSightTiles[i] = false;
	}

	return true;
}

bool MapDrawerObject::Update(acfw::Vector2 p_PlayerPosition, acfw::Vector2 p_PlayerMapPosition)
{
	// go from centre of screen, move 11 units to the left and 7 units up, add on player position
	m_CurrentDrawPosition.x = m_StartDrawPosition.x = -64 + p_PlayerPosition.x;
	m_CurrentDrawPosition.y = m_StartDrawPosition.y = -64 + p_PlayerPosition.y;

	m_PlayerMapPosition = p_PlayerMapPosition;

	for(int i = 0; i < 900; i++) {
		m_InSightTiles[i] = false;
	}

	return true;
}

bool MapDrawerObject::Render(acfw::Renderer* p_Renderer, acfw::FontRenderer* p_FontRenderer)
{
	m_CurrentMapPosition.x = m_PlayerMapPosition.x - 11;
	m_CurrentMapPosition.y = m_PlayerMapPosition.y - 7;

	for(int i = 0; i < 22; i++) {
		for(int j = 0; j < 14; j++) {
			if(SetSpriteOffset()) {
				m_MapSprite->SetPosition(m_CurrentDrawPosition.x, m_CurrentDrawPosition.y);
				
				// Check if the sprite is within vertical or horizontal line with the player + and - one
				// Check if sprite within vertical line
				// Check if sprite within horizontal line
				if(!m_InSightTiles[(m_CurrentMapPosition.x*30)+m_CurrentMapPosition.y]) {
					if((m_CurrentMapPosition.x >= (m_PlayerMapPosition.x - 1)) && (m_CurrentMapPosition.x <= (m_PlayerMapPosition.x + 1))) {
						// Check how many blocks above the players block are floor tiles
						int l_FloorBlocksAbove = 0;
						int l_FloorBlocksBelow = 0;
						while(m_Map[m_PlayerMapPosition.y - l_FloorBlocksAbove][m_PlayerMapPosition.x] == 0) {
							l_FloorBlocksAbove ++;
						}
						while(m_Map[m_PlayerMapPosition.y + l_FloorBlocksBelow][m_PlayerMapPosition.x] == 0) {
							l_FloorBlocksBelow ++;
						}
						if((m_CurrentMapPosition.y >= (m_PlayerMapPosition.y - l_FloorBlocksAbove)) &&
						   (m_CurrentMapPosition.y <= (m_PlayerMapPosition.y + l_FloorBlocksBelow))) {
							m_ValidTiles[(m_CurrentMapPosition.x*30)+m_CurrentMapPosition.y] = true;
							m_InSightTiles[(m_CurrentMapPosition.x*30)+m_CurrentMapPosition.y] = true;
							p_Renderer->Render(m_MapSprite);
						}
					}

					if((m_CurrentMapPosition.y >= (m_PlayerMapPosition.y - 1)) && (m_CurrentMapPosition.y <= (m_PlayerMapPosition.y + 1))) {
						// Check how many blocks above the players block are floor tiles
						int l_FloorBlocksLeft = 0;
						int l_FloorBlocksRight = 0;
						while(m_Map[m_PlayerMapPosition.y][m_PlayerMapPosition.x - l_FloorBlocksLeft] == 0) {
							l_FloorBlocksLeft ++;
						}
						while(m_Map[m_PlayerMapPosition.y][m_PlayerMapPosition.x + l_FloorBlocksRight] == 0) {
							l_FloorBlocksRight ++;
						}
						if((m_CurrentMapPosition.x >= (m_PlayerMapPosition.x - l_FloorBlocksLeft)) &&
						   (m_CurrentMapPosition.x <= (m_PlayerMapPosition.x + l_FloorBlocksRight))) {
							m_ValidTiles[(m_CurrentMapPosition.x*30)+m_CurrentMapPosition.y] = true;
							m_InSightTiles[(m_CurrentMapPosition.x*30)+m_CurrentMapPosition.y] = true;
							p_Renderer->Render(m_MapSprite);
						}
					}
				}
				
				if(m_ValidTiles[(m_CurrentMapPosition.x*30)+m_CurrentMapPosition.y] && !m_InSightTiles[(m_CurrentMapPosition.x*30)+m_CurrentMapPosition.y]){
					m_MapSprite->SetOffsetY(64);
					p_Renderer->Render(m_MapSprite);
				}
			}
			m_CurrentDrawPosition.y += 64;
			m_CurrentMapPosition.y ++;
		}
		m_CurrentDrawPosition.y = m_StartDrawPosition.y;
		m_CurrentDrawPosition.x += 64;
		m_CurrentMapPosition.y = m_PlayerMapPosition.y - 7;
		m_CurrentMapPosition.x ++;
	}

	return true;
}

void MapDrawerObject::Shutdown(void)
{
	if(m_MapSprite) {
		delete m_MapSprite;
		m_MapSprite = NULL;
	}

	if(m_ValidTiles) {
		delete [] m_ValidTiles;
		m_ValidTiles = NULL;
	}

	if(m_InSightTiles) {
		delete [] m_InSightTiles;
		m_InSightTiles = NULL;
	}

	if(m_Map) {
		for(int i = 0; i < 30; i++) {
			delete m_Map[i];
			m_Map[i] = NULL;
		}

		delete [] m_Map;
		m_Map = NULL;
	}

	return;
}

bool MapDrawerObject::SetSpriteOffset(void)
{
	if(!(m_CurrentMapPosition.x < 0) && !(m_CurrentMapPosition.y < 0)) {
		if(!(m_CurrentMapPosition.x >= 30) && !(m_CurrentMapPosition.y >= 30)) {
			switch(m_Map[m_CurrentMapPosition.y][m_CurrentMapPosition.x]) {
				case 0:
					m_MapSprite->SetOffsetPosition(0,0);
					return true;
				case 1:
					m_MapSprite->SetOffsetPosition(64,0);
					return true;
				case 2:
					m_MapSprite->SetOffsetPosition(128,0);
					return true;
				default:
					return false;
			}
		}
	}

	return false;
}

void MapDrawerObject::ResetValidTiles(void)
{
	for(int i = 0; i < 900; i++) {
		m_ValidTiles[i] = false;
	}
}