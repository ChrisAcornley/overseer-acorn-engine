/*
 * Name:	MapDrawerObject.hpp
 * Author:	Christopher Acornley
 * Date:	12-Mar-2014
 */

#ifndef _MAP_DRAWER_OBJECT_HPP_
#define _MAP_DRAWER_OBJECT_HPP_

//////////////
// INCLUDES //
//////////////
#include "Acorn Engine Library\AcornEngine.hpp"

/*
 * This class handles the drawing of the game maze.
 */
class MapDrawerObject
{
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////
	public:
		// Constructor
		MapDrawerObject(void);
		// Deconstructor
		~MapDrawerObject(void);
		// Initialise function
		bool Initialise(acfw::AcornEngine* p_EnginePointer);
		// Update
		bool Update(acfw::Vector2 p_PlayerPosition, acfw::Vector2 p_PlayerMapPosition);
		// Render
		bool Render(acfw::Renderer* p_Renderer, acfw::FontRenderer* p_FontRenderer);
		// Shutdown
		void Shutdown(void);
		// Gets the maze array
		int** GetMap(void) { return m_Map; }
		// Gets the maze size
		acfw::Vector2 GetMapSize(void) { return m_MapSize; }
		// Resets valid tiles (blacks out ghost areas)
		void ResetValidTiles(void);

	///////////////////////
	// PRIVATE FUNCTIONS //
	///////////////////////
	private:
		// Sets the current frame of a sprite
		bool SetSpriteOffset(void);

	///////////////////
	// PRIVATE TYPES //
	///////////////////
	private:
		int **m_Map;							// Pointer to maze
		bool *m_ValidTiles;						// Pointer to hash array for ghost tiles
		bool *m_InSightTiles;					// Pointer to hash array for line of sight tiles
		acfw::Sprite *m_MapSprite;				// Pointer to map sprite object
		acfw::Vector2 m_StartDrawPosition;		// Position to start drawing
		acfw::Vector2 m_CurrentDrawPosition;	// Current drawing position
		acfw::Vector2 m_PlayerMapPosition;		// Position of player in the maze
		acfw::Vector2 m_CurrentMapPosition;		// Current position in maze
		acfw::Vector2 m_MapSize;				// Size of the maze
};

#endif