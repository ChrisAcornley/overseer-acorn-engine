#include "GameBackground.hpp"

GameBackground::GameBackground(void)
{
	m_SpriteArray = NULL;
}

GameBackground::~GameBackground(void)
{
}

bool GameBackground::Initialise(acfw::AcornEngine* p_EnginePointer)
{
	/*
	m_SpriteArray = p_EnginePointer->CreateSprite(0,0,64,64,64,64,0,0);
	if(!m_SpriteArray) {
		return false;
	}

	m_SpriteArray->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Game Sprites/Floor.bmp"));
	*/
	m_SpriteArray = p_EnginePointer->CreateSprite(0,0,p_EnginePointer->GetRezSizeX(),p_EnginePointer->GetRezSizeY(),64,64,0,0);
	if(!m_SpriteArray) {
		return false;
	}

	m_SpriteArray->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Game Sprites/Background.bmp"));


	return true;
}

bool GameBackground::Update(acfw::Vector2 p_OffsetVector)
{
	// Set the starting position
	///m_StartVector.x = -64 + p_OffsetVector.x;
	//m_StartVector.y = -64 + p_OffsetVector.y;

	return true;
}

bool GameBackground::Render(acfw::Renderer *p_Renderer, acfw::FontRenderer *p_FontRenderer)
{
	/*
	acfw::Vector2 l_CurrentVector = m_StartVector;

	for(int i = 0; i < 22; i++) {
		for(int j = 0; j < 14; j++) {
			m_SpriteArray->SetAbsolutePosition(l_CurrentVector.x, l_CurrentVector.y);
			p_Renderer->Render(m_SpriteArray);
			l_CurrentVector.y += m_SpriteArray->GetSpriteHeight();
		}
		l_CurrentVector.y = m_StartVector.y;
		l_CurrentVector.x += m_SpriteArray->GetSpriteWidth();
	}
	*/
	p_Renderer->Render(m_SpriteArray);

	return true;
}

void GameBackground::Shutdown(void)
{
	if(m_SpriteArray) {
		delete m_SpriteArray;
		m_SpriteArray = NULL;
	}
}