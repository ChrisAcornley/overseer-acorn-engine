/*
 * Name:	PlayerObject.hpp
 * Author:	Christopher Acornley
 * Date:	09-Mar-2014
 */

#ifndef _PLAYER_OBJECT_HPP_
#define _PLAYER_OBJECT_HPP_

//////////////
// INCLUDES //
//////////////
#include "Acorn Engine Library\AcornEngine.hpp"

// Namespace containing direction the player is trying to move in
namespace PlayerDirection
{
	enum Direction { MOVE_LEFT = 0, MOVE_UP = 1, MOVE_RIGHT = 2, MOVE_DOWN = 3 };
}

/*
 * Class contains player object data and functions.
 */
class PlayerObject
{
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////
	public:
		// Constructor
		PlayerObject(void);
		// Deconstructor
		~PlayerObject(void);
		// Initialise
		bool Initialise(acfw::AcornEngine* p_EnginePointer);
		// Update
		bool Update(acfw::InputHandler* p_InputHandler, int** p_Map, acfw::Vector2 p_MapSize);
		// Render
		bool Render(acfw::Renderer *p_Renderer, acfw::FontRenderer *p_FontRenderer);
		// Safely close this object
		void Shutdown(void);
		// Get the players position in the current tile
		acfw::Vector2 GetPlayerPosition() { return m_PlayerPosition; }
		// Get the players position in the maze
		acfw::Vector2 GetPlayerMapPosition() { return m_PlayerMapPosition; }
		// Check to see if the player is at the exit
		bool IsPlayerAtExit(void) { return m_IsPlayerInExitBlock; }

	///////////////////////
	// PRIVATE FUNCTIONS //
	///////////////////////
	private:
		// Checks if the move attempted is allowed
		bool IsMoveValid(PlayerDirection::Direction p_PlayerDirection, int** p_Map, acfw::Vector2 p_MapSize);

	///////////////////
	// PRIVATE TYPES //
	///////////////////
	private:
		acfw::Sprite* m_SpriteArray;		// Pointer to player sprite
		acfw::Vector2 m_PlayerPosition;		// Position of player within current maze tile
		acfw::Vector2 m_PlayerMapPosition;	// Position of player within maze
		bool m_IsPlayerInExitBlock;			// Flag to check if player is at the exit of the maze
		int m_FramesPassed;					// Check the number of frames that have passed
		int m_Frame;						// Check the animation frame the player is in
};

#endif