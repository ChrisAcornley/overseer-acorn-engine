//////////////
// INCLDUES //
//////////////
#include "PlayerObject.hpp"

//////////////////////////
// FUNCTION DEFINITIONS //
//////////////////////////

// Constructor
PlayerObject::PlayerObject(void)
{
	// Nullify the sprite object pointer
	m_SpriteArray = NULL;

	// State the player is not in an exit block (this will be updated in the Update function)
	m_IsPlayerInExitBlock = false;

	// Set the starting frame to 0 (first frame) and ensure the frame couter is at 0
	m_Frame = 0;
	m_FramesPassed = 0;
}

// Deconstructor
PlayerObject::~PlayerObject(void)
{
	// Nothing to clean up
}

// Initialise Function
// Arguments - Pointer to the engine object
// Returns - Boolean - States if the function executed correctly or not
bool PlayerObject::Initialise(acfw::AcornEngine* p_EnginePointer)
{
	// Set the starting poisiton of the player as the centre of the current block he/she is standing on
	m_PlayerPosition.x = 0;
	m_PlayerPosition.y = 0;

	// Set the map posisiton of the player as the top left hand corner
	m_PlayerMapPosition.x = 1;
	m_PlayerMapPosition.y = 1;

	// Create a sprite object from the engine in accordance with the following specifications
	m_SpriteArray = p_EnginePointer->CreateSprite(	(64*10)+20,
													(64*6)+16,
													24,
													32,
													32,
													64,
													0,
													192);
	// Check the pointer returned is valid
	if(!m_SpriteArray) {
		// End the Initilise function stating an error occured
		return false;
	}

	// Set the Bitmap data of the sprite to the one specified
	m_SpriteArray->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Game Sprites/Player.bmp"));

	// State the Initialise function was successful
	return true;
}

// Update Function
// Arguments - Pointers to the input handler, Map array and a Vector to describe the maps size
// Returns - Boolean - States if the function executed correctly or not
bool PlayerObject::Update(acfw::InputHandler* p_InputHandler, int** p_Map, acfw::Vector2 p_MapSize)
{
	if(p_InputHandler->IsKeyDown('A')) {
		m_SpriteArray->SetOffsetY(0);
		if(IsMoveValid(PlayerDirection::MOVE_LEFT, p_Map, p_MapSize)) {
			m_FramesPassed ++;
			m_PlayerPosition.x += 3;
			if(m_PlayerPosition.x > 32) {
				if(m_PlayerMapPosition.x > 0) {
					m_PlayerPosition.x = -32;
					m_PlayerMapPosition.x --;
				}
				else {
					m_PlayerPosition.x --;
				}
			}
		}
	}
	if(p_InputHandler->IsKeyDown('D')) {
		m_SpriteArray->SetOffsetY(m_SpriteArray->GetSourceHeight());
		if(IsMoveValid(PlayerDirection::MOVE_RIGHT, p_Map, p_MapSize)) {
			m_FramesPassed ++;
			m_PlayerPosition.x -= 3;
			if(m_PlayerPosition.x < -32) {
				if(m_PlayerMapPosition.x < 30) {
					m_PlayerPosition.x = 32;
					m_PlayerMapPosition.x ++;
				}
				else {
					m_PlayerPosition.x ++;
				}
			}
		}
	}
	if(p_InputHandler->IsKeyDown('W')) {
		m_SpriteArray->SetOffsetY(m_SpriteArray->GetSourceHeight()*2);
		if(IsMoveValid(PlayerDirection::MOVE_UP, p_Map, p_MapSize)) {
			m_FramesPassed ++;
			m_PlayerPosition.y += 3;
			if(m_PlayerPosition.y > 32) {
				if(m_PlayerMapPosition.y > 0) {
					m_PlayerPosition.y = -32;
					m_PlayerMapPosition.y --;
				}
				else {
					m_PlayerPosition.y --;
				}
			}
		}
	}
	if(p_InputHandler->IsKeyDown('S')) {
		m_SpriteArray->SetOffsetY(m_SpriteArray->GetSourceHeight()*3);
		if(IsMoveValid(PlayerDirection::MOVE_DOWN, p_Map, p_MapSize)) {
			m_FramesPassed ++;
			m_PlayerPosition.y -= 3;
			if(m_PlayerPosition.y < -32) {
				if(m_PlayerMapPosition.y < 30) {
					m_PlayerPosition.y = 32;
					m_PlayerMapPosition.y ++;
				}
				else {
					m_PlayerPosition.y ++;
				}
			}
		}
	}

	// Check if the block the player is on is the exit block
	if(p_Map[m_PlayerMapPosition.y][m_PlayerMapPosition.x] == 2) {
		// State the player has stepped on an exit block
		m_IsPlayerInExitBlock = true;
	}

	// Check if 15 frames of movement have passed since the last check
	if(m_FramesPassed >= 15) {
		// Check if the user is in the first frame
		if(m_Frame == 0) {
			// Move to the second frame
			m_Frame ++;
			// Move the actual drawing offset to the second frame
			m_SpriteArray->SetOffsetX(m_SpriteArray->GetSourceWidth());
		}
		// Check if the user is in the second frame
		else if(m_Frame == 1) {
			// Go back to the first frame
			m_Frame --;
			// Reset the actual drawing offset to the first frame
			m_SpriteArray->SetOffsetX(0);
		}
		// Reset the frame counter
		m_FramesPassed = 0;
	}

	// State the player object updated without any problems
	return true;
	
}

// Render Function
// Arguments - Pointers to the 2D Renderer and Font Renderer
// Returns - Boolean - States if the function executed correctly or not - Currently useless
bool PlayerObject::Render(acfw::Renderer *p_Renderer, acfw::FontRenderer *p_FontRenderer)
{
	// Render the player sprite onto the screen
	p_Renderer->Render(m_SpriteArray);

	// State the rendering process was successful
	return true;
}

// Shutdown Function
void PlayerObject::Shutdown(void)
{
	// Check if the sprite object pointer is valid
	if(m_SpriteArray) {
		// Delete the object data
		delete m_SpriteArray;
		//Nullify the pointer
		m_SpriteArray = NULL;
	}

	// State the end of the Shutdown function
	return;
}

// Valid Move Function
// Notes - Checks if the player can make the move they are trying. 
// Returns - Boolean - Returns true if the player can move and false if they cannot
bool PlayerObject::IsMoveValid(PlayerDirection::Direction p_PlayerDirection, int** p_Map, acfw::Vector2 p_MapSize)
{
	// Check the direction the player is trying to move
	switch(p_PlayerDirection) {
		case PlayerDirection::MOVE_LEFT:
		{
			// If at the edge of the map
			if(m_PlayerMapPosition.x == 0) {
				if(m_PlayerPosition.x >= 20) {
					return false;
				}
			}
			// Check if the block to the left is valid to move to
			else if(p_Map[m_PlayerMapPosition.y][m_PlayerMapPosition.x - 1] == 1) {
				if(m_PlayerPosition.x >= 20) {
					// Ensure the player is on the edge so as not to get stuck
					m_PlayerPosition.x = 20;
					return false;
				}
			}

			// check if at edge and lower left is valid
			else if(m_PlayerPosition.x >= 20) {
				if(m_PlayerPosition.y > 16) {
					return false;
				}
				else if(m_PlayerPosition.y < -16) {
					return false;
				}
			}

			return true;
		}
		case PlayerDirection::MOVE_UP:
		{
			// If at the edge of the map
			if(m_PlayerMapPosition.y == 0) {
				if(m_PlayerPosition.y >= 16) {
					return false;
				}
			}
			// Check if the block above is valid to move to
			else if(p_Map[m_PlayerMapPosition.y - 1][m_PlayerMapPosition.x] == 1) {
				if(m_PlayerPosition.y >= 16) {
					m_PlayerPosition.y = 16;
					return false;
				}
			}

			else if(m_PlayerPosition.y >= 16) {
				if(m_PlayerPosition.x > 20) {
					return false;
				}
				else if(m_PlayerPosition.x < -20) {
					return false;
				}
			}

			return true;
		}
		case PlayerDirection::MOVE_RIGHT:
		{
			// If at the edge of the map
			if(m_PlayerMapPosition.x == p_MapSize.x - 1) {
				if(m_PlayerPosition.x <= -20) {
					return false;
				}
			}
			// Check if the block to the left is valid to move to
			else if(p_Map[m_PlayerMapPosition.y][m_PlayerMapPosition.x + 1] == 1) {
				if(m_PlayerPosition.x <= -20) {
					m_PlayerPosition.x = -20;
					return false;
				}
			}

			else if(m_PlayerPosition.x <= -20) {
				if(m_PlayerPosition.y > 16) {
					return false;
				}
				else if(m_PlayerPosition.y < -16) {
					return false;
				}
			}

			return true;
		}
		case PlayerDirection::MOVE_DOWN:
		{
			// If at the edge of the map
			if(m_PlayerMapPosition.y == p_MapSize.y - 1) {
				if(m_PlayerPosition.y <= -16) {
					return false;
				}
			}
			// Check if the block to the left is valid to move to
			else if(p_Map[m_PlayerMapPosition.y + 1][m_PlayerMapPosition.x] == 1) {
				if(m_PlayerPosition.y <= -16) {
					m_PlayerPosition.y = -16;
					return false;
				}
			}

			else if(m_PlayerPosition.y <= -16) {
				if(m_PlayerPosition.x > 20) {
					return false;
				}
				else if(m_PlayerPosition.x < -20) {
					return false;
				}
			}

			return true;
		}
		default:
		{
			break;
		}
	}

	return false;
}
