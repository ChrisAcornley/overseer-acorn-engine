#include "UserInterfaceHandler.hpp"

UserInterfaceHandler::UserInterfaceHandler(void)
{
	m_OverseerTextBackground = NULL;
	m_EndGameBackground = NULL;
	m_RenderNormalEndGame = false;
	m_RenderFailedEndGame = false;
	m_RenderOverseerText = false;
	m_ElapsedTime = 0.0f;
}

UserInterfaceHandler::~UserInterfaceHandler(void)
{
	// Nothing to clean up
}

bool UserInterfaceHandler::Initialise(acfw::AcornEngine* p_EnginePointer)
{
	m_OverseerTextBackground = p_EnginePointer->CreateSprite(0, 0, p_EnginePointer->GetRezSizeX(), 60, 1, 1, 0, 0);
	if(!m_OverseerTextBackground) {
		return false;
	}

	m_OverseerTextBackground->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Game Sprites/TextBackground.bmp"));

	m_EndGameBackground = p_EnginePointer->CreateSprite(440, 300, 400, 200, 1, 1, 0, 0);
	if(!m_EndGameBackground) {
		return false;
	}

	m_EndGameBackground->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Game Sprites/TextBackground.bmp"));

	return true;
}

bool UserInterfaceHandler::Update(bool p_PlayerEndsGame, bool p_TimeExpired, bool p_OverseerSpeaks, std::string p_OverseerText, acfw::Vector2 p_PlayerMapPosition, float p_ElapsedTime)
{
	m_RenderNormalEndGame = p_PlayerEndsGame;
	m_RenderFailedEndGame = p_TimeExpired;
	m_RenderOverseerText = p_OverseerSpeaks;

	m_PlayerMapPosition = p_PlayerMapPosition;
	m_ElapsedTime = p_ElapsedTime;

	m_OverseerText = p_OverseerText;

	return true;
}

bool UserInterfaceHandler::Render(acfw::Renderer* p_Renderer, acfw::FontRenderer* p_FontRenderer)
{
	if(m_RenderNormalEndGame) {
		p_Renderer->Render(m_EndGameBackground);
		p_FontRenderer->RenderText("CONGRATULATIONS", 470, 320, p_Renderer, 8);
		p_FontRenderer->RenderText("You have completed the level.", 490, 400, p_Renderer, 5); 
		p_FontRenderer->RenderText("Press ESC to return to the menu.", 480, 440, p_Renderer, 5); 
	}

	if(m_RenderFailedEndGame) {
		p_Renderer->Render(m_EndGameBackground);
		p_FontRenderer->RenderText("GAME OVER", 500, 320, p_Renderer, 10);
		p_FontRenderer->RenderText("You have run out of time.", 500, 400, p_Renderer, 5); 
		p_FontRenderer->RenderText("Press ESC to return to the menu.", 480, 440, p_Renderer, 5);
	}

	p_Renderer->Render(m_OverseerTextBackground);

	if(m_RenderOverseerText) {
		p_FontRenderer->RenderText(m_OverseerText, 620, 20, p_Renderer, 5);
	}

	// Create a string stream
	std::ostringstream ostr;

	// Create a vector to temporarily hold the player data
	acfw::Vector2 a = m_PlayerMapPosition;

	// Pass the data into the string stream
	ostr << a.x << "        " << a.y;

	// Create a string
	std::string c;

	// Convert the data in the stream into a string
	c = ostr.str();

	// Render the string onto the screen
	p_FontRenderer->RenderText(c, 20, 20, p_Renderer, 5);

	std::ostringstream ostr2;
	ostr2 << (int)((300000 - m_ElapsedTime)/1000);
	std::string d;
	d = ostr2.str();

	p_FontRenderer->RenderText(d, 500, 20, p_Renderer, 5);

	return true;
}

void UserInterfaceHandler::Shutdown(void)
{
	if(m_OverseerTextBackground) {
		delete m_OverseerTextBackground;
		m_OverseerTextBackground = NULL;
	}

	if(m_EndGameBackground) {
		delete m_EndGameBackground;
		m_EndGameBackground = NULL;
	}

	return;
}
