/*
 * Name:	MainGameClass.hpp
 * Author:	Christopher Acornley
 * Date:	05-Feb-2014
 */

#ifndef _MAIN_GAME_CLASS_HPP_
#define _MAIN_GAME_CLASS_HPP_

//////////////
// INCLUDES //
//////////////
#include "Acorn Engine Library\AcornEngine.hpp"
#include "Overseer Library\OverseerLibrary.hpp"
#include "GameBackground.hpp"
#include "PlayerObject.hpp"
#include "MapDrawerObject.hpp"
#include "UserInterfaceHandler.hpp"

// Contains enumeration for the game state
namespace MainGameStates
{
	enum MainGameState { GAME_CUTSCENE = 0, GAME_NORMAL = 1, GAME_PAUSE = 2, GAME_END = 3 };
}

/*
 * Main game class that handles the updating and rendering of the game as well
 * as the normal game logic execution
 */
class MainGameClass
{
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////
	public:
		// Constructor
		MainGameClass(void);
		// Deconstructor
		~MainGameClass(void);
		// Initialise function
		bool Initialise(acfw::InputHandler* p_InputHandler, acfw::AcornEngine* p_EnginePointer, const bool p_IsOverseerActive);
		// Handles updating and rendering of objects
		bool Run(acfw::Renderer *p_Renderer, acfw::FontRenderer *p_FontRenderer, const float p_DeltaTime);
		// Safely closes down the game
		void Shutdown(void);

	///////////////////////
	// PRIVATE FUNCTIONS //
	///////////////////////
	private:
		// Handles updating of the objects
		bool Update(float p_DeltaTime);
		// Handles rendering of the objects
		bool Render(acfw::Renderer *p_Renderer, acfw::FontRenderer *p_FontRenderer);
		// Checks to see if the game has ended
		bool EndOfGame(void);

	///////////////////
	// PRIVATE TYPES //
	///////////////////
	private:
		MainGameStates::MainGameState m_CurrentGameState;	// Game state
		acfw::InputHandler *m_Input;						// Pointer to input handler
		GameBackground *m_BackgroundObject;					// Pointer to background object
		PlayerObject *m_PlayerObject;						// Pointer to player object
		MapDrawerObject *m_MapDrawer;						// Pointer to the map drawer object
		UserInterfaceHandler *m_UIClass;					// Pointer to the user interface object
		olib::OverseerLibrary *m_Overseer;					// Pointer to the Overseer AI object
		bool m_OverseerActive;								// Flag to check if the Overseer is active
		bool m_OverseerChange;								// Flag to check if the Overseer has made a change
		bool m_GameOver;									// Flag to check to see if the game is over
		float m_ElapsedGameTime;							// Container for the elapsed time of the game
};

#endif

