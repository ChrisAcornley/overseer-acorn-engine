//////////////
// INCLUDES //
//////////////
#include "GameClass.hpp"

/*
 * Name:		GameClass
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
GameClass::GameClass(void)
{
	// Set the starting game state
	m_CurrentGameState = GameStates::MAIN_MENU;

	// Nullify any pointers
	m_Input = NULL;
	m_2DRenderer = NULL;
	m_FontRenderer = NULL;
	m_MainMenuHandler = NULL;
	m_MainGameHandler = NULL;
}

/*
 * Name:		GameClass
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
GameClass::GameClass(GameStates::GameState p_DesiredStartingState)
{
	// Set the starting game state
	m_CurrentGameState = p_DesiredStartingState;

	// Nullify any pointers
	m_Input = NULL;
	m_2DRenderer = NULL;
	m_FontRenderer = NULL;
	m_MainMenuHandler = NULL;
	m_MainGameHandler = NULL;
}

/*
 * Name:		~GameClass
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
GameClass::~GameClass(void)
{
	// Nothing to clean up
}

/*
 * Name:		Initialise
===============================================================================
 * Arguments:	p_InputHandlerPointer - Pointer to input handler object
 *				p_EnginePointer - Pointer to game engine
 *
 * Returns:		Boolean	- True	- Fuction was succesful
 *						- False	- Error occurs
 *
 * Notes:		None
 */
bool GameClass::Initialise(acfw::InputHandler *p_InputHandlerPointer, acfw::AcornEngine *p_EnginePointer)
{
	// Set the pointer to the input handler
	m_Input = p_InputHandlerPointer;
	// Check the pointer is valid
	if(!m_Input) {
		return false;
	}

	/*
	 * Create the object and a pointer to it
	 * Check the pointer is valid
	 */

	// 2D Renderer
	m_2DRenderer = p_EnginePointer->CreateRenderer();
	if(!m_2DRenderer) {
		return false;
	}

	// Font Renderer
	m_FontRenderer = p_EnginePointer->CreateFontRenderer(L"Resources/Sprite Bitmaps/FontBitmap.bmp", "Resources/Window Data/FontData.txt");
	if(!m_FontRenderer) {
		return false;
	}

	// Main Menu Object
	m_MainMenuHandler = new MainMenuClass();
	if(!m_MainMenuHandler) {
		return false;
	}
	
	// Initialise the menu handler and give it the pointer to the input handler object 
	if(!m_MainMenuHandler->Initialise(p_InputHandlerPointer)) {
		return false;
	}

	// State the function was succesful
	return true;
}

/*
 * Name:		Run
===============================================================================
 * Arguments:	p_EnginePointer - Pointer to game engine
 *				p_DeltaTime - Frame time
 *
 * Returns:		Boolean	- True	- Fuction was succesful
 *						- False	- Error occurs
 *
 * Notes:		None
 */
bool GameClass::Run(acfw::AcornEngine *p_EnginePointer, float p_DeltaTime)
{
	// Look at the current gamestate and run the appropriate code
	switch(m_CurrentGameState)
	{
		// Main Menu state
		case GameStates::MAIN_MENU:
		{
			// Check if the resources are not loaded
			if(!m_MainMenuHandler->AreResourcesLoaded()) {
				// If not loaded, load the resources
				if(!m_MainMenuHandler->LoadResources(p_EnginePointer)) {
					// Return false if it cannot load resources
					return false;
				}
			}

			// Update the main menu and return false if error occurs
			if(!m_MainMenuHandler->Update()) {
				return false;
			}

			// Draw the main menu and return false is error occurs
			if(!m_MainMenuHandler->Draw(m_2DRenderer, m_FontRenderer)) {
				return false;
			}

			// Check if the application is to move to another state and return false if an error occurs
			if(!MenuMoveToNewGameState(m_MainMenuHandler->GetCurrentMenuOption())) {
				return false;
			}

			// End the switch statement
			break;
		}

		// Settings State
		case GameStates::SETTINGS:
		{
			// Check if the user presses the escape key and return to the main menu if they do
			if(m_Input->IsKeyDown(27)) {
				m_CurrentGameState = GameStates::MAIN_MENU;
			}

			// Render temporary text
			m_FontRenderer->RenderText("There is nothing here as of yet. Press ESC to return to the main menu.", 30, (p_EnginePointer->GetRezSizeY()/2) - 10, m_2DRenderer, 8);

			// End the switch statement
			break;
		}

		// Game with running Overseer AI state
		case GameStates::GAME_WITH_AI:
		{
			// Run the game with the Overseer AI and return false if an error occurs
			if(!RunGame(p_EnginePointer, p_DeltaTime, true)) {
				return false;
			}

			// End the switch statement
			break;
		}
		// Game without running Overseer AI state
		case GameStates::GAME_WITHOUT_AI:
		{
			// Run the game without the Overseer AI and return false if an error occurs
			if(!RunGame(p_EnginePointer, p_DeltaTime, false)) {
				return false;
			}

			// End the switch statement
			break;
		}
		// Invalid state - recommended for debugging use
		case GameStates::INVALID:
		{
			// End the switch statement
			break;
		}
		// Default state - should any input not be any of the above
		default:
		{
			// Display a message stating the game loop entered a state outside the enumeraion
			MessageBox(NULL, L"There was a strange state in the main games state machine.", L"Error - GameClass::Run()", MB_OK & MB_ICONERROR);

			// Return false stating the run function did something it wasn't suppose to
			return false;
		}
	}

	// Display the scene
	m_2DRenderer->DisplayFrame();

	// State the function was succesful
	return true;
}

/*
 * Name:		Shutdown
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
void GameClass::Shutdown(void)
{
	// If the input pointer is valid
	if(m_Input) {
		// Set the pointer to NULL
		m_Input = NULL;
	}

	/*
	 * Check the pointer is valid
	 * If it is, call the classes Shutdown function
	 * Delete the object
	 * Nullify the pointer
	 */

	// Handler of the Main Menu
	if(m_MainMenuHandler) {
		m_MainMenuHandler->Shutdown();
		delete m_MainMenuHandler;
		m_MainMenuHandler = NULL;
	}

	// Handler of the Game
	if(m_MainGameHandler) {
		m_MainGameHandler->Shutdown();
		delete m_MainGameHandler;
		m_MainGameHandler = NULL;
	}

	// Font Renderer
	if(m_FontRenderer) {
		m_FontRenderer->Shutdown();
		delete m_FontRenderer;
		m_FontRenderer = NULL;
	}

	// 2D Renderer
	if(m_2DRenderer) {
		m_2DRenderer->Shutdown();
		delete m_2DRenderer;
		m_2DRenderer = NULL;
	}

	// State the end of the function
	return;
}

/*
 * Name:		MenuMoveToNewGameState
===============================================================================
 * Arguments:	p_Option - Option chosen in the menu class
 *
 * Returns:		Boolean	- True	- Fuction was succesful
 *						- False	- Error occurs
 *
 * Notes:		None
 */
bool GameClass::MenuMoveToNewGameState(MainMenuOptions::MainMenuOption p_Option)
{
	// Pass the option into a switch statement
	switch(p_Option)
	{
		// No option
		case MainMenuOptions::NO_OPTION:
		{
			// Do nothing and break;
			break;
		}

		// Settings option
		case MainMenuOptions::SETTINGS_OPTION:
		{
			// Unload the current menu resources
			m_MainMenuHandler->UnloadResources();

			// Set the game state to Settings
			m_CurrentGameState = GameStates::SETTINGS;

			// Break from the switch statement
			break;
		}
		case MainMenuOptions::GAME_WITH_AI_OPTION:
		{
			// Unload the current menu resources
			m_MainMenuHandler->UnloadResources();

			// Set the game state to Game With AI
			m_CurrentGameState = GameStates::GAME_WITH_AI;

			// Break from the switch statement
			break;
		}
		case MainMenuOptions::GAME_WITHOUT_AI_OPTION:
		{
			// Unload the current menu resources
			m_MainMenuHandler->UnloadResources();

			// Set the game state to Game Without AI
			m_CurrentGameState = GameStates::GAME_WITHOUT_AI;

			// Break from the switch statement
			break;
		}
		case MainMenuOptions::EXIT_OPTION:
		{
			// Unload the current menu resources
			m_MainMenuHandler->UnloadResources();

			// Break from the switch statement
			return false;
		}
		case MainMenuOptions::INVALID:
		{
			// Unload the current menu resources
			m_MainMenuHandler->UnloadResources();

			// Set the game state to Invalid
			m_CurrentGameState = GameStates::INVALID;

			// Break from the switch statement
			break;
		}
		default:
		{
			// Break from the switch statement
			break;
		}
	}

	// State the function was succesful
	return true;
}

/*
 * Name:		RunGame
===============================================================================
 * Arguments:	p_EnginePointer - Pointer to the game engine
 *				p_DeltaTime - Frame time
 *				p_RunOverseer - Flag to see if the Overseer if running
 *
 * Returns:		Boolean	- True	- Fuction was succesful
 *						- False	- Error occurs
 *
 * Notes:		None
 */
bool GameClass::RunGame(acfw::AcornEngine* p_EnginePointer, float p_DeltaTime, bool p_RunOverseer)
{
	// Check the pointer to the game handler is valid
	if(m_MainGameHandler) {
		// Run the game and return false if an error occurs
		if(!m_MainGameHandler->Run(m_2DRenderer, m_FontRenderer, p_DeltaTime)) {
			return false;
		}
	}

	// If the pointer is invalid
	else {
		// Create an object at a new pointer location
		m_MainGameHandler = new MainGameClass();
		// Check the pointer is valid and return false if its not
		if(!m_MainGameHandler) {
			return false;
		}

		// Initialise the game handler and return false if an error occurs
		if(!m_MainGameHandler->Initialise(m_Input, p_EnginePointer, p_RunOverseer)) {
			return false;
		}
	}

	// Check if the ESC key is being pressed
	if(m_Input->IsKeyDown(27)) {
		// Return to the main menu
		m_CurrentGameState = GameStates::MAIN_MENU;

		// Show the cursor
		ShowCursor(true);

		// Check if the pointer to the Game Handler is valid
		if(m_MainGameHandler) {
			// Call the shutdown function of the class
			m_MainGameHandler->Shutdown();

			// Delete the game object
			delete m_MainGameHandler;

			// Nullify the pointer
			m_MainGameHandler = NULL;
		}
	}

	// State the function was succesful
	return true;
}