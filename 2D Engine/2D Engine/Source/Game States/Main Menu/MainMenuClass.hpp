/*
 * Name:	MainMenuClass.hpp
 * Author:	Christopher Acornley
 * Date:	26-Feb-2014
 */
#ifndef _MAIN_MENU_CLASS_HPP_
#define _MAIN_MENU_CLASS_HPP

//////////////
// INCLUDES //
//////////////
#include "Acorn Engine Library\AcornEngine.hpp"

// Namespace contains the options the user can choose during the menu
namespace MainMenuOptions
{
	enum MainMenuOption { GAME_WITH_AI_OPTION = 0, GAME_WITHOUT_AI_OPTION = 1, SETTINGS_OPTION = 2, EXIT_OPTION = 3, NO_OPTION = 4, INVALID = 5 };
}

/*
 * This class handles the menu logic, updating and rendering
 */
class MainMenuClass
{
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////
	public:
		// Constructor
		MainMenuClass(void);
		// Deconstructor
		~MainMenuClass(void);
		// Initialise
		bool Initialise(acfw::InputHandler *p_InputPointer);
		// Update
		bool Update();
		// Render
		bool Draw(acfw::Renderer *p_Renderer, acfw::FontRenderer *p_FontRenderer);
		// Safely close this class
		void Shutdown(void);
		// Get the current option chosen in the menu
		MainMenuOptions::MainMenuOption GetCurrentMenuOption(void) { return m_CurrentOption; }
		// Load menu resources
		bool LoadResources(acfw::AcornEngine *p_EnginePointer);
		// Unload menu resources
		void UnloadResources(void);
		// Check to ensure resources are properly loaded
		bool AreResourcesLoaded(void) { return m_ResourcesLoaded; }

	///////////////////////
	// PRIVATE FUNCTIONS //
	///////////////////////
	private:
		// Unloads a specific resource
		void UnloadResource(acfw::Sprite *p_SpriteToRemove);

	///////////////////
	// PRIVATE TYPES //
	///////////////////
	private:
		acfw::Sprite *m_Background;						// Pointer to background object
		acfw::Sprite *m_GameWithAIButton;				// Pointer to Normal Game button
		acfw::Sprite *m_GameWithoutAIButton;			// Pointer to limited game button
		acfw::Sprite *m_SettingsButton;					// Pointer to settings button
		acfw::Sprite *m_ExitButton;						// Pointer to exit button
		MainMenuOptions::MainMenuOption m_CurrentOption;// Option chosen by the user
		acfw::InputHandler *m_Input;					// Pointer to input handler
		bool m_ResourcesLoaded;							// Flag to check if resources are correctly loaded+ 
};

#endif
