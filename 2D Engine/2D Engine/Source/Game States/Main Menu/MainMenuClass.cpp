//////////////
// INCLUDES //
//////////////
#include "MainMenuClass.hpp"

/*
 * Name:		MainMenuClass
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
MainMenuClass::MainMenuClass(void)
{
	// Nullify all pointers
	m_Input = NULL;
	m_Background = NULL;
	m_GameWithAIButton = NULL;
	m_GameWithoutAIButton = NULL;
	m_SettingsButton = NULL;
	m_ExitButton = NULL;

	// Set the current option to no option
	m_CurrentOption = MainMenuOptions::NO_OPTION;

	// Set the loaded resources flag to false;
	m_ResourcesLoaded = false;
}

/*
 * Name:		MainMenuClass
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
MainMenuClass::~MainMenuClass(void)
{
	// Nothing to deconstruct
}

/*
 * Name:		Initialise
===============================================================================
 * Arguments:	p_InputPointer - Pointer to the input handler
 *
 * Returns:		Boolean	- True	- Fuction was succesful
 *						- False	- Error occurs
 *
 * Notes:		None
 */
bool MainMenuClass::Initialise(acfw::InputHandler *p_InputPointer)
{
	// Set member input pointer to the passed object
	m_Input = p_InputPointer;
	// Check if the pointer is valid
	if(!m_Input) {
		return false;
	}

	// State function was succesful
	return true;
}

/*
 * Name:		Update
===============================================================================
 * Arguments:	None
 *
 * Returns:		Boolean	- True	- Fuction was succesful
 *						- False	- Error occurs
 *
 * Notes:		None
 */
bool MainMenuClass::Update()
{
	/*
	 * Check the Mouse is over the specified sprite/button
	 * If so, set the buttons animation frame to the second frame
	 * Check if the user clicks the button using the left mouse button
	 * Move the application to the correct state
	 * If the mouse is not over the sprite/button, set the animation to the first frame
	 */

	// Game With AI Button
	if(m_Input->MouseOverSprite(m_GameWithAIButton)) {
		m_GameWithAIButton->SetOffsetY(m_GameWithAIButton->GetSourceHeight());
		if(m_Input->WasLeftMousePressed()) {
			m_CurrentOption = MainMenuOptions::GAME_WITH_AI_OPTION;
		}
	}
	else {
		m_GameWithAIButton->SetOffsetY(0);
	}

	// Game Without AI Button
	if(m_Input->MouseOverSprite(m_GameWithoutAIButton)) {
		m_GameWithoutAIButton->SetOffsetY(m_GameWithoutAIButton->GetSourceHeight());
		if(m_Input->WasLeftMousePressed()) {
			m_CurrentOption = MainMenuOptions::GAME_WITHOUT_AI_OPTION;
		}
	}
	else {
		m_GameWithoutAIButton->SetOffsetY(0);
	}

	// Settings Button
	if(m_Input->MouseOverSprite(m_SettingsButton)) {
		m_SettingsButton->SetOffsetY(m_SettingsButton->GetSourceHeight());
		if(m_Input->WasLeftMousePressed()) {
			m_CurrentOption = MainMenuOptions::SETTINGS_OPTION;
		}
	}
	else {
		m_SettingsButton->SetOffsetY(0);
	}

	// Exit Button
	if(m_Input->MouseOverSprite(m_ExitButton)) {
		m_ExitButton->SetOffsetY(m_ExitButton->GetSourceHeight());
		if(m_Input->WasLeftMousePressed()) {
			m_CurrentOption = MainMenuOptions::EXIT_OPTION;
		}
	}
	else {
		m_ExitButton->SetOffsetY(0);
	}

	// State Update function was succesful
	return true;
}

/*
 * Name:		Draw
===============================================================================
 * Arguments:	p_Renderer - Pointer to the 2D renderer
 *				p_FontRenderer - Pointer to the font renderer
 *
 * Returns:		Boolean	- True	- Fuction was succesful
 *						- False	- Error occurs
 *
 * Notes:		None
 */
bool MainMenuClass::Draw(acfw::Renderer* p_Renderer, acfw::FontRenderer *p_FontRenderer)
{
	// Render the buttons and background
	p_Renderer->Render(m_Background);
	p_Renderer->Render(m_GameWithAIButton);
	p_Renderer->Render(m_GameWithoutAIButton);
	p_Renderer->Render(m_SettingsButton);
	p_Renderer->Render(m_ExitButton);

	// Render the text on the buttons
	p_FontRenderer->RenderText("Game With AI", 90, 115, p_Renderer, 5);
	p_FontRenderer->RenderText("Game Without AI", 65, 185, p_Renderer, 5);
	p_FontRenderer->RenderText("Settings", 140, 255, p_Renderer, 5);
	p_FontRenderer->RenderText("Exit", 160, 325, p_Renderer, 5);

	// State rendering was succesful
	return true;
}

/*
 * Name:		LoadResources
===============================================================================
 * Arguments:	p_EnginePointer - Pointer to the engine object
 *
 * Returns:		Boolean	- True	- Fuction was succesful
 *						- False	- Error occurs
 *
 * Notes:		Manually loads resources
 */
bool MainMenuClass::LoadResources(acfw::AcornEngine *p_EnginePointer)
{
	/*
	 * Create the sprite using the engine
	 * Check the sprite pointer is valid
	 * If not, return false
	 * Set the bitmap data for the sprite
	 */

	// Background Sprite
	m_Background = p_EnginePointer->CreateSprite( 0, 0, acfw::SPRITE_FULLSIZE, acfw::SPRITE_FULLSIZE, 682, 384, 0, 0 );
	if(!m_Background) {
		return false;
	}
	m_Background->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Main Menu/Background.bmp"));

	// Game With AI Button Sprite
	m_GameWithAIButton = p_EnginePointer->CreateSprite( 50, 100, 300, 60, 120, 30, 0, 0 );
	if(!m_GameWithAIButton) {
		return false;
	}
	m_GameWithAIButton->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Main Menu/MainMenuButton.bmp"));

	// Game Without AI Button Sprite
	m_GameWithoutAIButton = p_EnginePointer->CreateSprite( 50, 170, 300, 60, 120, 30, 0, 0 );
	if(!m_GameWithoutAIButton) {
		return false;
	}
	m_GameWithoutAIButton->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Main Menu/MainMenuButton.bmp"));

	// Settings Button Sprite
	m_SettingsButton = p_EnginePointer->CreateSprite( 50, 240, 300, 60, 120, 30, 0, 0 );
	if(!m_SettingsButton) {
		return false;
	}
	m_SettingsButton->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Main Menu/MainMenuButton.bmp"));

	// Exit Button Sprite
	m_ExitButton = p_EnginePointer->CreateSprite( 50, 310, 300, 60, 120, 30, 0, 0 );
	if(!m_ExitButton) {
		return false;
	}
	m_ExitButton->SetBitmap(p_EnginePointer->LoadABitmap(L"Resources/Sprite Bitmaps/Main Menu/MainMenuButton.bmp"));

	// State the resources have loaded
	m_ResourcesLoaded = true;

	// State function was succesful
	return true;
}

/*
 * Name:		UnloadResources
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		Manually unload all resources in the main menu
 */
void MainMenuClass::UnloadResources(void)
{
	/*
	 * Unload the resource
	 * Set the pointer to the object to NULL
	 */

	// Background Sprite
	UnloadResource(m_Background);
	m_Background = NULL;

	// Game With AI Button Sprite
	UnloadResource(m_GameWithAIButton);
	m_GameWithAIButton = NULL;

	// Game Without AI Button Sprite
	UnloadResource(m_GameWithoutAIButton);
	m_GameWithoutAIButton = NULL;

	// Settings Button Sprite
	UnloadResource(m_SettingsButton);
	m_SettingsButton = NULL;

	// Exit Button Sprite
	UnloadResource(m_ExitButton);
	m_ExitButton = NULL;

	// Set the menu option as none
	m_CurrentOption = MainMenuOptions::NO_OPTION;

	// State the resources are not currently loaded
	m_ResourcesLoaded = false;

	// State the end of the function
	return;
}

/*
 * Name:		Shutdown
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
void MainMenuClass::Shutdown(void)
{
	// Check the input pointer is valid
	if(m_Input) {
		// Nullify the pointer - It is destroyed elsewhere
		m_Input = NULL;
	}

	// State the end of the function
	return;
}

/*
 * Name:		UnloadResource
===============================================================================
 * Arguments:	p_SpriteToRemove - Pointer to the target sprite
 * Returns:		None
 * Notes:		Unloads a specific sprite from memory
 */
void MainMenuClass::UnloadResource(acfw::Sprite *p_SpriteToRemove)
{
	// Check the pointer is valid first
	if(p_SpriteToRemove) {
		// Check the texture bitmap is valid
		if(p_SpriteToRemove->GetTexture() != NULL) {
			// Set the bitmap handle to NULL
			p_SpriteToRemove->SetBitmap(NULL);
		}
		// Delete the sprite object
		delete p_SpriteToRemove;
		// Nullify the pointer
		p_SpriteToRemove = NULL;
	}
	// State the end of the function
	return;
}