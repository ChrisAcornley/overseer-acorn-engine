//////////////
// INCLUDES //
//////////////
#include "ApplicationClass.hpp"

/////////////////////////////////
// PUBLIC FUNCTION DEFINITIONS //
/////////////////////////////////

/*
 * Name:		ApplicationClass
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
ApplicationClass::ApplicationClass(void) 
{
	// Set pointers to NULL
	m_AcornEngine = NULL;
	m_UserInput = NULL;
	m_GameClass = NULL;
	m_GameTimer = NULL;

	// Set the FPS and time variables
	m_DeltaTime = 0.0f;					
	m_FramesPerSecond = 50;				
	m_MinimumTimePerFrame = 0.0f;

	// Set app running boolean to true - causes main loop to execute
	m_AppRunning = true;
}

/*
 * Name:		~ApplicationClass
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
ApplicationClass::~ApplicationClass(void)
{
	// Nothing to deconstruct here
}

/*
 * Name:		Initialise
===============================================================================
 * Arguments:	p_WindowInstance - Handle to the window
 *
 * Returns:		Boolean - True	- Succesful Execution
 *						- False	- Error Occured
 * Notes:		None
 */
bool ApplicationClass::Initialise(HINSTANCE p_WindowInstance)
{
	// Creates a new AcornEngine object
	m_AcornEngine = new acfw::AcornEngine();
	// Check if the new AcornEngine was created successfully
	if(!m_AcornEngine) {
		// Return false if unsuccessful
		return false;
	}

	// Ask the AcornEngine to create a new window using data from AppData.txt
	// and check it was created successfully
	if(!m_AcornEngine->CreateAWindow(p_WindowInstance, "Resources/Window Data/AppData.txt")) {
		// Return false if unsuccessful
		return false;
	}

	// Ask the AcornEngine for a pointer to the input handler
	m_UserInput = m_AcornEngine->GetInputHandler();
	// Check the pointer is valid (has an address)
	if(!m_UserInput) {
		// Return false if the pointer is NULL
		return false;
	}

	// Create a new Game class object
	m_GameClass = new GameClass();
	// Check the pointer is valid
	if(!m_GameClass) {
		// Return false if pointer is not valid
		return false;
	}

	// Check if the initialisation function executes correctly
	if(!m_GameClass->Initialise(m_UserInput, m_AcornEngine)) {
		// Return false if it was unsuccessful
		return false;
	}

	// Create a new Game Timer object
	m_GameTimer = new acfw::GameTimer();
	// Check if the game timer object is valid
	if(!m_GameTimer) {
		// Return false if pointer is invalid
		return false;
	}

	// Get FPS object and work out minimum frame time
	m_FramesPerSecond = m_AcornEngine->GetAppFPS();
	m_MinimumTimePerFrame = 1000.0f/ (float)m_FramesPerSecond;

	// Return true, stating the initialisation was successful
	return true;
}

/*
 * Name:		Run
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		Main loop of the application
 */
void ApplicationClass::Run()
{
	// Create a local store for the message information
	MSG l_Msg;

	// Zero the memory at the message information structure
	ZeroMemory(&l_Msg, sizeof(l_Msg));

	// Start the game timer
	m_GameTimer->Reset();

	// Main program loop
	while(m_AppRunning) {
		// Handle the windows messages.
		if(PeekMessage(&l_Msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&l_Msg);
			DispatchMessage(&l_Msg);	
		}

		// Check if the message sent is WM_QUIT 
		if(l_Msg.message == WM_QUIT) {
			// Break out of the while loop
			break;
		}

		else {
			// Tick the game timer
			m_GameTimer->Tick();
			// Add the delta time from the timer onto the app delta time
			m_DeltaTime += m_GameTimer->GetDeltaTimeMS();
			// Check to see if enough time has passed to render the frame
			if(m_DeltaTime >= m_MinimumTimePerFrame) {
				// Check if the games run function executes correctly
				if(!m_GameClass->Run(m_AcornEngine, m_DeltaTime)) {
					// Stop the game timer
					m_GameTimer->Stop();
					// Change the running flag to false
					m_AppRunning = false;
				}
				// Reset app delta time counter
				m_DeltaTime = 0.0f;
			}
		}
	}

	// State the end of the function
	return;
}

/*
 * Name:		Shutdown
===============================================================================
 * Arguments:	None
 * Returns:		None
 * Notes:		None
 */
void ApplicationClass::Shutdown()
{
	// Check if the game class pointer is valid
	if(m_GameClass) {
		// Call the shutdown method for the GameClass object
		m_GameClass->Shutdown();
		// Delete the game class object
		delete m_GameClass;
		// Nullify the pointer
		m_GameClass = NULL;
	}

	// Check if the input handler pointer is valid
	if(m_UserInput) {
		// Nullify the pointer - The actual input handler object is cleared in
		// the AcornEngine object
		m_UserInput = NULL;
	}

	// Check if the game timer pointer is valid
	if(m_GameTimer) {
		// Delete the object and nullify the pointer
		delete m_GameTimer;
		m_GameTimer = NULL;
	}

	// Check if the Acorn Engine pointer is valid
	if(m_AcornEngine) {
		// Call the shutdown method for the AcornEngine
		m_AcornEngine->Shutdown();
		// Delete the object at the address
		delete m_AcornEngine;
		// Nullify the pointer 
		m_AcornEngine = NULL;
	}

	// State the end of the function
	return;
}
