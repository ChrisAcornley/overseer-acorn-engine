//////////////
// INCLUDES //
//////////////
#include <Windows.h>
#include "ApplicationClass.hpp"

// Window main fuction - This is the entry point for the program.
/*
 * Name:		WinMain
===============================================================================
 * Arguments:	hinstance - Handle to the current window
 *				prevInstance - Handle to the previous window (if any)
 *				cmdLine - Pointer to the command line
 *				showCmd - Shows how the window will be displayed
 *
 * Returns:		Integer
 *
 * Notes:		None
 */
int WINAPI WinMain (HINSTANCE hinstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	// Create a pointer for the application close in the functions scope. Set it to NULL to start with.
	ApplicationClass *m_AppClass = NULL;
	// Create a new Application Class object
	m_AppClass = new ApplicationClass();
	// Check if the class created correctly
	if(!m_AppClass) {
		// If the class failed to create return 0 and end the program
		return 0;
	}

	// Try to initialise the program
	if(m_AppClass->Initialise(hinstance)) {
		// If initialisation ws successful, run the program
		m_AppClass->Run();
	}

	// Check if the application class exsists
	if(m_AppClass) {
		// Call the shutdown for the class object
		m_AppClass->Shutdown();
		// Delete the class at the pointer
		delete m_AppClass;
		// Null the pointer
		m_AppClass = NULL;
	}

	// End program
	return 0;
}