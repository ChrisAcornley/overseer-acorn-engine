/*
 * Name:	ApplicationClass.hpp
 * Author:	Christopher Acornley
 * Date:	25-Feb-2014
 */

#ifndef _APPLICATION_CLASS_HPP_
#define _APPLICATION_CLASS_HPP_

//////////////
// INCLUDES //
//////////////
#include "Acorn Engine Library\AcornEngine.hpp"
#include "Acorn Engine Library\Input Handler\InputHandler.hpp"
#include "Acorn Engine Library\Graphics\Sprite.hpp"
#include "..\Game States\GameClass.hpp"

/*
 *	This class is the main application class and contains the applications main
 *	loop. It also contains the input handler pointer, which can be passed onto
 *	other classes and structures as needed.
 */
class ApplicationClass
{
	//////////////////////
	// PUBLIC FUNCTIONS //
	//////////////////////
	public:
		// Constructor
		ApplicationClass(void);
		// Deconstructor
		~ApplicationClass(void);
		// Initialise function - creates window and class objects
		bool Initialise(HINSTANCE p_WindowInstance);
		// Run function - Runs the main application loop
		void Run();
		// Gets the status of the m_AppRunning boolean
		bool IsAppRunning() { return m_AppRunning; }
		// Sets the m_AppRunning boolean to the passed value
		void SetAppRunning(bool p_AppRunning) { m_AppRunning = p_AppRunning; }
		// Shutdown function - Closes the pointers safely for the class
		void Shutdown();

	///////////////////
	// PRIVATE TYPES //
	///////////////////
	private:
		acfw::AcornEngine *m_AcornEngine;	// Pointer to an instance of Acorn Engine
		acfw::InputHandler *m_UserInput;	// Pointer to an instance of the Input Handler
		bool m_AppRunning;					// Boolean to state if the app is running or not
		GameClass *m_GameClass;				// Pointer to the game class object
		acfw::GameTimer *m_GameTimer;		// Pointer to the game timer class
		float m_DeltaTime;					// The delta time of the applicato
		int m_FramesPerSecond;				// The number of frames per second the application runs at
		float m_MinimumTimePerFrame;		// The minimum elapsed time allowed per frame

}; //== End of ApplicationClass ==//

#endif

