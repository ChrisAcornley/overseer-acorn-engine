========================================================================
    WINDOWS APPLICATION : 2D Engine Project Overview
========================================================================
    AUTHOR : 	     	  Christopher Acornley (1000697)
========================================================================

This application is the game which will demonstrate the 'Overseer' AI that is the focus of this project. The application makes use of the 
Acorn Engine Library which provides a framework in which to work on. The library contain as much code as possible for creating and drawing 
graphics on the screen, as well as text, handling input and reading text files for data. The application handles more gameplay orientated 
code and and contains the code for the Overseer AI.

This project contains eight classes and the main.cpp file. They are in no particular order;

main.cpp -	This is the entry point of the application. Whilst the program is meant to be object orientated, as a C++ program, 
it needs a non-class entry point which is handled in this file.

ApplicationClass -	This class contains the main game loop and constructs the rest of the program so it can run.

GameClass -	This class contains the current game state and tells the program where to go depending on what state it is in. This class 
has several other classes inside it, each of which represents a part of the game.

MainMenuClass -	This class contains the code to draw and update the main menu of the game. From here, users can move to other parts of 
the program. 	 

GameBackground - Handles the positioning and rendering of the background in the game and the main menu.

MainGameClass - Handles the updating and rendering of the game as well as logical operations.

MapDrawerObject - Handles the rendering and updating of the maze/map.

PlayerObject - Handles the positioning, updating and rendering of the player object. It also controls where the player can move.

UserInterfaceHandler - Renders and updates the UI to help display data that is helpful to the player, as well as the text from the
Overseer.


http://www.robelle.com/smugbook/ascii.html - 21 March 2014 - 11:11